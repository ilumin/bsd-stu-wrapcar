
namespace PrintControl
{
    /// <summary>
    /// Enumeration for the page orientation.
    /// </summary>
    public enum PageOrientation
    {
        /// <summary>
        /// Portrait
        /// </summary>
        Portrait,
        /// <summary>
        /// Landscape
        /// </summary>
        Landscape
    }
}