namespace PrintControl
{
    /// <summary>
    /// Enumeration for text alignment
    /// </summary>
    public enum TextAlignment
    {
        /// <summary>
        /// Left side
        /// </summary>
        Left,
        //Center,
        /// <summary>
        /// Right side
        /// </summary>
        Right
    }


}