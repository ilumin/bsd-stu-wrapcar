namespace PrintControl
{
    /// <summary>
    /// PrintDocumentControl
    /// </summary>
    partial class PrintDocumentControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:DoNotPassLiteralsAsLocalizedParameters", MessageId = "System.Windows.Forms.ToolStripItem.set_Text(System.String)"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:DoNotPassLiteralsAsLocalizedParameters", MessageId = "System.Windows.Forms.Control.set_Text(System.String)")]
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrintDocumentControl));
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.panelLeftFill = new System.Windows.Forms.Panel();
            this.propertyGrid = new System.Windows.Forms.PropertyGrid();
            this.panelLeftTop = new System.Windows.Forms.Panel();
            this.toolStripPrint = new System.Windows.Forms.ToolStrip();
            this.tsLabelPrint = new System.Windows.Forms.ToolStripLabel();
            this.tsButtonPrintNow = new System.Windows.Forms.ToolStripButton();
            this.tsSeparatorPrinters = new System.Windows.Forms.ToolStripSeparator();
            this.tsLabelReset = new System.Windows.Forms.ToolStripLabel();
            this.tsButtonReset = new System.Windows.Forms.ToolStripButton();
            this.panelRightFill = new System.Windows.Forms.Panel();
            this.panelRightTop = new System.Windows.Forms.Panel();
            this.toolStripPreview = new System.Windows.Forms.ToolStrip();
            this.tsButtonShowPreview = new System.Windows.Forms.ToolStripButton();
            this.tsSeparatorZoom = new System.Windows.Forms.ToolStripSeparator();
            this.tsButtonZoomIn = new System.Windows.Forms.ToolStripButton();
            this.tsButtonZoomOut = new System.Windows.Forms.ToolStripButton();
            this.tsButtonZoomFit = new System.Windows.Forms.ToolStripButton();
            this.tsComboZoomFactor = new System.Windows.Forms.ToolStripComboBox();
            this.tsSeparatorNavigation = new System.Windows.Forms.ToolStripSeparator();
            this.tsButtonFirstPage = new System.Windows.Forms.ToolStripButton();
            this.tsButtonPreviousPage = new System.Windows.Forms.ToolStripButton();
            this.tsTextBoxPageNr = new System.Windows.Forms.ToolStripTextBox();
            this.tsTextBoxPageTotal = new System.Windows.Forms.ToolStripTextBox();
            this.tsButtonNextPage = new System.Windows.Forms.ToolStripButton();
            this.tsButtonLastPage = new System.Windows.Forms.ToolStripButton();
            this.tsSeparatorPageView = new System.Windows.Forms.ToolStripSeparator();
            this.tsButtonSinglePageView = new System.Windows.Forms.ToolStripButton();
            this.tsButtonDoublePageView = new System.Windows.Forms.ToolStripButton();
            this.tsComboPageView = new System.Windows.Forms.ToolStripComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.panelLeftFill.SuspendLayout();
            this.panelLeftTop.SuspendLayout();
            this.toolStripPrint.SuspendLayout();
            this.panelRightTop.SuspendLayout();
            this.toolStripPreview.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer
            // 
            this.splitContainer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer.Cursor = System.Windows.Forms.Cursors.Default;
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.panelLeftFill);
            this.splitContainer.Panel1.Controls.Add(this.panelLeftTop);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.panelRightFill);
            this.splitContainer.Panel2.Controls.Add(this.panelRightTop);
            this.splitContainer.Size = new System.Drawing.Size(834, 592);
            this.splitContainer.SplitterDistance = 212;
            this.splitContainer.TabIndex = 0;
            // 
            // panelLeftFill
            // 
            this.panelLeftFill.Controls.Add(this.propertyGrid);
            this.panelLeftFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeftFill.Location = new System.Drawing.Point(0, 38);
            this.panelLeftFill.Name = "panelLeftFill";
            this.panelLeftFill.Size = new System.Drawing.Size(208, 550);
            this.panelLeftFill.TabIndex = 1;
            // 
            // propertyGrid
            // 
            this.propertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGrid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.propertyGrid.Location = new System.Drawing.Point(0, 0);
            this.propertyGrid.Name = "propertyGrid";
            this.propertyGrid.Size = new System.Drawing.Size(208, 550);
            this.propertyGrid.TabIndex = 0;
            // 
            // panelLeftTop
            // 
            this.panelLeftTop.Controls.Add(this.toolStripPrint);
            this.panelLeftTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLeftTop.Location = new System.Drawing.Point(0, 0);
            this.panelLeftTop.Name = "panelLeftTop";
            this.panelLeftTop.Size = new System.Drawing.Size(208, 38);
            this.panelLeftTop.TabIndex = 0;
            // 
            // toolStripPrint
            // 
            this.toolStripPrint.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStripPrint.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsLabelPrint,
            this.tsButtonPrintNow,
            this.tsSeparatorPrinters,
            this.tsLabelReset,
            this.tsButtonReset});
            this.toolStripPrint.Location = new System.Drawing.Point(0, 0);
            this.toolStripPrint.Name = "toolStripPrint";
            this.toolStripPrint.Size = new System.Drawing.Size(208, 31);
            this.toolStripPrint.TabIndex = 0;
            this.toolStripPrint.Text = "toolStrip1";
            // 
            // tsLabelPrint
            // 
            this.tsLabelPrint.Name = "tsLabelPrint";
            this.tsLabelPrint.Size = new System.Drawing.Size(85, 28);
            this.tsLabelPrint.Text = "Send to printer";
            this.tsLabelPrint.Click += new System.EventHandler(this.tsLabelPrint_Click);
            // 
            // tsButtonPrintNow
            // 
            this.tsButtonPrintNow.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButtonPrintNow.Image = ((System.Drawing.Image)(resources.GetObject("tsButtonPrintNow.Image")));
            this.tsButtonPrintNow.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButtonPrintNow.Name = "tsButtonPrintNow";
            this.tsButtonPrintNow.Size = new System.Drawing.Size(28, 28);
            this.tsButtonPrintNow.Text = "Print";
            this.tsButtonPrintNow.Click += new System.EventHandler(this.tsButtonPrintNow_Click);
            // 
            // tsSeparatorPrinters
            // 
            this.tsSeparatorPrinters.Name = "tsSeparatorPrinters";
            this.tsSeparatorPrinters.Size = new System.Drawing.Size(6, 31);
            // 
            // tsLabelReset
            // 
            this.tsLabelReset.Name = "tsLabelReset";
            this.tsLabelReset.Size = new System.Drawing.Size(35, 28);
            this.tsLabelReset.Text = "Reset";
            this.tsLabelReset.Click += new System.EventHandler(this.tsLabelReset_Click);
            // 
            // tsButtonReset
            // 
            this.tsButtonReset.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButtonReset.Image = ((System.Drawing.Image)(resources.GetObject("tsButtonReset.Image")));
            this.tsButtonReset.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButtonReset.Name = "tsButtonReset";
            this.tsButtonReset.Size = new System.Drawing.Size(28, 28);
            this.tsButtonReset.Text = "Reset";
            this.tsButtonReset.ToolTipText = "Reset";
            this.tsButtonReset.Click += new System.EventHandler(this.tsButtonReset_Click);
            // 
            // panelRightFill
            // 
            this.panelRightFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelRightFill.Location = new System.Drawing.Point(0, 38);
            this.panelRightFill.Name = "panelRightFill";
            this.panelRightFill.Size = new System.Drawing.Size(614, 550);
            this.panelRightFill.TabIndex = 1;
            // 
            // panelRightTop
            // 
            this.panelRightTop.Controls.Add(this.toolStripPreview);
            this.panelRightTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelRightTop.Location = new System.Drawing.Point(0, 0);
            this.panelRightTop.Name = "panelRightTop";
            this.panelRightTop.Size = new System.Drawing.Size(614, 38);
            this.panelRightTop.TabIndex = 0;
            // 
            // toolStripPreview
            // 
            this.toolStripPreview.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStripPreview.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsButtonShowPreview,
            this.tsSeparatorZoom,
            this.tsButtonZoomIn,
            this.tsButtonZoomOut,
            this.tsButtonZoomFit,
            this.tsComboZoomFactor,
            this.tsSeparatorNavigation,
            this.tsButtonFirstPage,
            this.tsButtonPreviousPage,
            this.tsTextBoxPageNr,
            this.tsTextBoxPageTotal,
            this.tsButtonNextPage,
            this.tsButtonLastPage,
            this.tsSeparatorPageView,
            this.tsButtonSinglePageView,
            this.tsButtonDoublePageView,
            this.tsComboPageView});
            this.toolStripPreview.Location = new System.Drawing.Point(0, 0);
            this.toolStripPreview.Name = "toolStripPreview";
            this.toolStripPreview.Size = new System.Drawing.Size(614, 31);
            this.toolStripPreview.TabIndex = 0;
            this.toolStripPreview.Text = "toolStripPreview";
            // 
            // tsButtonShowPreview
            // 
            this.tsButtonShowPreview.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButtonShowPreview.Image = ((System.Drawing.Image)(resources.GetObject("tsButtonShowPreview.Image")));
            this.tsButtonShowPreview.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButtonShowPreview.Name = "tsButtonShowPreview";
            this.tsButtonShowPreview.Size = new System.Drawing.Size(28, 28);
            this.tsButtonShowPreview.Text = "Show Preview";
            this.tsButtonShowPreview.Click += new System.EventHandler(this.tsButtonShowPreview_Click);
            // 
            // tsSeparatorZoom
            // 
            this.tsSeparatorZoom.Name = "tsSeparatorZoom";
            this.tsSeparatorZoom.Size = new System.Drawing.Size(6, 31);
            // 
            // tsButtonZoomIn
            // 
            this.tsButtonZoomIn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButtonZoomIn.Image = ((System.Drawing.Image)(resources.GetObject("tsButtonZoomIn.Image")));
            this.tsButtonZoomIn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButtonZoomIn.Name = "tsButtonZoomIn";
            this.tsButtonZoomIn.Size = new System.Drawing.Size(28, 28);
            this.tsButtonZoomIn.Text = "Zoom In";
            this.tsButtonZoomIn.Click += new System.EventHandler(this.tsButtonZoomIn_Click);
            // 
            // tsButtonZoomOut
            // 
            this.tsButtonZoomOut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButtonZoomOut.Image = ((System.Drawing.Image)(resources.GetObject("tsButtonZoomOut.Image")));
            this.tsButtonZoomOut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButtonZoomOut.Name = "tsButtonZoomOut";
            this.tsButtonZoomOut.Size = new System.Drawing.Size(28, 28);
            this.tsButtonZoomOut.Text = "Zoom Out";
            this.tsButtonZoomOut.Click += new System.EventHandler(this.tsButtonZoomOut_Click);
            // 
            // tsButtonZoomFit
            // 
            this.tsButtonZoomFit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButtonZoomFit.Image = ((System.Drawing.Image)(resources.GetObject("tsButtonZoomFit.Image")));
            this.tsButtonZoomFit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButtonZoomFit.Name = "tsButtonZoomFit";
            this.tsButtonZoomFit.Size = new System.Drawing.Size(28, 28);
            this.tsButtonZoomFit.Text = "Fit to Page";
            this.tsButtonZoomFit.Click += new System.EventHandler(this.tsButtonZoomFit_Click);
            // 
            // tsComboZoomFactor
            // 
            this.tsComboZoomFactor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tsComboZoomFactor.DropDownWidth = 50;
            this.tsComboZoomFactor.Name = "tsComboZoomFactor";
            this.tsComboZoomFactor.Size = new System.Drawing.Size(75, 31);
            this.tsComboZoomFactor.ToolTipText = "Zoom factor";
            // 
            // tsSeparatorNavigation
            // 
            this.tsSeparatorNavigation.Name = "tsSeparatorNavigation";
            this.tsSeparatorNavigation.Size = new System.Drawing.Size(6, 31);
            // 
            // tsButtonFirstPage
            // 
            this.tsButtonFirstPage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButtonFirstPage.Image = ((System.Drawing.Image)(resources.GetObject("tsButtonFirstPage.Image")));
            this.tsButtonFirstPage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButtonFirstPage.Name = "tsButtonFirstPage";
            this.tsButtonFirstPage.Size = new System.Drawing.Size(28, 28);
            this.tsButtonFirstPage.Text = "First Page";
            this.tsButtonFirstPage.Click += new System.EventHandler(this.tsButtonFirstPage_Click);
            // 
            // tsButtonPreviousPage
            // 
            this.tsButtonPreviousPage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButtonPreviousPage.Image = ((System.Drawing.Image)(resources.GetObject("tsButtonPreviousPage.Image")));
            this.tsButtonPreviousPage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButtonPreviousPage.Name = "tsButtonPreviousPage";
            this.tsButtonPreviousPage.Size = new System.Drawing.Size(28, 28);
            this.tsButtonPreviousPage.Text = "Previous Page";
            this.tsButtonPreviousPage.Click += new System.EventHandler(this.tsButtonPreviousPage_Click);
            // 
            // tsTextBoxPageNr
            // 
            this.tsTextBoxPageNr.Name = "tsTextBoxPageNr";
            this.tsTextBoxPageNr.Size = new System.Drawing.Size(25, 31);
            this.tsTextBoxPageNr.Text = "0";
            this.tsTextBoxPageNr.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tsTextBoxPageNr.ToolTipText = "Current page number";
            this.tsTextBoxPageNr.TextChanged += new System.EventHandler(this.tsTextBoxPageNr_TextChanged);
            // 
            // tsTextBoxPageTotal
            // 
            this.tsTextBoxPageTotal.Name = "tsTextBoxPageTotal";
            this.tsTextBoxPageTotal.ReadOnly = true;
            this.tsTextBoxPageTotal.Size = new System.Drawing.Size(25, 31);
            this.tsTextBoxPageTotal.Text = "0";
            this.tsTextBoxPageTotal.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tsTextBoxPageTotal.ToolTipText = "Total page amount";
            // 
            // tsButtonNextPage
            // 
            this.tsButtonNextPage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButtonNextPage.Image = ((System.Drawing.Image)(resources.GetObject("tsButtonNextPage.Image")));
            this.tsButtonNextPage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButtonNextPage.Name = "tsButtonNextPage";
            this.tsButtonNextPage.Size = new System.Drawing.Size(28, 28);
            this.tsButtonNextPage.Text = "Next Page";
            this.tsButtonNextPage.Click += new System.EventHandler(this.tsButtonNextPage_Click);
            // 
            // tsButtonLastPage
            // 
            this.tsButtonLastPage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButtonLastPage.Image = ((System.Drawing.Image)(resources.GetObject("tsButtonLastPage.Image")));
            this.tsButtonLastPage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButtonLastPage.Name = "tsButtonLastPage";
            this.tsButtonLastPage.Size = new System.Drawing.Size(28, 28);
            this.tsButtonLastPage.Text = "Last Page";
            this.tsButtonLastPage.Click += new System.EventHandler(this.tsButtonLastPage_Click);
            // 
            // tsSeparatorPageView
            // 
            this.tsSeparatorPageView.Name = "tsSeparatorPageView";
            this.tsSeparatorPageView.Size = new System.Drawing.Size(6, 31);
            // 
            // tsButtonSinglePageView
            // 
            this.tsButtonSinglePageView.CheckOnClick = true;
            this.tsButtonSinglePageView.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButtonSinglePageView.Image = ((System.Drawing.Image)(resources.GetObject("tsButtonSinglePageView.Image")));
            this.tsButtonSinglePageView.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButtonSinglePageView.Name = "tsButtonSinglePageView";
            this.tsButtonSinglePageView.Size = new System.Drawing.Size(28, 28);
            this.tsButtonSinglePageView.Text = "Single Page View";
            this.tsButtonSinglePageView.Click += new System.EventHandler(this.tsButtonSinglePageView_Click);
            // 
            // tsButtonDoublePageView
            // 
            this.tsButtonDoublePageView.CheckOnClick = true;
            this.tsButtonDoublePageView.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButtonDoublePageView.Image = ((System.Drawing.Image)(resources.GetObject("tsButtonDoublePageView.Image")));
            this.tsButtonDoublePageView.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButtonDoublePageView.Name = "tsButtonDoublePageView";
            this.tsButtonDoublePageView.Size = new System.Drawing.Size(28, 28);
            this.tsButtonDoublePageView.Text = "Double Page View";
            this.tsButtonDoublePageView.Click += new System.EventHandler(this.tsButtonDoublePageView_Click);
            // 
            // tsComboPageView
            // 
            this.tsComboPageView.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tsComboPageView.Name = "tsComboPageView";
            this.tsComboPageView.Size = new System.Drawing.Size(75, 31);
            this.tsComboPageView.ToolTipText = "Page view amount";
            // 
            // PrintDocumentControl
            // 
            this.Controls.Add(this.splitContainer);
            this.Name = "PrintDocumentControl";
            this.Size = new System.Drawing.Size(834, 592);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.panelLeftFill.ResumeLayout(false);
            this.panelLeftTop.ResumeLayout(false);
            this.panelLeftTop.PerformLayout();
            this.toolStripPrint.ResumeLayout(false);
            this.toolStripPrint.PerformLayout();
            this.panelRightTop.ResumeLayout(false);
            this.panelRightTop.PerformLayout();
            this.toolStripPreview.ResumeLayout(false);
            this.toolStripPreview.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.Panel panelRightTop;
        private System.Windows.Forms.Panel panelRightFill;
        private System.Windows.Forms.ToolStrip toolStripPreview;
        private System.Windows.Forms.ToolStripButton tsButtonZoomIn;
        private System.Windows.Forms.ToolStripButton tsButtonZoomOut;
        private System.Windows.Forms.ToolStripSeparator tsSeparatorZoom;
        private System.Windows.Forms.ToolStripSeparator tsSeparatorNavigation;
        private System.Windows.Forms.ToolStripButton tsButtonFirstPage;
        private System.Windows.Forms.ToolStripButton tsButtonPreviousPage;
        private System.Windows.Forms.ToolStripTextBox tsTextBoxPageNr;
        private System.Windows.Forms.ToolStripButton tsButtonNextPage;
        private System.Windows.Forms.ToolStripButton tsButtonLastPage;
        private System.Windows.Forms.Panel panelLeftTop;
        private System.Windows.Forms.Panel panelLeftFill;
        private System.Windows.Forms.PropertyGrid propertyGrid;
        private System.Windows.Forms.ToolStrip toolStripPrint;
        private System.Windows.Forms.ToolStripButton tsButtonPrintNow;
        private System.Windows.Forms.ToolStripSeparator tsSeparatorPrinters;
        private System.Windows.Forms.ToolStripLabel tsLabelPrint;
        private System.Windows.Forms.ToolStripLabel tsLabelReset;
        private System.Windows.Forms.ToolStripButton tsButtonReset;
        private System.Windows.Forms.ToolStripButton tsButtonShowPreview;
        private System.Windows.Forms.ToolStripComboBox tsComboZoomFactor;
        private System.Windows.Forms.ToolStripSeparator tsSeparatorPageView;
        private System.Windows.Forms.ToolStripButton tsButtonSinglePageView;
        private System.Windows.Forms.ToolStripButton tsButtonDoublePageView;
        private System.Windows.Forms.ToolStripButton tsButtonZoomFit;
        private System.Windows.Forms.ToolStripTextBox tsTextBoxPageTotal;
        private System.Windows.Forms.ToolStripComboBox tsComboPageView;

    }
}