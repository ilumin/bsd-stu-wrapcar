using System;
using System.Drawing.Printing;
using System.Globalization;
using System.ServiceProcess;
using System.Windows.Forms;

namespace PrintControl
{
    /// <summary>
    /// User control that contains all the visual functions of the combined printing utility.
    /// </summary>
    public partial class PrintDocumentControl : UserControl
    {
        # region Members

        private PrintDocumentComponent  _printDocument;
        private PrintPreviewControl     _printPreviewControl;
        private PrintDocumentSettings   _printDocumentSettings;
        private int _pageCounter;

        # endregion Members

        # region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PrintDocumentControl"/> class.
        /// </summary>
        /// <param name="controlToPrint">The control to print.</param>
        public PrintDocumentControl(Control controlToPrint)
        {
            InitializeComponent();

            //check if the PrintSpooler service is available

            try
            {
                ServiceController svcController = new ServiceController("Print Spooler");
                if (svcController.Status != ServiceControllerStatus.Running)
                {
                    MessageBox.Show("The print spooler service is not available. Cannot continue.",
                                    "Print preview...",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error,
                                    MessageBoxDefaultButton.Button1,
                                    0);
                    return; //exit
                }
            }
            catch (Exception)
            {
                //swallow it
            }

            _printDocumentSettings = new PrintDocumentSettings();

            propertyGrid.PropertySort = PropertySort.CategorizedAlphabetical;
            propertyGrid.ToolbarVisible = false;
            propertyGrid.SelectedObject = _printDocumentSettings;
            propertyGrid.Refresh();

            tsComboZoomFactor.Items.Add("1");
            tsComboZoomFactor.Items.Add("1.5");
            tsComboZoomFactor.Items.Add("2");
            tsComboZoomFactor.Items.Add("2.5");
            tsComboZoomFactor.Items.Add("3");
            tsComboZoomFactor.Items.Add("4");
            tsComboZoomFactor.Items.Add("5");
            tsComboZoomFactor.Text = "2";

            tsComboPageView.Items.Add("2");
            tsComboPageView.Items.Add("4");
            tsComboPageView.Items.Add("6");
            tsComboPageView.Text = "2";

            _printDocument = new PrintDocumentComponent(controlToPrint, _printDocumentSettings);


            //EndPrint handler
            _printDocument.EndPrint += new PrintEventHandler(OnPrintDocumentEndPrint);

            if (String.IsNullOrEmpty(_printDocumentSettings.DefaultPrinter))
                MessageBox.Show("There is no default printer installed. Printing is not possible.",
                "Print preview...",
                MessageBoxButtons.OK,
                MessageBoxIcon.Error,
                MessageBoxDefaultButton.Button1,
                0);
            else
                ShowPreview();
        }

        # endregion Constructor

        # region Preview

        private void OnPrintDocumentEndPrint(object sender, PrintEventArgs e)
        {
            if (_printDocument.PrintController.IsPreview)
            {
                //set page total for usage in PrintDocumentSettings
                _printDocumentSettings.TotalPages = _printDocument.PageTotal;
                //set preview toolstrip
                SetPreviewToolStrip();
                //refresh the property grid
                propertyGrid.Refresh();
            }
        }

        private void NewPrintPreviewControl()
        {
            panelRightFill.SuspendLayout();
            panelRightFill.Controls.Clear();

            //new control
            _printPreviewControl = new PrintPreviewControl();
            _printPreviewControl.Parent = panelRightFill;
            _printPreviewControl.Dock = DockStyle.Fill;
            _printPreviewControl.AutoZoom = true;
            _printPreviewControl.Columns = 1;
            _printPreviewControl.Rows = 1;
            tsButtonSinglePageView.Checked = true;
            tsButtonDoublePageView.Checked = !tsButtonSinglePageView.Checked;
            _printPreviewControl.StartPage = 0;

            panelRightFill.ResumeLayout();            
        }

        private void ShowPreview()
        {
            Cursor = Cursors.WaitCursor;

            //rig up a new PrintPreviewControl
            NewPrintPreviewControl();

            //set several properties of the PrintDocument
            //_printDocument.DocumentName = _printDocumentSettings.HeaderText;
            _printDocument.OriginAtMargins = _printDocumentSettings.OriginAtMargins;
          
            _printDocument.DefaultPageSettings = _printDocumentSettings.PageSettings;
            _printDocument.PrinterSettings = _printDocumentSettings.PrinterSettings;

            //create a PreviewPrintController
            PreviewPrintController pc = new PreviewPrintController();
            pc.UseAntiAlias = true;
            
            //assign the PreviewPrintController to the PrintDocument
            _printDocument.PrintController = pc;

            //assign the document to the PrintPreviewControl
            _printPreviewControl.Document = _printDocument;
            _printPreviewControl.StartPage = 0;
            _printPreviewControl.UseAntiAlias = true;

            //set page counter
            _pageCounter = 1;

            //set initial values for the preview toolstrip
            SetPreviewToolStrip();

            Cursor = Cursors.Default;
        }

        # endregion Preview

        # region Preview Toolstrip

        /// <summary>
        /// Sets the preview tool strip.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Int32.ToString")]
        private void SetPreviewToolStrip()
        {
            tsTextBoxPageNr.Text = _pageCounter.ToString();
            tsTextBoxPageTotal.Text = _printDocument.PageTotal.ToString();

            _printPreviewControl.StartPage = _pageCounter - 1;

            if (_printDocument.PageTotal <= 1)
            {
                tsButtonFirstPage.Enabled = false;
                tsButtonPreviousPage.Enabled = false;
                tsButtonNextPage.Enabled = false;
                tsButtonLastPage.Enabled = false;
            }
            else
            {
                tsButtonFirstPage.Enabled = true;
                tsButtonPreviousPage.Enabled = true;
                tsButtonNextPage.Enabled = true;
                tsButtonLastPage.Enabled = true;
 
                if (_printPreviewControl.StartPage == 0)
                {
                    tsButtonFirstPage.Enabled = false;
                    tsButtonPreviousPage.Enabled = false;
                }
                else
                {
                    tsButtonFirstPage.Enabled = true;
                    tsButtonPreviousPage.Enabled = true;
                }

                if (_printPreviewControl.StartPage == (_printDocument.PageTotal - 1))
                {
                    tsButtonNextPage.Enabled = false;
                    tsButtonLastPage.Enabled = false;
                }
                else
                {
                    tsButtonNextPage.Enabled = true;
                    tsButtonLastPage.Enabled = true;
                }
            }

            //register current page in document settings
            _printDocumentSettings.CurrentPage = _pageCounter;

        }

        private void tsButtonShowPreview_Click(object sender, EventArgs e)
        {
            ShowPreview();
        }

        private void tsButtonZoomIn_Click(object sender, EventArgs e)
        {
            if (_printPreviewControl.Zoom < 2)
            {
                _printPreviewControl.Zoom =
                    _printPreviewControl.Zoom * (Double.Parse(tsComboZoomFactor.Text, CultureInfo.InvariantCulture));
                _printPreviewControl.InvalidatePreview();
                SetPreviewToolStrip();
            }
        }

        private void tsButtonZoomOut_Click(object sender, EventArgs e)
        {
            if (_printPreviewControl.Zoom > 0.1)
            {
                _printPreviewControl.Zoom = 
                    _printPreviewControl.Zoom / (Double.Parse(tsComboZoomFactor.Text, CultureInfo.InvariantCulture));
                _printPreviewControl.InvalidatePreview();
                SetPreviewToolStrip();
            }
        }

        private void tsButtonZoomFit_Click(object sender, EventArgs e)
        {
            _printPreviewControl.AutoZoom = true;
            _printPreviewControl.InvalidatePreview();
            SetPreviewToolStrip();
        }

        private void tsButtonFirstPage_Click(object sender, EventArgs e)
        {
            _pageCounter = 1;
            SetPreviewToolStrip();
        }

        private void tsButtonPreviousPage_Click(object sender, EventArgs e)
        {
            DecreasePageCounter();
            SetPreviewToolStrip();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Int32.ToString"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Convert.ToInt32(System.String)")]
        private void tsTextBoxPageNr_TextChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(tsTextBoxPageNr.Text))
            {
                try
                {
                    int pageNr = Convert.ToInt32(tsTextBoxPageNr.Text);

                    if (pageNr > _printDocument.PageTotal)
                    {
                        pageNr = _printDocument.PageTotal;
                    }
                    if (pageNr < 1)
                    {
                        pageNr = 1;
                    }

                    _pageCounter = pageNr;
                    tsTextBoxPageNr.Text = pageNr.ToString();
                    SetPreviewToolStrip();
                }
                catch (FormatException)
                {
                    //swallow it
                }
            }
        }

        private void tsButtonNextPage_Click(object sender, EventArgs e)
        {
            IncreasePageCounter();
            SetPreviewToolStrip();
        }

        private void tsButtonLastPage_Click(object sender, EventArgs e)
        {
            _pageCounter = _printDocument.PageTotal;
            SetPreviewToolStrip();
        }

        private void IncreasePageCounter()
        {
            if (_pageCounter < _printDocument.PageTotal)
                _pageCounter++;
        }

        private void DecreasePageCounter()
        {
            if (_pageCounter > 1)
                _pageCounter--;
        }

        private void tsButtonSinglePageView_Click(object sender, EventArgs e)
        {
            _printPreviewControl.Rows = 1;
            _printPreviewControl.Columns = 1;
            tsButtonSinglePageView.Checked = true;
            tsButtonDoublePageView.Checked = !tsButtonSinglePageView.Checked;
            SetPreviewToolStrip();
        }

        private void tsButtonDoublePageView_Click(object sender, EventArgs e)
        {
            switch (tsComboPageView.Text)
            {
                case "2":
                    _printPreviewControl.Rows = 1;
                    _printPreviewControl.Columns = 2;
                    break;
                case "4":
                    _printPreviewControl.Rows = 2;
                    _printPreviewControl.Columns = 2;
                    break;
                case "6":
                    _printPreviewControl.Rows = 2;
                    _printPreviewControl.Columns = 3;
                    break;
            }
            tsButtonSinglePageView.Checked = false;
            tsButtonDoublePageView.Checked = !tsButtonSinglePageView.Checked;
            SetPreviewToolStrip();
        }

        # endregion Preview Toolstrip

        # region Printing Toolstrip

        private void tsButtonPrintNow_Click(object sender, EventArgs e)
        {
            PrintNow();
        }

        private void tsLabelPrint_Click(object sender, EventArgs e)
        {
            PrintNow();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:DoNotPassLiteralsAsLocalizedParameters", MessageId = "System.Windows.Forms.MessageBox.Show(System.String,System.String,System.Windows.Forms.MessageBoxButtons,System.Windows.Forms.MessageBoxIcon,System.Windows.Forms.MessageBoxDefaultButton,System.Windows.Forms.MessageBoxOptions)")]
        private void PrintNow()
        {
            DialogResult result;
            if (_printDocumentSettings.PrintToFile)
            {
                result =
                    MessageBox.Show(
                        "The document " + _printDocument.DocumentName +
                        " will be saved to file " + _printDocumentSettings.FileName +
                        ". Continue?",
                        "Print to file...",
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question,
                        MessageBoxDefaultButton.Button2,
                        0);

                if (result == DialogResult.Yes)
                {
                    PrintToFile();
                    return;
                }
            }

            result =
                MessageBox.Show(
                "The document " + _printDocument.DocumentName + 
                    " from page " + _printDocumentSettings.FromPage +
                    " to page " + _printDocumentSettings.ToPage +
                    " will be sent to printer " + _printDocumentSettings.SelectedPrinter + 
                    ". Continue?",
                "Print to printer...",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question,
                MessageBoxDefaultButton.Button2,
                0);

            if (result == DialogResult.Yes)
            {
                _printDocument.PrinterSettings = _printDocumentSettings.PrinterSettings;
                _printDocumentSettings.PageSettings.PrinterSettings = _printDocument.PrinterSettings;
                _printDocument.DefaultPageSettings = _printDocumentSettings.PageSettings;

                StandardPrintController spc = new StandardPrintController();
                
                _printDocument.PrintController = 
                    new PrintControllerWithStatusDialog(spc,
                    "Printing "+_printDocument.DocumentName + " to " + 
                        _printDocumentSettings.SelectedPrinter);

                _printDocument.Print();
            }
        }


        private void PrintToFile()
        {
            _printDocument.PrinterSettings = _printDocumentSettings.PrinterSettings;

            System.Drawing.Bitmap bitmap = _printDocument.GetBitmap();
            bitmap.Save(_printDocumentSettings.FileName);
            bitmap.Dispose();
        }

        private void tsButtonReset_Click(object sender, EventArgs e)
        {
            Reset();
        }

        private void tsLabelReset_Click(object sender, EventArgs e)
        {
            Reset();
        }

        private void Reset()
        {
            _printDocumentSettings.SetDefaults(); 
            propertyGrid.Refresh();
        }

        # endregion Printing Toolstrip


    }
}