using System.Windows.Forms;

namespace PrintControl
{
    /// <summary>
    /// Form encapsulating the PrintDocumentControl
    /// </summary>
    public partial class PrintDocumentForm : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrintDocumentForm"/> class.
        /// </summary>
        /// <param name="controlToPrint">The control to print.</param>
        public PrintDocumentForm(Control controlToPrint)
        {
            InitializeComponent();

            pnlPrint.Dock = DockStyle.Fill;

            /* Create a new PrintDocumentControl 
             *  and provide the control that should be printed. */
            PrintDocumentControl pdc = new PrintDocumentControl(controlToPrint);
            pnlPrint.Controls.Add(pdc);
            pdc.Dock = DockStyle.Fill;
        }

        private void PrintDocumentForm_Load(object sender, System.EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }
    }
}