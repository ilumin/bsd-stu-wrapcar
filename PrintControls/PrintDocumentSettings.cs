using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Drawing.Printing;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace PrintControl
{
    /// <summary>
    /// PrintDocumentSettings
    /// </summary>
    [DefaultProperty("SelectedPrinter")]
    public sealed class PrintDocumentSettings : IDisposable
    {
        # region Members

        private PrinterSettings _printerSettings;
        private PageSettings _pageSettings;

        private string _selectedPrinter;
        private string _defaultPrinter;
        private PaperKind _paperKind;
        private PaperSourceKind _paperSourceKind;
        private PrinterResolutionKind _printerResolutionKind;

        private PageOrientation _pageOrientation;
        private Duplex _duplex;
        private bool _collate;
        private int _copies;
        private PrintRange _printRange;
        private int _fromPage;
        private int _toPage;
        private bool _printToFile;
        private string _fileName;

        private Margins _margins;
        private bool _originAtMargins;
        private int _overlapArea;
        private bool _stretchToFit;

        private int _currentPage;
        private int _totalPages;

        private string _headerText;
        private Font _headerFont;
        private Color _headerColor;

        private string _footerText;
        private Font _footerFont;
        private Color _footerColor;

        private TextPlacement _pagenrPlacement;
        private TextAlignment _pagenrAlignment;
        private Font _pagenrFont;
        private Color _pagenrColor;

        private TextPlacement _datePlacement;
        private TextAlignment _dateAlignment;
        private Font _dateFont;
        private Color _dateColor;

        Font _font = new Font("Arial", 10, FontStyle.Regular);

        # endregion Members

        # region Category Labels

        //printer
        private const string catPrinter = "Printer";
        private const string catPrinterProperties = "Printer Details";
        //doc props
        private const string catDocument = "Sticker Roll";
        private const string catPageMargins = "Sticker Roll Margins";
        private const string catPageRange = "Sticker Roll Range";
        private const string catPaper = "Sticker Roll";
        //texts
        private const string catDocumentText = "Text Above (Header)";
        private const string catDocumentText2 = "Text Below (Footer)";
        private const string catTextPageNumber = "Text Page Number";
        private const string catTextPrintDate = "Text Print Date";

        # endregion Category Labels

        # region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PrintDocumentSettings"/> class.
        /// </summary>
        public PrintDocumentSettings()
        {
            SetDefaults();
        }

        # endregion Constructor

        # region Defaults

        /// <summary>
        /// Sets the defaults. Public access modifier for reset usage.
        /// </summary>
        public void SetDefaults()
        {
            _defaultPrinter = DefaultPrinter;

            if (String.IsNullOrEmpty(_defaultPrinter))
                return;

            PrinterSettings = new PrinterSettings();
            PageSettings = new PageSettings();

            SelectedPrinter = _defaultPrinter;
            PaperKind = System.Drawing.Printing.PaperKind.A4.ToString();
            PaperSourceKind = System.Drawing.Printing.PaperSourceKind.AutomaticFeed.ToString();
            PrinterResolutionKind = System.Drawing.Printing.PrinterResolutionKind.Medium.ToString();

            PageOrientation = PageOrientation.Portrait;
            Margins = new Margins(5,5,5,5);
            Duplex = System.Drawing.Printing.Duplex.Simplex.ToString();
            Collate = false;
            Copies = 1;
            TotalPages = 1;
            FromPage = 1;
            ToPage = 1;
            PrintRange = PrintRange.AllPages;
            PrintToFile = false;
            //FileName = String.Empty;

            OverlapArea = 10;
            StretchToFit = false;

            //HeaderText = "Document header";
            //HeaderFont = _font;
            //HeaderColor = Color.Blue;

            //FooterText = "Document footer";
            //FooterFont = _font;
            //FooterColor = Color.Blue;

            //PageNumberPlacement = TextPlacement.Footer;
            //PageNumberAlignment = TextAlignment.Right;
            //PageNumberFont = _font;
            //PageNumberColor = Color.Blue;

            //DatePlacement = TextPlacement.Footer;
            //DateAlignment = TextAlignment.Left;
            //DateFont = _font;
            //DateColor = Color.Blue;
        }

        # endregion Defaults

        # region Document properties

        /// <summary>
        /// Gets or sets the page settings.
        /// </summary>
        /// <value>The page settings.</value>
        [Category(catDocument)]
        [Browsable(false)]
        public PageSettings PageSettings
        {
            get { return _pageSettings; }
            set { _pageSettings = value; }
        }

        /// <summary>
        /// Gets or sets the paper orientation.
        /// </summary>
        /// <value>The paper orientation.</value>
        [Category(catDocument)]
        [DefaultValue(PageOrientation.Portrait)]
        [Description("Portrait or landscape orientation.")]
        [DisplayName("Page orientation")]
        public PageOrientation PageOrientation
        {
            get { return _pageOrientation; }
            set
            {
                _pageOrientation = value;
                _pageSettings.Landscape =
                    (_pageOrientation == PageOrientation.Landscape) ? true : false;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="PrintDocumentSettings"/> is duplex.
        /// </summary>
        /// <value><c>true</c> if duplex; otherwise, <c>false</c>.</value>
        [Category(catDocument)]
        [DefaultValue("Simplex")]
        [Description("Double-sided options: Simplex=single.")]
        [DisplayName("Print double-sided")]
        [TypeConverter(typeof(DuplexKindSelectionConverter))]        
        public string Duplex
        {
            get
            {
                return _duplex.ToString();
            }
            set
            {
                if (value != null)
                {
                    _duplex = (Duplex) Enum.Parse(typeof (Duplex), value.ToString());
                    _printerSettings.Duplex = _duplex;
                }
            }
        }

        /// <summary>
        /// Gets or sets the copies.
        /// </summary>
        /// <value>The copies.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:DoNotPassLiteralsAsLocalizedParameters", MessageId = "System.Windows.Forms.MessageBox.Show(System.String,System.String,System.Windows.Forms.MessageBoxButtons,System.Windows.Forms.MessageBoxIcon,System.Windows.Forms.MessageBoxDefaultButton,System.Windows.Forms.MessageBoxOptions)"), Category(catDocument)]
        [DefaultValue(1)]
        [Description("Number of copies that should be printed.")]
        [DisplayName("Print amount")]
        public int Copies
        {
            get { return _copies; }
            set
            {
                if (value < 1 || value > 10)
                {
                    MessageBox.Show("Value must be between 1 and 10",
                        "Print preview...",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error,
                        MessageBoxDefaultButton.Button1,
                        0);
                }
                else
                {
                    _copies = value;
                    _printerSettings.Copies = (short)value;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="PrintDocumentSettings"/> is collate.
        /// </summary>
        /// <value><c>true</c> if collate; otherwise, <c>false</c>.</value>
        [Category(catDocument)]
        [DefaultValue(false)]
        [Description("In case of more than one copy, collation will keep page numbers together.")]
        [DisplayName("Print collation")]
        public bool Collate
        {
            get { return _collate; }
            set
            {
                _collate = value;
                _printerSettings.Collate = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [print to file].
        /// </summary>
        /// <value><c>true</c> if [print to file]; otherwise, <c>false</c>.</value>
        [Category(catDocument)]
        [DefaultValue(false)]
        [DisplayName("Print to file")]
        [Description("Indicates whether the document should be printed as a bitmap to a file.")]
        public bool PrintToFile
        {
            get { return _printToFile; }
            set
            {
                _printToFile = value;
                _printerSettings.PrintToFile = _printToFile;
            }
        }

        /// <summary>
        /// Gets or sets the name of the file.
        /// </summary>
        /// <value>The name of the file.</value>
        [Category(catDocument)]
        [DisplayName("Print to file name")]
        [Description("Supply a bitmap (bmp) file to print to.")]
        [Editor(typeof(FileNameEditor), typeof(UITypeEditor))]
        public string FileName
        {
            get { return _fileName; }
            set
            {
                _fileName = value;
                _printerSettings.PrintFileName = _fileName;
            }
        }

        # endregion Document properties

        # region Margins

        /// <summary>
        /// Gets or sets a value indicating whether [origin at margins].
        /// </summary>
        /// <value><c>true</c> if [origin at margins]; otherwise, <c>false</c>.</value>
        [Category(catPageMargins)]
        [DefaultValue(true)]
        [DisplayName("Origin at margins")]
        [Description("")]
        [Browsable(false)]
        public bool OriginAtMargins
        {
            get { return _originAtMargins; }
            set { _originAtMargins = value; }
        }

        // TODO supply a localized variant
        /// <summary>
        /// Gets or sets the margins.
        /// </summary>
        /// <value>The margins.</value>
        [Category(catPageMargins)]
        [Description("Margin measurements in hundredths of an inch.")]
        [TypeConverter(typeof(MarginsConverter))]
        [DisplayName("Paper margins")]
        [DefaultValue("0; 0; 0; 0")]
        public Margins Margins
        {
            get { return _margins; }
            set
            {
                _margins = value;
                _pageSettings.Margins = value;
            }
        }

        /// <summary>
        /// Gets or sets the overlap area.
        /// </summary>
        /// <value>The overlap area.</value>
        [Category(catPageMargins)]
        [Description("Page overlap size in pixels.")]
        [DisplayName("Page overlap")]
        [DefaultValue(10)]
        public int OverlapArea
        {
            get { return _overlapArea; }
            set { _overlapArea = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [stretch to fit].
        /// </summary>
        /// <value><c>true</c> if [stretch to fit]; otherwise, <c>false</c>.</value>
        [Category(catPageMargins)]
        [Description("Stretch control to fit page.")]
        [DisplayName("Stretch to fit")]
        [DefaultValue(true)]
        public bool StretchToFit
        {
            get { return _stretchToFit; }
            set { _stretchToFit = value; }
        }

        # endregion Margins

        # region Pages

        /// <summary>
        /// Gets or sets the print range.
        /// </summary>
        /// <value>The print range.</value>
        [Category(catPageRange)]
        [DefaultValue(PrintRange.AllPages)]
        [Description("The range of the document that should be printed.")]
        [DisplayName("Range choice")]
        public PrintRange PrintRange
        {
            get { return _printRange; }
            set
            {
                _printRange = value;
                _printerSettings.PrintRange = value;

                switch (value)
                {
                    case PrintRange.AllPages:
                        FromPage = 1;
                        ToPage = TotalPages;
                        break;
                    case PrintRange.CurrentPage:
                        FromPage = CurrentPage;
                        ToPage = FromPage;
                        break;
                    case PrintRange.Selection:
                        FromPage = 1;
                        ToPage = FromPage;
                        break;
                    case PrintRange.SomePages:
                        //custom
                        break;
                }
            }
        }

        /// <summary>
        /// Gets or sets the page to print from.
        /// </summary>
        /// <value>From page.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:DoNotPassLiteralsAsLocalizedParameters", MessageId = "System.Windows.Forms.MessageBox.Show(System.String,System.String,System.Windows.Forms.MessageBoxButtons,System.Windows.Forms.MessageBoxIcon,System.Windows.Forms.MessageBoxDefaultButton,System.Windows.Forms.MessageBoxOptions)"), Category(catPageRange)]
        [DefaultValue(1)]
        [DisplayName("Range from page")]
        [Description("Print document starting from this page number.")]
        public int FromPage
        {
            get
            {
                return _fromPage;
            }
            set
            {
                if (value < 1 || value > TotalPages)
                {
                    MessageBox.Show("Value must be between 1 and " + TotalPages,
                        "Print preview...",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error,
                        MessageBoxDefaultButton.Button1,
                        0);
                }
                else
                {
                    _fromPage = value;
                    _printerSettings.FromPage = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the page to print until (including).
        /// </summary>
        /// <value>To page.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:DoNotPassLiteralsAsLocalizedParameters", MessageId = "System.Windows.Forms.MessageBox.Show(System.String,System.String,System.Windows.Forms.MessageBoxButtons,System.Windows.Forms.MessageBoxIcon,System.Windows.Forms.MessageBoxDefaultButton,System.Windows.Forms.MessageBoxOptions)"), Category(catPageRange)]
        [DefaultValue(1)]
        [DisplayName("Range to page")]
        [Description("Print document up to and including this page number.")]
        public int ToPage
        {
            get
            {
                return _toPage;
            }
            set
            {
                if (value < 1 || value > TotalPages)
                {
                    MessageBox.Show("Value must be between 1 and " + TotalPages,
                        "Print preview...",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error,
                        MessageBoxDefaultButton.Button1,
                        0);                     
                }
                else
                {
                    _toPage = value;
                    _printerSettings.ToPage = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the current page.
        /// </summary>
        /// <value>The current page.</value>
        [Category(catPageRange)]
        [ReadOnly(true)]
        [Browsable(false)]
        public int CurrentPage
        {
            get { return _currentPage; }
            set
            {
                if (_printRange == PrintRange.CurrentPage)
                {
                    _fromPage = _currentPage;
                    _toPage = _currentPage;
                }
                _currentPage = value;
            }
        }

        /// <summary>
        /// Gets or sets the total pages.
        /// </summary>
        /// <value>The total pages.</value>
        [Category(catPageRange)]
        [DefaultValue(0)]
        [DisplayName("Total pages")]
        [ReadOnly(true)]
        [Browsable(false)]
        public int TotalPages
        {
            get { return _totalPages; }
            set
            {
                _totalPages = value;
                if (_printRange == PrintRange.AllPages)
                {
                    _fromPage = 1;
                    _toPage = _totalPages;
                }
            }
        }

        # endregion Pages

        # region Printer

        /// <summary>
        /// Gets or sets the printer settings.
        /// </summary>
        /// <value>The printer settings.</value>
        [Category(catPrinter)]
        [Browsable(false)]
        [RefreshProperties(RefreshProperties.All)]
        public PrinterSettings PrinterSettings
        {
            get
            {
                return _printerSettings;
            }
            set
            {
                _printerSettings = value;
            }
        }

        /// <summary>
        /// Gets the default printer.
        /// </summary>
        /// <value>The default printer.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:DoNotPassLiteralsAsLocalizedParameters", MessageId = "System.Windows.Forms.MessageBox.Show(System.String,System.String,System.Windows.Forms.MessageBoxButtons,System.Windows.Forms.MessageBoxIcon,System.Windows.Forms.MessageBoxDefaultButton,System.Windows.Forms.MessageBoxOptions)"), Category(catPrinter)]
        [Browsable(false)]
        public string DefaultPrinter
        {
            get
            {
                try
                {
                    foreach (string printer in PrinterSettings.InstalledPrinters)
                    {
                        PrinterSettings printerSettings = new PrinterSettings();
                        printerSettings.PrinterName = printer;
                        if (printerSettings.IsDefaultPrinter)
                        {
                            return printer;
                        }
                    }
                }
                catch (InvalidPrinterException)
                {
                    MessageBox.Show("The default printer is invalid.",
                        "Print preview...",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error,
                        MessageBoxDefaultButton.Button1,
                        0);
                }
                return String.Empty;
            }
        }


        /// <summary>
        /// Gets or sets the selected printer.
        /// </summary>
        /// <value>The selected printer.</value>
        [Category(catPrinter)]
        [TypeConverter(typeof(PrinterSelectionConverter))]
        [RefreshProperties(RefreshProperties.All)]
        [Description("Choice for the selected printer.")]
        [DisplayName("Selected printer")]
        public string SelectedPrinter
        {
            get { return _selectedPrinter; }
            set
            {
                _selectedPrinter = value;
                _printerSettings.PrinterName = value;
            }
        }

        # endregion Printer

        # region Paper

        /// <summary>
        /// Gets or sets the kind of the paper.
        /// </summary>
        /// <value>The kind of the paper.</value>
        [Category(catPaper)]
        [DefaultValue("A4")]
        [Description("Sticker Roll Size kind for selected printer.")]
        [DisplayName("Sticker Roll Size kind")]
        [TypeConverter(typeof(PaperKindSelectionConverter))]
        public string PaperKind
        {
            get
            {
                return ValidatePaperSize(_paperKind).ToString();
            }
            set
            {
                if (value != null)
                {
                    _paperKind = (PaperKind) Enum.Parse(typeof (PaperKind), value.ToString());
                    _pageSettings.PaperSize = SetPaperSize(_paperKind);
                }
            }
        }

        /// <summary>
        /// Gets or sets the kind of the paper source.
        /// </summary>
        /// <value>The kind of the paper source.</value>
        [Category(catPaper)]
        [DefaultValue("AutomaticFeed")]
        [Description("Paper source tray for selected printer.")]
        [DisplayName("Paper source tray")]
        [TypeConverter(typeof(PaperSourceKindSelectionConverter))]
        public string PaperSourceKind
        {
            get
            {
                return ValidatePaperSource(_paperSourceKind).ToString();
            }
            set
            {
                if (value != null)
                {
                    _paperSourceKind = (PaperSourceKind) Enum.Parse(typeof (PaperSourceKind), value.ToString());
                    _pageSettings.PaperSource = SetPaperSource(_paperSourceKind);
                }
            }
        }

        /// <summary>
        /// Gets or sets the kind of the printer resolution.
        /// </summary>
        /// <value>The kind of the printer resolution.</value>
        [Category(catPaper)]
        [DefaultValue("Medium")]
        [Description("Resolution choice for selected printer.")]
        [DisplayName("Printer resolution")]
        [TypeConverter(typeof(PrinterResolutionKindSelectionConverter))]
        public string PrinterResolutionKind
        {
            get
            {
                return ValidatePrinterResolution(_printerResolutionKind).ToString();
            }
            set
            {
                if (value != null)
                {
                    _printerResolutionKind =
                        (PrinterResolutionKind) Enum.Parse(typeof (PrinterResolutionKind), value.ToString());
                    _pageSettings.PrinterResolution = SetPrinterResolution(_printerResolutionKind);
                }
            }
        }

        private PaperSize SetPaperSize(PaperKind paperKind)
        {
            foreach (PaperSize paperSize in _printerSettings.PaperSizes)
            {
                if (paperSize.Kind == paperKind)
                    return paperSize;
            }
            return new PaperSize();
        }

        private PaperKind ValidatePaperSize(PaperKind paperKind)
        {
            foreach (PaperSize paperSize in _printerSettings.PaperSizes)
            {
                if (paperSize.Kind == paperKind)
                    return paperKind;
            }
            if (_printerSettings.PaperSizes.Count > 0)
            {
                return _printerSettings.PaperSizes[0].Kind;
            }
            return new PaperSize().Kind;
        }

        private PaperSource SetPaperSource(PaperSourceKind paperSourceKind)
        {
            foreach (PaperSource paperSource in _printerSettings.PaperSources)
            {
                if (paperSource.Kind == paperSourceKind)
                    return paperSource;
            }
            return new PaperSource();
        }

        private PaperSourceKind ValidatePaperSource(PaperSourceKind paperSourceKind)
        {
            foreach (PaperSource paperSource in _printerSettings.PaperSources)
            {
                if (paperSource.Kind == paperSourceKind)
                    return paperSourceKind;
            }
            if (_printerSettings.PaperSources.Count > 0)
            {
                return _printerSettings.PaperSources[0].Kind;
            }
            return new PaperSource().Kind;
        }

        private PrinterResolution SetPrinterResolution(PrinterResolutionKind printerResolutionKind)
        {
            foreach (PrinterResolution printerResolution in _printerSettings.PrinterResolutions)
            {
                if (printerResolution.Kind == printerResolutionKind)
                    return printerResolution;
            }
            return new PrinterResolution();
        }

        private PrinterResolutionKind ValidatePrinterResolution(PrinterResolutionKind printerResolutionKind)
        {
            foreach (PrinterResolution printerResolution in _printerSettings.PrinterResolutions)
            {
                if (printerResolution.Kind == printerResolutionKind)
                    return printerResolutionKind;
            }
            if (_printerSettings.PrinterResolutions.Count > 0)
            {
                return _printerSettings.PrinterResolutions[0].Kind;
            }
            return new PrinterResolution().Kind;
        }

        # endregion Paper

        # region Printer details

        /// <summary>
        /// Gets a value indicating whether [supports colour].
        /// </summary>
        /// <value><c>true</c> if [supports colour]; otherwise, <c>false</c>.</value>
        [Category(catPrinterProperties)]
        [DisplayName("Supports colour")]
        [Browsable(false)]
        public bool SupportsColour
        {
            get { return _printerSettings.SupportsColor; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance can duplex.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance can duplex; otherwise, <c>false</c>.
        /// </value>
        [Category(catPrinterProperties)]
        [DisplayName("Double-sided printing")]
        [Browsable(false)]
        public bool CanDuplex
        {
            get { return _printerSettings.CanDuplex; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is plotter.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is plotter; otherwise, <c>false</c>.
        /// </value>
        [Category(catPrinterProperties)]
        [Browsable(false)]
        public bool IsPlotter
        {
            get { return _printerSettings.IsPlotter; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is valid.
        /// </summary>
        /// <value><c>true</c> if this instance is valid; otherwise, <c>false</c>.</value>
        [Category(catPrinterProperties)]
        [Browsable(false)]
        public bool IsValid
        {
            get { return _printerSettings.IsValid; }
        }

        # endregion Printer details

        # region Texts

        //#region Header Text

        ///// <summary>
        ///// Gets or sets the header text.
        ///// </summary>
        ///// <value>The header text.</value>
        //[Category(catDocumentText)]
        //[Description("Text for the document header (e.g. document name).")]
        //[DisplayName("Document header text")]
        //[DefaultValue("Document")]
        //public string HeaderText
        //{
        //    get { return _headerText;}
        //    set { _headerText = value; }
        //}

        ///// <summary>
        ///// Gets or sets the header font.
        ///// </summary>
        ///// <value>The header font.</value>
        //[Category(catDocumentText)]
        //[Description("Font for the document header.")]
        //[DisplayName("Font")]
        //public Font HeaderFont
        //{
        //    get { return _headerFont; }
        //    set { _headerFont = value; }
        //}

        ///// <summary>
        ///// Gets or sets the color of the header.
        ///// </summary>
        ///// <value>The color of the header.</value>
        //[Category(catDocumentText)]
        //[Description("Color for the document header.")]
        //[DisplayName("Font color")]
        //public Color HeaderColor
        //{
        //    get { return _headerColor; }
        //    set { _headerColor = value; }
        //}

        //# endregion Header Text

        //# region Pagenr Text

        ///// <summary>
        ///// Gets or sets the page nr placement.
        ///// </summary>
        ///// <value>The page nr placement.</value>
        //[Category(catTextPageNumber)]
        //[Description("Placement of the page number.")]
        //[DisplayName("Placing")]
        //[DefaultValue(TextPlacement.Footer)]
        //public TextPlacement PageNumberPlacement
        //{
        //    get { return _pagenrPlacement; }
        //    set { _pagenrPlacement = value; }
        //}

        ///// <summary>
        ///// Gets or sets the page nr alignment.
        ///// </summary>
        ///// <value>The page nr alignment.</value>
        //[Category(catTextPageNumber)]
        //[Description("Alignment of the page number.")]
        //[DisplayName("Alignment")]
        //[DefaultValue(TextAlignment.Right)]
        //public TextAlignment PageNumberAlignment
        //{
        //    get { return _pagenrAlignment; }
        //    set { _pagenrAlignment = value; }
        //}

        ///// <summary>
        ///// Gets or sets the page nr font.
        ///// </summary>
        ///// <value>The page nr font.</value>
        //[Category(catTextPageNumber)]
        //[Description("Font for the page number.")]
        //[DisplayName("Font")]
        //[DefaultValue("Arial")]
        //public Font PageNumberFont
        //{
        //    get { return _pagenrFont; }
        //    set { _pagenrFont = value; }
        //}

        ///// <summary>
        ///// Gets or sets the color of the page nr.
        ///// </summary>
        ///// <value>The color of the page nr.</value>
        //[Category(catTextPageNumber)]
        //[Description("Color for the page number.")]
        //[DisplayName("Font color")]
        //public Color PageNumberColor
        //{
        //    get { return _pagenrColor; }
        //    set { _pagenrColor = value; }
        //}

        //# endregion Pagenr Text

        //# region Date

        ///// <summary>
        ///// Gets or sets the date placement.
        ///// </summary>
        ///// <value>The date placement.</value>
        //[Category(catTextPrintDate)]
        //[Description("Placement of the print date.")]
        //[DisplayName("Placing")]
        //[DefaultValue(TextPlacement.Footer)]
        //public TextPlacement DatePlacement
        //{
        //    get { return _datePlacement; }
        //    set { _datePlacement = value; }
        //}

        ///// <summary>
        ///// Gets or sets the date alignment.
        ///// </summary>
        ///// <value>The date alignment.</value>
        //[Category(catTextPrintDate)]
        //[Description("Alignment of the print date.")]
        //[DisplayName("Alignment")]
        //[DefaultValue(TextAlignment.Left)]
        //public TextAlignment DateAlignment
        //{
        //    get { return _dateAlignment; }
        //    set { _dateAlignment = value; }
        //}

        ///// <summary>
        ///// Gets or sets the date font.
        ///// </summary>
        ///// <value>The date font.</value>
        //[Category(catTextPrintDate)]
        //[Description("Font for the print date.")]
        //[DisplayName("Font")]
        //[DefaultValue("Arial")]
        //public Font DateFont
        //{
        //    get { return _dateFont; }
        //    set { _dateFont = value; }
        //}

        ///// <summary>
        ///// Gets or sets the color of the date.
        ///// </summary>
        ///// <value>The color of the date.</value>
        //[Category(catTextPrintDate)]
        //[Description("Color for the print date.")]
        //[DisplayName("Font color")]
        //public Color DateColor
        //{
        //    get { return _dateColor; }
        //    set { _dateColor = value; }
        //}

        //# endregion Date

        //# region Footer text

        ///// <summary>
        ///// Gets or sets the footer text.
        ///// </summary>
        ///// <value>The footer text.</value>
        //[Category(catDocumentText2)]
        //[Description("Text for the document footer.")]
        //[DisplayName("Document footer text")]
        //[DefaultValue("")]
        //public string FooterText
        //{
        //    get { return _footerText; }
        //    set { _footerText = value; }
        //}

        ///// <summary>
        ///// Gets or sets the footer font.
        ///// </summary>
        ///// <value>The footer font.</value>
        //[Category(catDocumentText2)]
        //[Description("Font for the document footer.")]
        //[DisplayName("Font")]
        //[DefaultValue("Arial")]
        //public Font FooterFont
        //{
        //    get { return _footerFont; }
        //    set { _footerFont = value; }
        //}

        ///// <summary>
        ///// Gets or sets the color of the footer.
        ///// </summary>
        ///// <value>The color of the footer.</value>
        //[Category(catDocumentText2)]
        //[Description("Color for the document footer.")]
        //[DisplayName("Font color")]
        //public Color FooterColor
        //{
        //    get { return _footerColor; }
        //    set { _footerColor = value; }
        //}

        //# endregion Footer text

        # endregion Texts

        # region Helpers

        /// <summary>
        /// Formats the date time to an unambiguous presentation.
        /// </summary>
        /// <param name="dateTime">The date time.</param>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.String.Format(System.String,System.Object)"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
        public string FormatDateTime(DateTime dateTime)
        {
            string output = String.Format("{0:D2}", dateTime.Day) + "-" +
                            GetDtgMonth(dateTime.Month) + "-" +
                            String.Format("{0:D2}", dateTime.Year) + " " +
                            String.Format("{0:D2}", dateTime.Hour) + ":" +
                            String.Format("{0:D2}", dateTime.Minute);
            return output;
        }

        private static string GetDtgMonth(int month)
        {
            string dtgMonth = String.Empty;
            switch (month)
            {
                case 1:
                    dtgMonth = "JAN";
                    break;
                case 2:
                    dtgMonth = "FEB";
                    break;
                case 3:
                    dtgMonth = "MAR";
                    break;
                case 4:
                    dtgMonth = "APR";
                    break;
                case 5:
                    dtgMonth = "MAY";
                    break;
                case 6:
                    dtgMonth = "JUN";
                    break;
                case 7:
                    dtgMonth = "JUL";
                    break;
                case 8:
                    dtgMonth = "AUG";
                    break;
                case 9:
                    dtgMonth = "SEP";
                    break;
                case 10:
                    dtgMonth = "OCT";
                    break;
                case 11:
                    dtgMonth = "NOV";
                    break;
                case 12:
                    dtgMonth = "DEC";
                    break;
            }

            return dtgMonth;
        }


        # endregion Helpers
    
        # region Dispose

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                _font.Dispose();
            }
        }

        # endregion Dispose

    }
}
