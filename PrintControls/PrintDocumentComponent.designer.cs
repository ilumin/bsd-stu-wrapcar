namespace PrintControl
{
    /// <summary>
    /// PrintDocumentComponent
    /// </summary>
    partial class PrintDocumentComponent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                try { components.Dispose(); }
                finally { base.Dispose(disposing);}
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            //rig event handlers
            this.BeginPrint += new System.Drawing.Printing.PrintEventHandler(OnBeginPrint);
            this.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(OnPrintPage);
            this.EndPrint += new System.Drawing.Printing.PrintEventHandler(OnEndPrint);
        }

        #endregion

    }
}
