﻿namespace WrapCar._80CarInOut
{
    partial class CarInOut
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.btSave2 = new System.Windows.Forms.Button();
            this.btSave1 = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.mainpanel = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.pnlOutput = new System.Windows.Forms.Panel();
            this.label47 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.tb_app_search = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.lbl_app_search = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.mainpanel.SuspendLayout();
            this.pnlOutput.SuspendLayout();
            this.SuspendLayout();
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(281, 66);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(54, 18);
            this.label38.TabIndex = 10;
            this.label38.Text = "label38";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(282, 43);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(54, 18);
            this.label37.TabIndex = 9;
            this.label37.Text = "label37";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(71, 312);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(79, 18);
            this.label36.TabIndex = 8;
            this.label36.Text = "ผู้ส่งมอบรถ :";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(46, 256);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(104, 18);
            this.label35.TabIndex = 7;
            this.label35.Text = "วันที่นำรถเข้ามา :";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(61, 104);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(89, 18);
            this.label34.TabIndex = 6;
            this.label34.Text = "รหัสนัดหมาย :";
            // 
            // btSave2
            // 
            this.btSave2.Location = new System.Drawing.Point(148, 364);
            this.btSave2.Name = "btSave2";
            this.btSave2.Size = new System.Drawing.Size(226, 26);
            this.btSave2.TabIndex = 28;
            this.btSave2.Text = "บันทึกและพิมพ์ใบคืนรถ";
            this.btSave2.UseVisualStyleBackColor = true;
            this.btSave2.Click += new System.EventHandler(this.btSave2_Click);
            // 
            // btSave1
            // 
            this.btSave1.Location = new System.Drawing.Point(148, 253);
            this.btSave1.Name = "btSave1";
            this.btSave1.Size = new System.Drawing.Size(226, 24);
            this.btSave1.TabIndex = 27;
            this.btSave1.Text = "บันทึกและพิมพ์ใบรับรถ";
            this.btSave1.UseVisualStyleBackColor = true;
            this.btSave1.Click += new System.EventHandler(this.btSave1_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(189, 335);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(41, 13);
            this.label24.TabIndex = 22;
            this.label24.Text = "label24";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(189, 301);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(41, 13);
            this.label23.TabIndex = 21;
            this.label23.Text = "label23";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(156, 335);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(10, 13);
            this.label21.TabIndex = 19;
            this.label21.Text = ":";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(156, 301);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(10, 13);
            this.label20.TabIndex = 18;
            this.label20.Text = ":";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(156, 312);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(54, 18);
            this.label44.TabIndex = 16;
            this.label44.Text = "label44";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(156, 104);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(54, 18);
            this.label42.TabIndex = 14;
            this.label42.Text = "label42";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(156, 198);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(54, 18);
            this.label41.TabIndex = 13;
            this.label41.Text = "label41";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(156, 169);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(54, 18);
            this.label40.TabIndex = 12;
            this.label40.Text = "label40";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(156, 138);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(54, 18);
            this.label39.TabIndex = 11;
            this.label39.Text = "label39";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(37, 335);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(81, 13);
            this.label19.TabIndex = 17;
            this.label19.Text = "พนักงานที่รับรถ";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(35, 301);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(82, 13);
            this.label18.TabIndex = 16;
            this.label18.Text = "วันที่นำรถเข้ามา";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(191, 226);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(41, 13);
            this.label16.TabIndex = 14;
            this.label16.Text = "label16";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(191, 186);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 13);
            this.label15.TabIndex = 13;
            this.label15.Text = "label15";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(191, 109);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 13);
            this.label13.TabIndex = 11;
            this.label13.Text = "label13";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(191, 73);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 13);
            this.label12.TabIndex = 10;
            this.label12.Text = "label12";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(156, 256);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(54, 18);
            this.label43.TabIndex = 15;
            this.label43.Text = "label43";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(94, 198);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(56, 18);
            this.label33.TabIndex = 5;
            this.label33.Text = "รถยนต์ :";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(58, 169);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(92, 18);
            this.label32.TabIndex = 4;
            this.label32.Text = "ชื่อ - นามสกุล :";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(37, 138);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(113, 18);
            this.label31.TabIndex = 3;
            this.label31.Text = "รหัสรายการสั่งซื้อ :";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(234, 66);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(41, 18);
            this.label30.TabIndex = 2;
            this.label30.Text = "วันที่ :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(191, 149);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 13);
            this.label14.TabIndex = 12;
            this.label14.Text = "label14";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(39, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(156, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "ใบเสร็จรับเงิน";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(158, 226);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(10, 13);
            this.label48.TabIndex = 9;
            this.label48.Text = ":";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(158, 186);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(10, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = ":";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(158, 149);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(10, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = ":";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(158, 109);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(10, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = ":";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(158, 74);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(10, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = ":";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(35, 226);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "วันที่นัดรับรถ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 186);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "วันที่นัดหมาย";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(32, 149);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "รถยนต์";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(32, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "ชื่อ - นามสกุล";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "รหัวรายการสั่งซื้อ";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(231, 43);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(44, 18);
            this.label29.TabIndex = 1;
            this.label29.Text = "เลขที่ :";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(31, 22);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(268, 46);
            this.label45.TabIndex = 36;
            this.label45.Text = "ระบบรับ - คืนรถ";
            // 
            // mainpanel
            // 
            this.mainpanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mainpanel.Controls.Add(this.label49);
            this.mainpanel.Controls.Add(this.label17);
            this.mainpanel.Controls.Add(this.label25);
            this.mainpanel.Controls.Add(this.label26);
            this.mainpanel.Controls.Add(this.label27);
            this.mainpanel.Controls.Add(this.label28);
            this.mainpanel.Controls.Add(this.label47);
            this.mainpanel.Controls.Add(this.label11);
            this.mainpanel.Controls.Add(this.label46);
            this.mainpanel.Controls.Add(this.label22);
            this.mainpanel.Controls.Add(this.btSave2);
            this.mainpanel.Controls.Add(this.btSave1);
            this.mainpanel.Controls.Add(this.label24);
            this.mainpanel.Controls.Add(this.label23);
            this.mainpanel.Controls.Add(this.label21);
            this.mainpanel.Controls.Add(this.label20);
            this.mainpanel.Controls.Add(this.label19);
            this.mainpanel.Controls.Add(this.label18);
            this.mainpanel.Controls.Add(this.label16);
            this.mainpanel.Controls.Add(this.label15);
            this.mainpanel.Controls.Add(this.label14);
            this.mainpanel.Controls.Add(this.label13);
            this.mainpanel.Controls.Add(this.label12);
            this.mainpanel.Controls.Add(this.label48);
            this.mainpanel.Controls.Add(this.label10);
            this.mainpanel.Controls.Add(this.label9);
            this.mainpanel.Controls.Add(this.label8);
            this.mainpanel.Controls.Add(this.label7);
            this.mainpanel.Controls.Add(this.label6);
            this.mainpanel.Controls.Add(this.label5);
            this.mainpanel.Controls.Add(this.label4);
            this.mainpanel.Controls.Add(this.label3);
            this.mainpanel.Controls.Add(this.label2);
            this.mainpanel.Location = new System.Drawing.Point(167, 176);
            this.mainpanel.Name = "mainpanel";
            this.mainpanel.Size = new System.Drawing.Size(572, 477);
            this.mainpanel.TabIndex = 34;
            this.mainpanel.Visible = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(194, 441);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(41, 13);
            this.label17.TabIndex = 39;
            this.label17.Text = "label17";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(194, 407);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(41, 13);
            this.label25.TabIndex = 38;
            this.label25.Text = "label25";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(161, 441);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(10, 13);
            this.label26.TabIndex = 37;
            this.label26.Text = ":";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(161, 407);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(10, 13);
            this.label27.TabIndex = 36;
            this.label27.Text = ":";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(42, 441);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(86, 13);
            this.label28.TabIndex = 35;
            this.label28.Text = "พนักงานที่ส่งมอบ";
            // 
            // pnlOutput
            // 
            this.pnlOutput.BackColor = System.Drawing.Color.White;
            this.pnlOutput.Controls.Add(this.label44);
            this.pnlOutput.Controls.Add(this.label43);
            this.pnlOutput.Controls.Add(this.label42);
            this.pnlOutput.Controls.Add(this.label41);
            this.pnlOutput.Controls.Add(this.label40);
            this.pnlOutput.Controls.Add(this.label39);
            this.pnlOutput.Controls.Add(this.label38);
            this.pnlOutput.Controls.Add(this.label37);
            this.pnlOutput.Controls.Add(this.label36);
            this.pnlOutput.Controls.Add(this.label35);
            this.pnlOutput.Controls.Add(this.label34);
            this.pnlOutput.Controls.Add(this.label33);
            this.pnlOutput.Controls.Add(this.label32);
            this.pnlOutput.Controls.Add(this.label31);
            this.pnlOutput.Controls.Add(this.label30);
            this.pnlOutput.Controls.Add(this.label29);
            this.pnlOutput.Controls.Add(this.label1);
            this.pnlOutput.Location = new System.Drawing.Point(745, 210);
            this.pnlOutput.Name = "pnlOutput";
            this.pnlOutput.Size = new System.Drawing.Size(347, 376);
            this.pnlOutput.TabIndex = 35;
            this.pnlOutput.Visible = false;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(40, 407);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(72, 13);
            this.label47.TabIndex = 34;
            this.label47.Text = "วันที่ส่งมอบรถ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(191, 33);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 13);
            this.label11.TabIndex = 33;
            this.label11.Text = "label11";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(158, 34);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(10, 13);
            this.label46.TabIndex = 32;
            this.label46.Text = ":";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(32, 34);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(67, 13);
            this.label22.TabIndex = 31;
            this.label22.Text = "รหัวนัดหมาย";
            // 
            // tb_app_search
            // 
            this.tb_app_search.Location = new System.Drawing.Point(350, 107);
            this.tb_app_search.Name = "tb_app_search";
            this.tb_app_search.Size = new System.Drawing.Size(157, 20);
            this.tb_app_search.TabIndex = 33;
            this.tb_app_search.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(525, 105);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 32;
            this.button1.Text = "ค้นหา";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lbl_app_search
            // 
            this.lbl_app_search.AutoSize = true;
            this.lbl_app_search.Location = new System.Drawing.Point(258, 110);
            this.lbl_app_search.Name = "lbl_app_search";
            this.lbl_app_search.Size = new System.Drawing.Size(67, 13);
            this.lbl_app_search.TabIndex = 31;
            this.lbl_app_search.Text = "รหัสนัดหมาย";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Red;
            this.label49.Location = new System.Drawing.Point(169, 396);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(187, 13);
            this.label49.TabIndex = 40;
            this.label49.Text = "อยู่ระหว่างดำเนินการจิดสติกเกอร์";
            // 
            // CarInOut
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(816, 671);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.mainpanel);
            this.Controls.Add(this.tb_app_search);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lbl_app_search);
            this.Controls.Add(this.pnlOutput);
            this.Name = "CarInOut";
            this.Text = "CarIn";
            this.mainpanel.ResumeLayout(false);
            this.mainpanel.PerformLayout();
            this.pnlOutput.ResumeLayout(false);
            this.pnlOutput.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Button btSave2;
        private System.Windows.Forms.Button btSave1;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Panel mainpanel;
        private System.Windows.Forms.Panel pnlOutput;
        private System.Windows.Forms.TextBox tb_app_search;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lbl_app_search;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label49;
    }
}