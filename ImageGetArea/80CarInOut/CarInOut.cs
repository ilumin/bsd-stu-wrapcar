﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WrapCar_Connection;
using WrapCar_DataModel;
using PrintControl;
using WrapCar._50Payment;

namespace WrapCar._80CarInOut
{
    public partial class CarInOut : Form
    {
        CarInOutDAO  dao = new CarInOutDAO();
        APPCarData APPCarData = new APPCarData();
        CarInOutData CarInOutData = new CarInOutData();

        public CarInOut()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            searchAppointment();
        }

        private void searchAppointment()
        {
            //var paymentDAO = new PaymentDao();
            //var orderdata = new OrderData();
            //paymentdata = new PaymentData();
            APPCarData = dao.getAPPCarData(Convert.ToInt32(tb_app_search.Text));
            

            if (APPCarData != null)
            {
                CarInOutData = dao.getCarInOutData(APPCarData.APP_ID);
                mainpanel.Visible = true;

                label11.Text = APPCarData.APP_ID.ToString() ;
                label12.Text = APPCarData.ORDER_H_ID.ToString();
                label13.Text = APPCarData.TITLE.ToString() + " " + APPCarData.CUS_FNAME.ToString() + " " + APPCarData.CUS_LNAME.ToString();
                label14.Text = APPCarData.CAR_BRAND.ToString() + " " + APPCarData.CAR_MODEL.ToString() + " ปี " + APPCarData.CAR_YEAR.ToString() + " สี " + APPCarData.CAR_COLOR.ToString() + " ทะเบียน " + APPCarData.CAR_LICENSE_NO.ToString();
                label15.Text = APPCarData.APP_DATETIME_START.ToString();
                label16.Text = APPCarData.APP_DATETIME_FINISH.ToString();

                label23.Text = "";
                label24.Text = "";

                label25.Text = "";
                label17.Text = "";


                if (APPCarData.STATUS_APP == "WAITING")
                {
                    btSave1.Visible = true;

                    label18.Visible = false;
                    label20.Visible = false;
                    label23.Visible = false;

                    label19.Visible = false;
                    label21.Visible = false;
                    label24.Visible = false;

                    
                    
                    btSave2.Visible = false;

                    label47.Visible = false;
                    label27.Visible = false;
                    label25.Visible = false;

                    label28.Visible = false;
                    label26.Visible = false;
                    label17.Visible = false;

                    

                }
                else if (APPCarData.STATUS_APP == "IN PROGRESS")
                {
                    btSave1.Visible = false;

                    label18.Visible = true;
                    label20.Visible = true;
                    label23.Visible = true;

                    label19.Visible = true;
                    label21.Visible = true;
                    label24.Visible = true;

                    label23.Text = CarInOutData.CARIN_DATE.ToString();
                    label24.Text = CarInOutData.CARIN_EMP_TITLE + " " + CarInOutData.CARIN_EMP_FNAME + " " + CarInOutData.CARIN_EMP_LNAME;

                    btSave2.Visible = true;
                    if (isFinish(dao.getListTask(APPCarData.APP_ID)))
                    {

                        btSave2.Enabled = true;
                        label49.Visible = false;
                    }
                    else
                    {
                        btSave2.Enabled = false;
                        label49.Visible = true;
                    }

                    label47.Visible = false;
                    label27.Visible = false;
                    label25.Visible = false;

                    label28.Visible = false;
                    label26.Visible = false;
                    label17.Visible = false;

                    
                }
                else if (APPCarData.STATUS_APP == "APPROVE")
                {
                    btSave1.Visible = false;

                    label18.Visible = true;
                    label20.Visible = true;
                    label23.Visible = true;

                    label19.Visible = true;
                    label21.Visible = true;
                    label24.Visible = true;

                    label23.Text = CarInOutData.CARIN_DATE.ToString();
                    label24.Text = CarInOutData.CARIN_EMP_TITLE + " " + CarInOutData.CARIN_EMP_FNAME + " " + CarInOutData.CARIN_EMP_LNAME;                   
                    label25.Text = CarInOutData.CAROUT_DATE.ToString();
                    label17.Text = CarInOutData.CAROUT_EMP_TITLE + " " + CarInOutData.CAROUT_EMP_FNAME + " " + CarInOutData.CAROUT_EMP_LNAME;
                    
                    btSave2.Visible = false;

                    label47.Visible = true;
                    label27.Visible = true;
                    label25.Visible = true;

                    label28.Visible = true;
                    label26.Visible = true;
                    label17.Visible = true;

                    
                }

                
            }
            else
            {
                MessageBox.Show("ไม่พบข้อมูลรายการสั่งซื้อ", "Not Found", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }



        }

        private void btSave1_Click(object sender, EventArgs e)
        {
            tb_r_carinout carinout = new tb_r_carinout();
            carinout.APP_ID = APPCarData.APP_ID;
            carinout.CARIN_DATE = DateTime.Now;
            carinout.CARIN_EMP_ID = 1;



            if (dao.saveCarintout(carinout)) 
            {
                MessageBox.Show("บันทึกข้อมูลเรียบร้อย");
                CarInOutData = dao.getCarInOutData(APPCarData.APP_ID);
                Doc_addData(1);
                showPrintPreview();
                tb_app_search.Text = APPCarData.APP_ID.ToString();                
                searchAppointment();
            }
            else
            {
                MessageBox.Show("บันทึกข้อมูลผิดพลาด กรุณาติดต่อผู้ดูแลระบบ");
            }
        }

        private void btSave2_Click(object sender, EventArgs e)
        {
            if (dao.Updatecarout(APPCarData.APP_ID, CarInOutData.CARINOUT_ID,1))
            {
                MessageBox.Show("บันทึกข้อมูลเรียบร้อย");
                CarInOutData = dao.getCarInOutData(APPCarData.APP_ID);
                Doc_addData(2);
                showPrintPreview();
                //tb_app_search.Text = APPCarData.APP_ID.ToString();                
                //searchAppointment();
                Search_ID form = new Search_ID(APPCarData.ORDER_H_ID);
                form.Show();
            }
            else
            {
                MessageBox.Show("บันทึกข้อมูลผิดพลาด กรุณาติดต่อผู้ดูแลระบบ");
            }
        }

        public void showPrintPreview()
        {
            try
            {
                pnlOutput.Visible = true;
                Cursor = Cursors.WaitCursor;
                PrintDocumentForm form = new PrintDocumentForm(pnlOutput);
                Cursor = Cursors.Default;
                form.Show();
                form.Activate();
                pnlOutput.Visible = false;

            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return;
            }



        }

        private void Doc_addData(int role)
        {
            label37.Text = CarInOutData.CARINOUT_ID.ToString();
            label42.Text = APPCarData.APP_ID.ToString();
            label39.Text = APPCarData.ORDER_H_ID;
            label40.Text = APPCarData.TITLE + " " + APPCarData.CUS_FNAME + " " + APPCarData.CUS_LNAME;
            label41.Text = APPCarData.CAR_BRAND + " " + APPCarData.CAR_MODEL + " ปี " + APPCarData.CAR_YEAR + " สี " + APPCarData.CAR_COLOR + " ทะเบียน " + APPCarData.CAR_LICENSE_NO;

            if (role == 1)
            {

                
                label1.Text = "ใบรับรถ";
                label35.Text = "วันนัดรับรถ";
                label36.Text = "ผู้รับรถ";

                label35.Location = new System.Drawing.Point(70, 256);
                label35.Size = new System.Drawing.Size(80, 18);
                label36.Location = new System.Drawing.Point(94, 312);
                label36.Size = new System.Drawing.Size(55, 18);

                label38.Text = CarInOutData.CARIN_DATE.ToString();
                label43.Text = APPCarData.APP_DATETIME_FINISH.ToString();
                label44.Text = CarInOutData.CARIN_EMP_TITLE + " " + CarInOutData.CARIN_EMP_FNAME + " " + CarInOutData.CARIN_EMP_LNAME;
            
                
            }
            else
            {
                label1.Text = "ใบคืนรถ";
                label35.Text = "วันที่นำรถเข้ามา";
                label36.Text = "ผู้ส่งมอบรถ";

                label35.Location = new System.Drawing.Point(46, 256);
                label35.Size = new System.Drawing.Size(104, 18);
                label36.Location = new System.Drawing.Point(71, 312);
                label36.Size = new System.Drawing.Size(79, 18);

                label38.Text = CarInOutData.CAROUT_DATE.ToString();
                label43.Text = CarInOutData.CARIN_DATE.ToString();
                label44.Text = CarInOutData.CAROUT_EMP_TITLE + " " + CarInOutData.CAROUT_EMP_FNAME + " " + CarInOutData.CAROUT_EMP_LNAME;

                }
        }      




        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private Boolean isFinish(List<TaskData> data)
        {
            var result = true;
            for (int i = 0; i < data.Count; i++)
            {
                if (data[i].work_status != 2) { result = false; break; }

            }
            return result;
        
        }

    }
}
