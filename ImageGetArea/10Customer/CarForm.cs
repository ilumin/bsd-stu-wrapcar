﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WrapCar_Connection;
using WrapCar_DataModel;

namespace WrapCar._10Customer
{
    public partial class CarForm : Form
    {
        public string Myproperty1 { get; set; }
        private int customerId;
        private CustomerDAO dao = new CustomerDAO();
        private CustomerData customer;
        private List<CustomerCarData> custDataList = new List<CustomerCarData>();

        public CarForm(int customerId)
        {
            InitializeComponent();

            this.customerId = customerId;

            // get customer data
            customer = dao.getCustomer(this.customerId);

            subject.Text += customer.CUS_FNAME + " " + customer.CUS_LNAME;

            // register event
            this.FormClosed += beforeClose;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0 || e.ColumnIndex > 1)
                return;

            // get customer ID
            var customerId = dataGridView1.Rows[e.RowIndex].Cells[2].Value;

            switch (e.ColumnIndex)
            {
                case 0:
                    UpdateCarForm updateForm = new UpdateCarForm(Convert.ToInt32(customerId));
                    updateForm.Show();
                    break;

                default:
                    return;
            }

            this.FormClosed -= beforeClose;

            // close current form
            this.Close();
        }

        private void CarForm_Load(object sender, EventArgs e)
        {
            // this.WindowState = FormWindowState.Maximized;
            // txtCustomerID.Text = this.customerId.ToString();

            dataGridView1.CellClick += new DataGridViewCellEventHandler(dataGridView1_CellClick);

            dataGridView1.ColumnCount = 9;
            dataGridView1.Columns[0].Name = "   ";
            dataGridView1.Columns[1].Name = "No.";
            dataGridView1.Columns[2].Name = "CustomerCar ID";
            dataGridView1.Columns[3].Name = "Car Licence No.";
            dataGridView1.Columns[4].Name = "Brand";
            dataGridView1.Columns[5].Name = "Model";
            dataGridView1.Columns[6].Name = "Year";
            dataGridView1.Columns[7].Name = "Color";
            dataGridView1.Columns[8].Name = "HexCode";

            this.gridRender();
        }

        private void gridRender()
        {
            dataGridView1.Rows.Clear();
            custDataList = dao.getCustomerCarList();

            int row = 0;
            object[] temp = new object[14];
            List<DataGridViewRow> rows_item = new List<DataGridViewRow>();

            // list customer car only
            custDataList = custDataList.Where(cus => cus.CUS_ID == this.customerId).ToList();

            // filter by license number
            if (txtCustomerID.Text != "")
                custDataList = custDataList.Where(cus => cus.CAR_LICENSE_NO.Contains(txtCustomerID.Text)).ToList();

            foreach (CustomerCarData custData in custDataList)
            {
                string[] dataRow = new string[] { "", "" + (++row)
                    , custData.CUS_CAR_ID.ToString()
                    , custData.CAR_LICENSE_NO, custData.CAR_BRAND, custData.CAR_MODEL
                    , custData.CAR_YEAR, custData.CAR_COLOR, custData.CAR_COLOR_HEXCODE};
                
                // Add row 
                dataGridView1.Rows.Add(dataRow);

                // Add button cell
                var editButton = new DataGridViewButtonCell();
                editButton.Value = " Edit ";

                dataGridView1.Rows[row - 1].Cells[0] = editButton;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.gridRender();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            RegisterCarForm regist = new RegisterCarForm(this.customerId);
            regist.Show();

            this.FormClosed -= beforeClose;

            this.Close();
        }

        private void beforeClose(object sender, EventArgs e)
        {
            CustomerForm form = new CustomerForm();
            form.Show();
        }

    }
}
