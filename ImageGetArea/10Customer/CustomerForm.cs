﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WrapCar_Connection;
using WrapCar_DataModel;


namespace WrapCar._10Customer
{
    public partial class CustomerForm : Form
    {
        private CustomerDAO dao = new CustomerDAO();
        private List<CustomerData> custDataList = new List<CustomerData>();

        public CustomerForm()
        {
            InitializeComponent();

            // render grid head
            this.gridInit();

            // render data
            this.gridRender();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0 || e.ColumnIndex > 1)
                return;

            // get customer ID
            var customerId = dataGridView1.Rows[e.RowIndex].Cells[2].Value;

            switch (e.ColumnIndex)
            {
                case 0:
                    UpdateCustomerForm updateForm = new UpdateCustomerForm(Convert.ToInt32(customerId));
                    updateForm.Show();
                    break;

                case 1:
                    CarForm carForm = new CarForm(Convert.ToInt32(customerId));
                    carForm.Show();
                    break;

                default:
                    return;
            }

            // close current form
            this.Close();
        }

        /**
         * render datagrid header
         */
        private void gridInit() 
        {
            dataGridView1.ColumnCount = 14;

            dataGridView1.CellClick += new DataGridViewCellEventHandler(dataGridView1_CellClick);

            dataGridView1.Columns[0].Name = "   ";
            dataGridView1.Columns[1].Name = "   ";
            dataGridView1.Columns[2].Name = "Customer ID";
            dataGridView1.Columns[3].Name = "Title";
            dataGridView1.Columns[4].Name = "Customer Name";
            dataGridView1.Columns[5].Name = "Customer Lastname";
            dataGridView1.Columns[6].Name = "Mobile No.";
            dataGridView1.Columns[7].Name = "Email";
            dataGridView1.Columns[8].Name = "ID Card No.";
            dataGridView1.Columns[9].Name = "ID Card Issue Date";
            dataGridView1.Columns[10].Name = "ID Card Expire Date";
            dataGridView1.Columns[11].Name = "Driver License No.";
            dataGridView1.Columns[12].Name = "Driver License Issue Date";
            dataGridView1.Columns[13].Name = "Driver License Expire Date";

            this.gridClear();
        }

        /**
         * clear grid data
         */
        private void gridClear()
        {
            dataGridView1.Rows.Clear();
        }

        /**
         * render grid data
         */
        private void gridRender()
        {
            int row = 0;
            object[] temp = new object[14];
            List<DataGridViewRow> rows_item = new List<DataGridViewRow>();

            // clear grid data 
            this.gridClear();

            custDataList = dao.getCustomerList();

            if (txtCustName.Text != "")
                custDataList = custDataList.Where(cus => cus.CUS_FNAME.Contains(txtCustName.Text)).ToList();

            foreach (CustomerData custData in custDataList)
            {
                row = row + 1;

                // temp[0] = btnCell;
                // temp[1] = (++row);
                temp[2] = custData.CUS_ID.ToString();
                temp[3] = custData.TITLE.ToString();
                temp[4] = custData.CUS_FNAME.ToString();
                temp[5] = custData.CUS_LNAME.ToString();
                temp[6] = custData.MOBILE_NO.ToString();
                temp[7] = custData.EMAIL.ToString();
                temp[8] = custData.ID_CARD.ToString();
                temp[9] = custData.ID_CARD_ISSUE_DATE.ToString();
                temp[10] = custData.ID_CARD_EXP_DATE.ToString();
                temp[11] = (string) custData.DRIVING_LICENCE_ID;
                temp[12] = custData.DRIVING_LICENCE_ISSUE_DATE.ToString();
                temp[13] = custData.DRIVING_LICENCE_EXP_DATE.ToString();

                // dataGridView1.Rows.Add(dataRow);
                rows_item.Add(new DataGridViewRow());
                rows_item[rows_item.Count - 1].CreateCells(dataGridView1, temp);

                // Add button cell
                var cellButton = new DataGridViewButtonCell();
                cellButton.Value = " Edit ";

                var carButton = new DataGridViewButtonCell();
                carButton.Value = " View Car ";

                rows_item[rows_item.Count - 1].Cells[0] = cellButton;
                rows_item[rows_item.Count - 1].Cells[1] = carButton;
            }

            dataGridView1.Rows.AddRange(rows_item.ToArray());
        }

        private void CustomerForm_Load(object sender, EventArgs e)
        {
            // this.WindowState = FormWindowState.Maximized;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.gridRender();
        }

        // open add customer form
        private void btnAdd_Click(object sender, EventArgs e)
        {
            RegisterCustomerForm form = new RegisterCustomerForm();
            form.Show();

            // close form
            this.Close();
        }
    }
}
