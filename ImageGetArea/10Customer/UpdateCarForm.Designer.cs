﻿namespace WrapCar._10Customer
{
    partial class UpdateCarForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbCarColor = new System.Windows.Forms.ComboBox();
            this.btnEdit = new System.Windows.Forms.Button();
            this.txtHexCode = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbCarYear = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCarModel = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCarBrand = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCLNo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cmbCarColor
            // 
            this.cmbCarColor.FormattingEnabled = true;
            this.cmbCarColor.Items.AddRange(new object[] {
            "Grey",
            "Black",
            "White"});
            this.cmbCarColor.Location = new System.Drawing.Point(109, 180);
            this.cmbCarColor.Name = "cmbCarColor";
            this.cmbCarColor.Size = new System.Drawing.Size(124, 21);
            this.cmbCarColor.TabIndex = 30;
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(109, 266);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(99, 37);
            this.btnEdit.TabIndex = 29;
            this.btnEdit.Text = "บันทึกการแก้ไข";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // txtHexCode
            // 
            this.txtHexCode.Location = new System.Drawing.Point(109, 217);
            this.txtHexCode.Name = "txtHexCode";
            this.txtHexCode.Size = new System.Drawing.Size(124, 20);
            this.txtHexCode.TabIndex = 28;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(38, 219);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 13);
            this.label7.TabIndex = 27;
            this.label7.Text = "สี Hexcode";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(83, 180);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 13);
            this.label6.TabIndex = 26;
            this.label6.Text = "สี";
            // 
            // cmbCarYear
            // 
            this.cmbCarYear.FormattingEnabled = true;
            this.cmbCarYear.Items.AddRange(new object[] {
            "2020",
            "2019",
            "2018",
            "2017",
            "2016",
            "2015",
            "2014",
            "2013",
            "2012",
            "2011",
            "2010",
            "2009",
            "2008",
            "2007",
            "2006",
            "2005",
            "2004",
            "2003",
            "2002",
            "2001",
            "2000"});
            this.cmbCarYear.Location = new System.Drawing.Point(108, 137);
            this.cmbCarYear.Name = "cmbCarYear";
            this.cmbCarYear.Size = new System.Drawing.Size(124, 21);
            this.cmbCarYear.TabIndex = 25;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(54, 137);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 24;
            this.label5.Text = "ปีที่ผลิต";
            // 
            // txtCarModel
            // 
            this.txtCarModel.Location = new System.Drawing.Point(108, 96);
            this.txtCarModel.Name = "txtCarModel";
            this.txtCarModel.Size = new System.Drawing.Size(124, 20);
            this.txtCarModel.TabIndex = 23;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(45, 98);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "โมเดลรถ";
            // 
            // txtCarBrand
            // 
            this.txtCarBrand.Location = new System.Drawing.Point(108, 58);
            this.txtCarBrand.Name = "txtCarBrand";
            this.txtCarBrand.Size = new System.Drawing.Size(124, 20);
            this.txtCarBrand.TabIndex = 21;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(56, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "ยี่ห้อรถ";
            // 
            // txtCLNo
            // 
            this.txtCLNo.Location = new System.Drawing.Point(108, 18);
            this.txtCLNo.Name = "txtCLNo";
            this.txtCLNo.Size = new System.Drawing.Size(124, 20);
            this.txtCLNo.TabIndex = 19;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "เลขทะเบียนรถ";
            // 
            // UpdateCarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(287, 336);
            this.Controls.Add(this.cmbCarColor);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.txtHexCode);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cmbCarYear);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtCarModel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtCarBrand);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtCLNo);
            this.Controls.Add(this.label2);
            this.Name = "UpdateCarForm";
            this.Text = "UpdateCarForm";
            this.Load += new System.EventHandler(this.UpdateCarForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbCarColor;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.TextBox txtHexCode;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbCarYear;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCarModel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCarBrand;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCLNo;
        private System.Windows.Forms.Label label2;
        //private System.Windows.Forms.TextBox txtCusID;
    }
}