﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WrapCar_Connection;

namespace WrapCar._10Customer
{
    public partial class RegisterCarForm : Form
    {
        public string Myprop { get; set; }
        private int customerId;

        public RegisterCarForm(int customerId)
        {
            InitializeComponent();

            this.customerId = customerId;

            this.FormClosed += beforeClose;
        }

        private void RegisterCarForm_Load(object sender, EventArgs e)
        {
            this.BilningCarBrandCombo();
        }

        private void BilningCarBrandCombo()
        {
            List<string> brandList = Utilities.Utilities.GET_STR_CAR_BRAND_MASTER_ALL();
            this.cmbCarBrand.DataSource = brandList.ToArray();
        }

        private void cmbCarBrand_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedBrand = this.cmbCarBrand.SelectedItem.ToString();
            this.BilningCarModelCombo();
        }


        private void BilningCarModelCombo()
        {
            List<string> brandList = Utilities.Utilities.GET_STR_CAR_MODEL_ALL(this.cmbCarBrand.SelectedItem.ToString());
            this.cmbModel.DataSource = brandList.ToArray();
        }

        private void cmbModel_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedModel = this.cmbModel.SelectedItem.ToString();
            this.BilningCarYearCombo();
        }

        private void BilningCarYearCombo()
        {
            List<string> yearList = Utilities.Utilities.GET_STR_CAR_YEAR_ALL(this.cmbCarBrand.SelectedItem.ToString(), this.cmbModel.SelectedItem.ToString());
            this.cmbCarYear.DataSource = yearList.ToArray();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if ((string.Empty == txtCLNo.Text)
              || (string.Empty == cmbCarBrand.SelectedItem.ToString()))
            {
                MessageBox.Show("กรุณาระบุ Customer ID เลขทะเบียนรถยนตร์ และยี่ห้อรถให้ครบถ้วน");
            }
            else
            {
                tb_m_customer_car data1 = new tb_m_customer_car();
                data1.CUS_ID = this.customerId;
                data1.CAR_LICENSE_NO = txtCLNo.Text;
                data1.CAR_BRAND = cmbCarBrand.SelectedItem.ToString();
                data1.CAR_MODEL = cmbModel.SelectedItem.ToString();
                data1.CAR_YEAR = cmbCarYear.Text;
                data1.CAR_COLOR = cmbCarColor.Text;
                data1.CAR_COLOR_HEXCODE = txtHexCode.Text;

                WrapCar_Connection.CustomerDAO dao = new CustomerDAO();
                bool ret1 = dao.saveCustomerCar(data1);
                if (ret1)
                {
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ");
                }
                else
                {
                    MessageBox.Show("บันทึกข้อมูลไม่สำเร็จ กรุณาตรวจสอบใหม่");
                }

                this.Close();
            }

        }

        public tb_m_customer_car data1 { get; set; }

        private void beforeClose(object sender, EventArgs e)
        {
            CarForm form = new CarForm(this.customerId);
            form.Show();
        }



    }
}
