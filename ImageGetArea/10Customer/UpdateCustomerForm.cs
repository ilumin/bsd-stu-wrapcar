﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WrapCar_Connection;
using WrapCar_DataModel;

namespace WrapCar._10Customer
{
    public partial class UpdateCustomerForm : Form
    {

        private CustomerDAO dao = new CustomerDAO();
        private CustomerData customer;
        
        public UpdateCustomerForm(int customerId)
        {
            InitializeComponent();

            this.customer = dao.getCustomer(customerId);

            // register event
            this.FormClosed += beforeClose;
        }

        private void UpdateCustomerForm_Load(object sender, EventArgs e)
        {
            // this.WindowState = FormWindowState.Maximized;

            txtCusID.Text   = this.customer.CUS_ID.ToString();
            cmbTitle.Text   = this.customer.TITLE.ToString();
            txtName.Text    = this.customer.CUS_FNAME.ToString();
            txtSurname.Text = this.customer.CUS_LNAME.ToString();
            txtTelNo.Text   = this.customer.MOBILE_NO.ToString();
            txtEmail.Text   = this.customer.EMAIL.ToString();
            txtIDNo.Text    = this.customer.ID_CARD.ToString();
            dTPIDIssue.Text = this.customer.ID_CARD_ISSUE_DATE.ToString();
            dTPIDExp.Text   = this.customer.ID_CARD_EXP_DATE.ToString();
            txtDLNo.Text    = (string) this.customer.DRIVING_LICENCE_ID;
            dTPDLIssue.Text = this.customer.DRIVING_LICENCE_ISSUE_DATE.ToString();
            dTPDLExp.Text   = this.customer.DRIVING_LICENCE_EXP_DATE.ToString();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if ((string.Empty == txtName.Text)
                || (string.Empty == txtSurname.Text)
                || (string.Empty == txtTelNo.Text)
                || (string.Empty == txtEmail.Text))
            {
                MessageBox.Show("กรุณาระบุ ชื่อ นามสกุล เบอร์โทรศัพท์ และอีเมลล์ให้ครบถ้วน");
            }
            else
            {
                tb_m_customer data = new tb_m_customer();
                data.CUS_ID = Convert.ToInt16(txtCusID.Text);
                data.TITLE = cmbTitle.Text;
                data.CUS_FNAME = txtName.Text;
                data.CUS_LNAME = txtSurname.Text;
                data.EMAIL = (string.Empty == txtEmail.Text) ? null : txtEmail.Text;
                data.MOBILE_NO = (string.Empty == txtTelNo.Text) ? null : txtTelNo.Text;
                data.ID_CARD = txtIDNo.Text;
                data.ID_CARD_ISSUE_DATE = Convert.ToDateTime(dTPIDIssue.Text);
                data.ID_CARD_EXP_DATE = Convert.ToDateTime(dTPIDExp.Text);
                data.DRIVING_LICENCE_ID = txtDLNo.Text;
                data.DRIVING_LICENCE_ISSUE_DATE = Convert.ToDateTime(dTPDLIssue.Text);
                data.DRIVING_LICENCE_EXP_DATE = Convert.ToDateTime(dTPDLExp.Text);

                data.REGISTER_DATE = DateTime.Now;

                CustomerDAO dao = new CustomerDAO();
                bool ret1 = dao.UpdateCustomer(data);
                //bool ret = dao.UpdateCustomer(data);
                if (ret1)
                {
                    MessageBox.Show("แก้ไขข้อมูลสำเร็จ");
                }
                else
                {
                    MessageBox.Show("แก้ไขข้อมูลไม่สำเร็จ กรุณาตรวจสอบใหม่");
                }

                this.Close();
            }
        }

        private void beforeClose(object sender, EventArgs e)
        {
            CustomerForm form = new CustomerForm();
            form.Show();
        }
    }
}
