﻿namespace WrapCar._10Customer
{
    partial class RegisterCarForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.txtCLNo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbCarYear = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtHexCode = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.cmbCarColor = new System.Windows.Forms.ComboBox();
            this.cmbCarBrand = new System.Windows.Forms.ComboBox();
            this.cmbModel = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "เลขทะเบียนรถ";
            // 
            // txtCLNo
            // 
            this.txtCLNo.Location = new System.Drawing.Point(119, 19);
            this.txtCLNo.Name = "txtCLNo";
            this.txtCLNo.Size = new System.Drawing.Size(100, 20);
            this.txtCLNo.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(67, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "ยี่ห้อรถ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(56, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "โมเดลรถ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(65, 138);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "ปีที่ผลิต";
            // 
            // cmbCarYear
            // 
            this.cmbCarYear.FormattingEnabled = true;
            this.cmbCarYear.Items.AddRange(new object[] {
            "2020",
            "2019",
            "2018",
            "2017",
            "2016",
            "2015",
            "2014",
            "2013",
            "2012",
            "2011",
            "2010",
            "2009",
            "2008",
            "2007",
            "2006",
            "2005",
            "2004",
            "2003",
            "2002",
            "2001",
            "2000"});
            this.cmbCarYear.Location = new System.Drawing.Point(119, 138);
            this.cmbCarYear.Name = "cmbCarYear";
            this.cmbCarYear.Size = new System.Drawing.Size(100, 21);
            this.cmbCarYear.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(94, 181);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "สี";
            // 
            // txtHexCode
            // 
            this.txtHexCode.Location = new System.Drawing.Point(120, 218);
            this.txtHexCode.Name = "txtHexCode";
            this.txtHexCode.Size = new System.Drawing.Size(100, 20);
            this.txtHexCode.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(49, 220);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "สี Hexcode";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(120, 267);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 14;
            this.btnSave.Text = "บันทึก";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cmbCarColor
            // 
            this.cmbCarColor.FormattingEnabled = true;
            this.cmbCarColor.Items.AddRange(new object[] {
            "Grey",
            "Black",
            "White"});
            this.cmbCarColor.Location = new System.Drawing.Point(120, 181);
            this.cmbCarColor.Name = "cmbCarColor";
            this.cmbCarColor.Size = new System.Drawing.Size(100, 21);
            this.cmbCarColor.TabIndex = 15;
            // 
            // cmbCarBrand
            // 
            this.cmbCarBrand.DropDownHeight = 200;
            this.cmbCarBrand.FormattingEnabled = true;
            this.cmbCarBrand.IntegralHeight = false;
            this.cmbCarBrand.ItemHeight = 13;
            this.cmbCarBrand.Location = new System.Drawing.Point(120, 61);
            this.cmbCarBrand.Name = "cmbCarBrand";
            this.cmbCarBrand.Size = new System.Drawing.Size(143, 21);
            this.cmbCarBrand.TabIndex = 64;
            this.cmbCarBrand.SelectedIndexChanged += new System.EventHandler(this.cmbCarBrand_SelectedIndexChanged);
            // 
            // cmbModel
            // 
            this.cmbModel.DropDownHeight = 200;
            this.cmbModel.FormattingEnabled = true;
            this.cmbModel.IntegralHeight = false;
            this.cmbModel.ItemHeight = 13;
            this.cmbModel.Location = new System.Drawing.Point(120, 96);
            this.cmbModel.Name = "cmbModel";
            this.cmbModel.Size = new System.Drawing.Size(143, 21);
            this.cmbModel.TabIndex = 65;
            this.cmbModel.SelectedIndexChanged += new System.EventHandler(this.cmbModel_SelectedIndexChanged);
            // 
            // RegisterCarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(386, 402);
            this.Controls.Add(this.cmbModel);
            this.Controls.Add(this.cmbCarBrand);
            this.Controls.Add(this.cmbCarColor);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtHexCode);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cmbCarYear);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtCLNo);
            this.Controls.Add(this.label2);
            this.Name = "RegisterCarForm";
            this.Text = "RegisterCarForm";
            this.Load += new System.EventHandler(this.RegisterCarForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCLNo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbCarYear;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtHexCode;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ComboBox cmbCarColor;
        private System.Windows.Forms.ComboBox cmbCarBrand;
        private System.Windows.Forms.ComboBox cmbModel;
    }
}