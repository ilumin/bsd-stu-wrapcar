﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WrapCar_Connection;
using WrapCar_DataModel;

namespace WrapCar._10Customer
{
    public partial class UpdateCarForm : Form
    {

        public string Property1 { get; set; }
        public string Property2 { get; set; }
        public string Property3 { get; set; }
        public string Property4 { get; set; }
        public string Property5 { get; set; }
        public string Property6 { get; set; }
        public string Property7 { get; set; }
        public string Property8 { get; set; }

        private int customerId;
        private int carId;
        private CustomerDAO dao = new CustomerDAO();
        private CustomerCarData carData;

        public UpdateCarForm(int carId)
        {
            InitializeComponent();

            this.carId = carId;

            this.FormClosed += beforeClose;
        }

        private void UpdateCarForm_Load(object sender, EventArgs e)
        {
            // get car data from dao
            carData = this.dao.getCustomerCar(this.carId);

            this.carId = carData.CUS_CAR_ID;
            this.customerId = carData.CUS_ID;

            // txtCustomerID.Text = carData.CUS_ID.ToString();
            // txtCarID.Text = carData.CUS_CAR_ID.ToString();
            txtCLNo.Text = carData.CAR_LICENSE_NO.ToString();
            txtCarBrand.Text = carData.CAR_BRAND.ToString();
            txtCarModel.Text = carData.CAR_MODEL.ToString();
            cmbCarYear.Text = carData.CAR_YEAR.ToString();
            cmbCarColor.Text = (string) carData.CAR_COLOR;
            txtHexCode.Text = (string) carData.CAR_COLOR_HEXCODE;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if ((string.Empty == txtCLNo.Text)
                || (string.Empty == txtCarModel.Text)
                || (string.Empty == cmbCarYear.Text)
                || (string.Empty == cmbCarColor.Text))
            {
                MessageBox.Show("กรุณาระบุ เลขทะเบียนรถ รุ่น ปีที่ผลิต และสีให้ครบถ้วน");
            }
            else
            {
                tb_m_customer_car data = new tb_m_customer_car();

                data.CUS_CAR_ID = this.carId;
                data.CUS_ID = this.customerId;
                data.CAR_LICENSE_NO = txtCLNo.Text;
                data.CAR_BRAND = txtCarBrand.Text;
                data.CAR_MODEL = txtCarModel.Text;
                data.CAR_YEAR = cmbCarYear.Text;
                data.CAR_COLOR = cmbCarColor.Text;
                data.CAR_COLOR_HEXCODE = txtHexCode.Text;

                bool ret2 = this.dao.UpdateCustomerCar(data);
                if (ret2)
                {
                    MessageBox.Show("แก้ไขข้อมูลสำเร็จ");
                }
                else
                {
                    MessageBox.Show("แก้ไขข้อมูลไม่สำเร็จ กรุณาตรวจสอบใหม่");
                }

            }

            this.Close();
        }

        private void beforeClose(object sender, EventArgs e)
        {
            CarForm form = new CarForm(this.customerId);
            form.Show();
        }
    }
}
