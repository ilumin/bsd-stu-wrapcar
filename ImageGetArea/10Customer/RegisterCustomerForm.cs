﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WrapCar_Connection;
using WrapCar_DataModel;

namespace WrapCar._10Customer
{
    public partial class RegisterCustomerForm : Form
    {
        public RegisterCustomerForm()
        {
            InitializeComponent();
        }

        private void RegisterCustomerForm_Load(object sender, EventArgs e)
        {
            // this.WindowState = FormWindowState.Maximized;

            // Add event
            this.FormClosed += beforeClose;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            if ((string.Empty == txtName.Text) 
                || (string.Empty == txtSurname.Text)
                || (string.Empty == txtTelNo.Text)
                || (string.Empty == txtEmail.Text))
            {
                MessageBox.Show("กรุณาระบุ ชื่อ นามสกุล เบอร์โทรศัพท์ และอีเมลล์ให้ครบถ้วน");
            }
            else
            {
                tb_m_customer data = new tb_m_customer();
                data.TITLE = cmbTitle.Text;
                data.CUS_FNAME = txtName.Text;
                data.CUS_LNAME = txtSurname.Text;
                data.EMAIL = (string.Empty == txtEmail.Text) ? null : txtEmail.Text;
                data.MOBILE_NO = (string.Empty == txtTelNo.Text) ? null : txtTelNo.Text;
                data.ID_CARD = txtIDNo.Text;
                data.ID_CARD_ISSUE_DATE = Convert.ToDateTime(dTPIDIssue.Text);
                data.ID_CARD_EXP_DATE = Convert.ToDateTime(dTPIDExp.Text);
                data.DRIVING_LICENCE_ID = txtDLNo.Text;
                data.DRIVING_LICENCE_ISSUE_DATE = Convert.ToDateTime(dTPDLIssue.Text);
                data.DRIVING_LICENCE_EXP_DATE = Convert.ToDateTime(dTPDLExp.Text);

                data.REGISTER_DATE = DateTime.Now;

                CustomerDAO dao = new CustomerDAO();
                bool ret = dao.saveCustomer(data);
                if (ret)
                {
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ");
                }
                else
                {
                    MessageBox.Show("บันทึกข้อมูลไม่สำเร็จ กรุณาตรวจสอบใหม่");
                }

                List<CustomerData> custDataList = new List<CustomerData>();
                custDataList = dao.getCustomerList();

                custDataList = custDataList.Where(cus => cus.CUS_FNAME == txtName.Text && cus.CUS_LNAME == txtSurname.Text).ToList();

                // display usercar form
                // to add car
                CarForm carForm = new CarForm(custDataList[0].CUS_ID);
                carForm.Show();

                // remove event
                this.FormClosed -= beforeClose;
                this.Close();
            }
        }

        private DateTime? dateTimePicker1(string p)
        {
            throw new NotImplementedException();
        }

        private void beforeClose(object sender, EventArgs e)
        {
            CustomerForm form = new CustomerForm();
            form.Show();
        }
    }
}
