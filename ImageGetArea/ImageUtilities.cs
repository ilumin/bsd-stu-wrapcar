﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Collections.Generic;
using WrapCar._20Desing;
using System.Linq;
using System.Globalization;
using System.IO;

namespace WrapCar
{    

    public static class ImageUtilities
    {
        public static Bitmap drawTextOnImage(Bitmap img, string text, int x, int y)
        {

            Graphics g = Graphics.FromImage(img);
            g.DrawString(text, new Font("Arial", 7, FontStyle.Regular), Brushes.Black, new Point( x, y ));
            return img;
        }
        public static DateTime StartOfWeek(DateTime dt, DayOfWeek startOfWeek)
        {
           int diff = (startOfWeek - dt.DayOfWeek);   

           DateTime retDate =  dt.AddDays(diff).Date;
           return retDate;
        }

        public static DateTime EndOfWeek(DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = (dt.DayOfWeek - startOfWeek);

            DateTime retDate = dt.AddDays(-1 * diff).Date;
            return retDate;
        }

        public static Bitmap ReplaceColor(Bitmap _image, Color _colorOld, Color _colorNew)
        {
            Bitmap bmap = (Bitmap)_image.Clone();

            int _tolerance = 30;

            Color c;
            int iR_Min, iR_Max;
            int iG_Min, iG_Max;
            int iB_Min, iB_Max;

            //Defining Tolerance
            //R
            iR_Min = Math.Max((int)_colorOld.R - _tolerance, 0);
            iR_Max = Math.Min((int)_colorOld.R + _tolerance, 255);

            //G
            iG_Min = Math.Max((int)_colorOld.G - _tolerance, 0);
            iG_Max = Math.Min((int)_colorOld.G + _tolerance, 255);

            //B
            iB_Min = Math.Max((int)_colorOld.B - _tolerance, 0);
            iB_Max = Math.Min((int)_colorOld.B + _tolerance, 255);


            for (int x = 0; x < bmap.Width; x++)
            {
                for (int y = 0; y < bmap.Height; y++)
                {
                    c = bmap.GetPixel(x, y);


                    //Determinig Color Match
                    if (
                        (c.R >= iR_Min && c.R <= iR_Max) &&
                        (c.G >= iG_Min && c.G <= iG_Max) &&
                        (c.B >= iB_Min && c.B <= iB_Max)
                    )
                        if (_colorNew == Color.Transparent)
                            bmap.SetPixel(x, y, Color.FromArgb(0,
                                                              _colorOld.R,
                                                              _colorOld.G,
                                                              _colorOld.B));
                        else
                            bmap.SetPixel(x, y, Color.FromArgb(c.A,
                                                              _colorOld.R,
                                                              _colorOld.G,
                                                              _colorOld.B));
                }
            }
            return (Bitmap)bmap.Clone();
        }

        public static Color findTheNearestColor(Dictionary<string, StickerDetection> mapObj, Color colorInp)
        {
            Color nearest_color = colorInp;
            double dbl_test_red;
            double dbl_test_green;
            double dbl_test_blue;

            double dbl_input_red = Convert.ToDouble(colorInp.R);
            double dbl_input_green = Convert.ToDouble(colorInp.G);
            double dbl_input_blue = Convert.ToDouble(colorInp.B);

            double temp;
            double distance = 90;
            foreach (Color obj in mapObj.Values.Select(t => t.color))
            {
                // compute the Euclidean distance between the two colors
                // note, that the alpha-component is not used in this example
                dbl_test_red = Math.Pow(Convert.ToDouble(obj.R) - dbl_input_red, 2.0);
                dbl_test_green = Math.Pow(Convert.ToDouble(obj.G) - dbl_input_green, 2.0);
                dbl_test_blue = Math.Pow(Convert.ToDouble(obj.B) - dbl_input_blue, 2.0);

                // it is not necessary to compute the square root
                // it should be sufficient to use:
                // temp = dbl_test_blue + dbl_test_green + dbl_test_red;
                // if you plan to do so, the distance should be initialized by 250000.0
                temp = Math.Sqrt(dbl_test_blue + dbl_test_green + dbl_test_red);

                // explore the result and store the nearest color
                if (temp < distance)
                {
                    //Console.WriteLine(" temp < distance : " + temp + "<" + distance);
                    distance = temp;
                    nearest_color = obj;
                }
            }

            return nearest_color;
        }

        public static string getColorName(string hexString)
        {
            int red = int.Parse(hexString.Substring(0, 2), NumberStyles.HexNumber);
            int green = int.Parse(hexString.Substring(2, 2), NumberStyles.HexNumber);
            int blue = int.Parse(hexString.Substring(4, 2), NumberStyles.HexNumber);
            return Color.FromArgb(red, green, blue).Name;
        }

        public static Bitmap MergeImage(Bitmap b1, Bitmap b2)
        {
            try
            {
                Bitmap img3 = new Bitmap(b1.Width, b1.Height);
                for (int i = 0; i < b1.Width; i++)
                {
                   for (int j = 0; j < b1.Height; j++)
                   {
                       Color col1 = b1.GetPixel(i, j);
                       Color col2 = b2.GetPixel(i,j);
                       if (!Color.Transparent.Equals(col2))
                       {                       
                           img3.SetPixel(i, j, col2);
                       }
                       else
                       {
                           img3.SetPixel(i, j, col1);
                       }
                   }
                }
                //Bitmap img3 = new Bitmap(b1.Width, b1.Height);
                //Graphics g = Graphics.FromImage(img3);
                //g.Clear(Color.Black);
                //g.DrawImage(b1, new Point(0, 0));
                //g.DrawImage(b2, new Point(b1.Width, 0));
                //g.Dispose();

                return b1;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return null ;
            }
        }

        public static bool EdgeDetectHorizontal(Bitmap b)
        {
            Bitmap bmTemp = (Bitmap)b.Clone();

            // GDI+ still lies to us - the return format is BGR, NOT RGB.
            BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            BitmapData bmData2 = bmTemp.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            System.IntPtr Scan0 = bmData.Scan0;
            System.IntPtr Scan02 = bmData2.Scan0;

            unsafe
            {
                byte* p = (byte*)(void*)Scan0;
                byte* p2 = (byte*)(void*)Scan02;

                int nOffset = stride - b.Width * 3;
                int nWidth = b.Width * 3;

                int nPixel = 0;

                p += stride;
                p2 += stride;

                for (int y = 1; y < b.Height - 1; ++y)
                {
                    p += 9;
                    p2 += 9;

                    for (int x = 9; x < nWidth - 9; ++x)
                    {
                        nPixel = ((p2 + stride - 9)[0] +
                            (p2 + stride - 6)[0] +
                            (p2 + stride - 3)[0] +
                            (p2 + stride)[0] +
                            (p2 + stride + 3)[0] +
                            (p2 + stride + 6)[0] +
                            (p2 + stride + 9)[0] -
                            (p2 - stride - 9)[0] -
                            (p2 - stride - 6)[0] -
                            (p2 - stride - 3)[0] -
                            (p2 - stride)[0] -
                            (p2 - stride + 3)[0] -
                            (p2 - stride + 6)[0] -
                            (p2 - stride + 9)[0]);

                        if (nPixel < 0) nPixel = 0;
                        if (nPixel > 255) nPixel = 255;

                        (p + stride)[0] = (byte)nPixel;

                        ++p;
                        ++p2;
                    }

                    p += 9 + nOffset;
                    p2 += 9 + nOffset;
                }
            }

            b.UnlockBits(bmData);
            bmTemp.UnlockBits(bmData2);

            return true;
        }

        unsafe public static bool EdgeDetectVertical(Bitmap b)
        {
            Bitmap bmTemp = (Bitmap)b.Clone();

            // GDI+ still lies to us - the return format is BGR, NOT RGB.
            BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            BitmapData bmData2 = bmTemp.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            System.IntPtr Scan0 = bmData.Scan0;
            System.IntPtr Scan02 = bmData2.Scan0;

            unsafe
            {
                byte* p = (byte*)(void*)Scan0;
                byte* p2 = (byte*)(void*)Scan02;

                int nOffset = stride - b.Width * 3;
                int nWidth = b.Width * 3;

                int nPixel = 0;

                int nStride2 = stride * 2;
                int nStride3 = stride * 3;

                p += nStride3;
                p2 += nStride3;

                for (int y = 3; y < b.Height - 3; ++y)
                {
                    p += 3;
                    p2 += 3;

                    for (int x = 3; x < nWidth - 3; ++x)
                    {
                        nPixel = ((p2 + nStride3 + 3)[0] +
                            (p2 + nStride2 + 3)[0] +
                            (p2 + stride + 3)[0] +
                            (p2 + 3)[0] +
                            (p2 - stride + 3)[0] +
                            (p2 - nStride2 + 3)[0] +
                            (p2 - nStride3 + 3)[0] -
                            (p2 + nStride3 - 3)[0] -
                            (p2 + nStride2 - 3)[0] -
                            (p2 + stride - 3)[0] -
                            (p2 - 3)[0] -
                            (p2 - stride - 3)[0] -
                            (p2 - nStride2 - 3)[0] -
                            (p2 - nStride3 - 3)[0]);

                        if (nPixel < 0) nPixel = 0;
                        if (nPixel > 255) nPixel = 255;

                        p[0] = (byte)nPixel;

                        ++p;
                        ++p2;
                    }

                    p += 3 + nOffset;
                    p2 += 3 + nOffset;
                }
            }

            b.UnlockBits(bmData);
            bmTemp.UnlockBits(bmData2);

            return true;
        }
        
        public static Bitmap AlphaBlendMatrix(Bitmap destImage, Bitmap srcImage, byte alpha)
        {            
            Bitmap alphaBlendedImage = (Bitmap)destImage.Clone();

            // for the matrix the range is 0.0 - 1.0
            float alphaNorm = (float)alpha / 255.0F;

            // just change the alpha
            ColorMatrix matrix = new ColorMatrix(new float[][]{
                            new float[] {1F, 0, 0, 0, 0},
                            new float[] {0, 1F, 0, 0, 0},
                            new float[] {0, 0, 1F, 0, 0},
                            new float[] {0, 0, 0, alphaNorm, 0},
                            new float[] {0, 0, 0, 0, 1F}});

            ImageAttributes imageAttributes = new ImageAttributes();
            imageAttributes.SetColorMatrix(matrix);

            int offsetX = (destImage.Width - srcImage.Width) / 2;
            int offsetY = (destImage.Height - srcImage.Height) / 2;

            using (Graphics g = Graphics.FromImage(alphaBlendedImage))
            {
                g.CompositingMode = CompositingMode.SourceOver;
                g.CompositingQuality = CompositingQuality.HighQuality;

                // Scaled image (stretched)
                //g.DrawImage(srcImage,
                //    new Rectangle(0, 0, destImage.Width, destImage.Height),
                //    0,
                //    0,
                //    srcImage.Width,
                //    srcImage.Height,
                //    GraphicsUnit.Pixel,
                //    imageAttributes);

                // No scaling
                g.DrawImage(srcImage,
                    new Rectangle(offsetX, offsetY, srcImage.Width, srcImage.Height),
                    0,
                    0,
                    srcImage.Width,
                    srcImage.Height,
                    GraphicsUnit.Pixel,
                    imageAttributes);
            }

            return alphaBlendedImage;
        }


        public static Bitmap ResizeImage(Image image, int width, int height)
        {
            //a holder for the result
            Bitmap result = new Bitmap(width, height);
            //set the resolutions the same to avoid cropping due to resolution differences
            result.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            double srcImageWidth = (double)image.Width;
            double srcImageHeight = (double)image.Height;

            // Determine which ratio to use
            double destToSrcRatioWidth = (double)width / srcImageWidth;
            double destToSrcRatioHeight = (double)height / srcImageHeight;
            double destToSrcRatio = Math.Min(destToSrcRatioWidth, destToSrcRatioHeight);

            // Resize the image based on the ratio
            int resizedImageWidth = (int)(srcImageWidth * destToSrcRatio);
            int resizedImageHeight = (int)(srcImageHeight * destToSrcRatio);


            int offsetX = Math.Abs(width - resizedImageWidth) / 2;
            int offsetY = Math.Abs(height - resizedImageHeight) / 2;

            //use a graphics object to draw the resized image into the bitmap
            using (Graphics graphics = Graphics.FromImage(result))
            {
                //set the resize quality modes to high quality
                graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                //draw the image into the target bitmap
                graphics.DrawImage(image, 0, 0, result.Width, result.Height);
            }

            //return the resulting bitmap
            return result;
        }
 
        public static Bitmap ResizeImage(Image srcImage, int destFrameWidth, int destFrameHeight, bool retainDestDimensions)
        {            
            double srcImageWidth = (double)srcImage.Width;
            double srcImageHeight = (double)srcImage.Height;

            // Determine which ratio to use
            double destToSrcRatioWidth = (double)destFrameWidth / srcImageWidth;
            double destToSrcRatioHeight = (double)destFrameHeight / srcImageHeight;
            double destToSrcRatio = destToSrcRatioWidth < destToSrcRatioHeight ? destToSrcRatioWidth : destToSrcRatioHeight;

            // Resize the image based on the ratio
            int resizedImageWidth = (int)(srcImageWidth * destToSrcRatio);
            int resizedImageHeight = (int)(srcImageHeight * destToSrcRatio);

            int offsetX = Math.Abs(destFrameWidth - resizedImageWidth) / 2;
            int offsetY = Math.Abs(destFrameHeight - resizedImageHeight) / 2 ;

            Bitmap canvas = null;

            if (retainDestDimensions)
            {
                canvas = new Bitmap(resizedImageWidth, resizedImageHeight);
            }
            else
            {
                canvas = new Bitmap(resizedImageWidth, resizedImageHeight);
            }

            using (Graphics gfx = Graphics.FromImage(canvas))
            {
                gfx.InterpolationMode = InterpolationMode.HighQualityBicubic;
                
                if (retainDestDimensions)
                {
                    //gfx.FillRectangle(Brushes.Transparent, 0, 0, destFrameWidth, destFrameHeight);
                    gfx.DrawImage(srcImage, 0, 0, destFrameWidth, destFrameHeight);
                }
                else
                {
                    //gfx.FillRectangle(Brushes.Transparent, 0, 0, resizedImageWidth, resizedImageHeight);
                    gfx.DrawImage(srcImage, 0, 0, resizedImageWidth, resizedImageHeight);
                }
            }

            return canvas;
        }

        public static string generateCarCutoffPNGName(string stkId)
        {
            return Constants.CAR_CUTOFF_PREFIX + stkId + ".png";
        }

        public static string generateStickerPNGName(string stkId)
        {
            //if(isDicut)return Constants.STK_DICUT_CUTOFF_PREFIX + stkId + ".png";
            //else 
            
            return Constants.STK_PRINT_CUTOFF_PREFIX + stkId + ".png";
            
        }

        public static Bitmap createCarCutOffPNG(Bitmap newBitmap, string stkId)
        {

            //Check order folder
            string strFullpath = getOrderFolderPath();
            if (!Directory.Exists(strFullpath))
            {
                //Create new folder
                Directory.CreateDirectory(strFullpath);
            }

            string newStickerRanderFileName = strFullpath + ImageUtilities.generateCarCutoffPNGName(stkId);
            if (System.IO.File.Exists(newStickerRanderFileName))
                System.IO.File.Delete(newStickerRanderFileName);

            //Bitmap mm = newBitmap;
            newBitmap.Save(newStickerRanderFileName, System.Drawing.Imaging.ImageFormat.Png);
            // newBitmap.Dispose();

            return newBitmap;
        }

        public static Bitmap createStickerCutOffPNG(Bitmap newBitmap, string stkId)
        {
            //Check order folder
            string strFullpath = getOrderFolderPath();

            if (!Directory.Exists(strFullpath))
            {
                //Create new folder
                Directory.CreateDirectory(strFullpath);
            }
            
            string newStickerRanderFileName = strFullpath + ImageUtilities.generateStickerPNGName(stkId);

            deleteExistsFile(newStickerRanderFileName);
            newBitmap.Save(newStickerRanderFileName, System.Drawing.Imaging.ImageFormat.Png);
            //newBitmap.Dispose();

            return newBitmap;
        }

        public static string getOrderFolderPath()
        {
            string strFullpath = Constants.BASE_PATH + Constants.DRAFT_DIR + "/" + Constants.orderID + "/";
            return strFullpath;
        }

        public static void deleteExistsFile(string fileNamePath)
        {
            if (System.IO.File.Exists(fileNamePath))
                System.IO.File.Delete(fileNamePath);

        }
        public static byte[] ImageToByteArray(Image imageIn)
        {
            if (imageIn == null) return null;

            var ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            return ms.ToArray();
        }

        public static Image ByteArrayToImage(byte[] byteArrayIn)
        {
            if (byteArrayIn == null || byteArrayIn.Length == 0) return null;

            var ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }
    }   
}