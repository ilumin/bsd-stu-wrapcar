﻿namespace WrapCar._30Appointment2
{
    partial class DataAppointment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.LbOrderID = new System.Windows.Forms.Label();
            this.appstartdate = new System.Windows.Forms.DateTimePicker();
            this.appenddate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnsave = new System.Windows.Forms.Button();
            this.WorkerList = new System.Windows.Forms.CheckedListBox();
            this.Search = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.planenddate = new System.Windows.Forms.DateTimePicker();
            this.planstartdate = new System.Windows.Forms.DateTimePicker();
            this.Manhours = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.CustName = new System.Windows.Forms.Label();
            this.lb_nameCus = new System.Windows.Forms.Label();
            this.TBOrderID = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // LbOrderID
            // 
            this.LbOrderID.AutoSize = true;
            this.LbOrderID.Location = new System.Drawing.Point(21, 45);
            this.LbOrderID.Name = "LbOrderID";
            this.LbOrderID.Size = new System.Drawing.Size(44, 13);
            this.LbOrderID.TabIndex = 1;
            this.LbOrderID.Text = "OrderID";
            // 
            // appstartdate
            // 
            this.appstartdate.Location = new System.Drawing.Point(168, 132);
            this.appstartdate.Name = "appstartdate";
            this.appstartdate.Size = new System.Drawing.Size(200, 20);
            this.appstartdate.TabIndex = 3;
            // 
            // appenddate
            // 
            this.appenddate.Location = new System.Drawing.Point(168, 168);
            this.appenddate.Name = "appenddate";
            this.appenddate.Size = new System.Drawing.Size(200, 20);
            this.appenddate.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 132);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Appointment Start Date";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 168);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Appointment Finish Date";
            // 
            // btnsave
            // 
            this.btnsave.Location = new System.Drawing.Point(24, 614);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(164, 42);
            this.btnsave.TabIndex = 7;
            this.btnsave.Text = "Save";
            this.btnsave.UseVisualStyleBackColor = true;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // WorkerList
            // 
            this.WorkerList.FormattingEnabled = true;
            this.WorkerList.Location = new System.Drawing.Point(24, 426);
            this.WorkerList.Name = "WorkerList";
            this.WorkerList.Size = new System.Drawing.Size(306, 184);
            this.WorkerList.TabIndex = 8;
            // 
            // Search
            // 
            this.Search.Location = new System.Drawing.Point(25, 211);
            this.Search.Name = "Search";
            this.Search.Size = new System.Drawing.Size(75, 23);
            this.Search.TabIndex = 9;
            this.Search.Text = "Search";
            this.Search.UseVisualStyleBackColor = true;
            this.Search.Click += new System.EventHandler(this.Search_Click);
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 345);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Plan Start";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 389);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Plan End";
            // 
            // planenddate
            // 
            this.planenddate.Location = new System.Drawing.Point(164, 389);
            this.planenddate.Name = "planenddate";
            this.planenddate.Size = new System.Drawing.Size(200, 20);
            this.planenddate.TabIndex = 12;
            // 
            // planstartdate
            // 
            this.planstartdate.Location = new System.Drawing.Point(164, 345);
            this.planstartdate.Name = "planstartdate";
            this.planstartdate.Size = new System.Drawing.Size(200, 20);
            this.planstartdate.TabIndex = 13;
            // 
            // Manhours
            // 
            this.Manhours.AutoSize = true;
            this.Manhours.Location = new System.Drawing.Point(165, 106);
            this.Manhours.Name = "Manhours";
            this.Manhours.Size = new System.Drawing.Size(145, 13);
            this.Manhours.TabIndex = 14;
            this.Manhours.Text = "----------------------------------------------";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 106);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Manhours";
            // 
            // CustName
            // 
            this.CustName.AutoSize = true;
            this.CustName.Location = new System.Drawing.Point(165, 83);
            this.CustName.Name = "CustName";
            this.CustName.Size = new System.Drawing.Size(151, 13);
            this.CustName.TabIndex = 16;
            this.CustName.Text = "------------------------------------------------";
            // 
            // lb_nameCus
            // 
            this.lb_nameCus.AutoSize = true;
            this.lb_nameCus.Location = new System.Drawing.Point(22, 83);
            this.lb_nameCus.Name = "lb_nameCus";
            this.lb_nameCus.Size = new System.Drawing.Size(82, 13);
            this.lb_nameCus.TabIndex = 17;
            this.lb_nameCus.Text = "Customer Name";
            // 
            // TBOrderID
            // 
            this.TBOrderID.FormattingEnabled = true;
            this.TBOrderID.Location = new System.Drawing.Point(168, 37);
            this.TBOrderID.Name = "TBOrderID";
            this.TBOrderID.Size = new System.Drawing.Size(121, 21);
            this.TBOrderID.TabIndex = 21;
            // 
            // DataAppointment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 677);
            this.Controls.Add(this.TBOrderID);
            this.Controls.Add(this.lb_nameCus);
            this.Controls.Add(this.CustName);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Manhours);
            this.Controls.Add(this.planstartdate);
            this.Controls.Add(this.planenddate);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Search);
            this.Controls.Add(this.WorkerList);
            this.Controls.Add(this.btnsave);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.appenddate);
            this.Controls.Add(this.appstartdate);
            this.Controls.Add(this.LbOrderID);
            this.Name = "DataAppointment";
            this.Text = "AppointmentForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LbOrderID;
        private System.Windows.Forms.DateTimePicker appstartdate;
        private System.Windows.Forms.DateTimePicker appenddate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.CheckedListBox WorkerList;
        private System.Windows.Forms.Button Search;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker planenddate;
        private System.Windows.Forms.DateTimePicker planstartdate;
        private System.Windows.Forms.Label Manhours;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label CustName;
        private System.Windows.Forms.Label lb_nameCus;
        private System.Windows.Forms.ComboBox TBOrderID;
    }
}