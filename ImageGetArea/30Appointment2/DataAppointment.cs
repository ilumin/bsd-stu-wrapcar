﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WrapCar_DataModel;
using WrapCar_Connection;
using System.Windows.Documents;

namespace WrapCar._30Appointment2
{
    public partial class DataAppointment : Form
    {
        public DataAppointment()
        {
            InitializeComponent();


            var appointment = new AppointmentDAO();//get info from dao

            List<AppointmentData> mockdataes = new List<AppointmentData>(); // get all task 
            mockdataes = appointment.getAppointmentDataList(); // assign to variable

            //Loop for assign variable to list

            List<OrderList> orderlist = new List<OrderList>();
            orderlist = appointment.getOrderNoApp();
            for(int i=0; i< orderlist.Count; i++){
                TBOrderID.Items.Add(orderlist[i].orderid);
            }

            List<getEmployee> employeedata = new List<getEmployee>(); // get all task 
            employeedata = appointment.getEmployee(); // assign to variable
    

            DateTime DateStart = new DateTime(2014, 3, 1);
            DateTime DateEnd = new DateTime(2014, 3, 31);
            TimeSpan totalDate = (DateEnd - DateStart);
           // Console.WriteLine(Convert.ToInt16(DateStart.DayOfWeek));

           

                     
           

             
        }

        private void Search_Click(object sender, EventArgs e)
        {
            var appointment = new AppointmentDAO();//get info from dao

            List<AppointmentOrderData> mockdataes = new List<AppointmentOrderData>(); // get all task 
            String OrderID = TBOrderID.Text;
            mockdataes = appointment.getAppointmentOrderList(OrderID);// assign to variable
            for (int i = 0; i < mockdataes.Count(); i++)
            {
                CustName.Text = mockdataes[i].Customername;
                Manhours.Text = Convert.ToString(mockdataes[i].manhour);

                //  appstartdate.Value = mockdataes[i].orderdate.AddDays(5); // Default 5 day for printed;
                // appenddate.Value = mockdataes[i].orderdate.AddDays(30); // Search Date Between order

            }

            // Search Recommended Date


            // Find Avaliable Date

            List<DateTime> holiday = new List<DateTime>();
            List<Holiday> holidaydates = new List<Holiday>();
            holidaydates = appointment.HolidayList();

            for (int i = 0; i < holidaydates.Count; i++)
            {
                holiday.Add(holidaydates[i].holidaydate);
            }
            int periodhourperday = 8;
            int perioddate = 30;
            DateTime startNewJob = Convert.ToDateTime(appstartdate.Text);//Convert.ToDateTime(appstartdate.Text);
            DateTime endperiodJob = Convert.ToDateTime(appenddate.Text);//startNewJob.AddDays(perioddate);
            int slotday = 0;

            for (int i = 0; i < perioddate; i++)
            {
                DateTime checkdate = startNewJob.AddDays(i);
                if (checkdate.DayOfWeek == DayOfWeek.Saturday || checkdate.DayOfWeek == DayOfWeek.Sunday)
                {
                    continue;
                }

                if (holiday.Contains(checkdate))
                {
                    continue;
                }
                slotday++;
            }


            List<getEmployee> employeedata = new List<getEmployee>(); // get all task 
            employeedata = appointment.getEmployee(); // assign to variable

            List<double> employeeID = new List<double>();

            int allestdate = perioddate * periodhourperday;
            int[,] slothour = new int[allestdate, employeedata.Count]; // slot work

            for (int i = 0; i < employeedata.Count; i++)
            {
                employeeID.Add(employeedata[i].workerid);
            }


            List<getUnAvaliableworker> Timeschedule = new List<getUnAvaliableworker>();
            Timeschedule = appointment.getUnAvaliableTime(startNewJob, endperiodJob);

            WorkerList.Items.Clear();



            //  MessageBox.Show(employeedata.Count.ToString());

            for (int k = 0; k < employeedata.Count; k++)
            {
                WorkerList.Items.Insert(k, employeedata[k].workername);
                WorkerList.SetItemChecked(k, true);
            }


            if (Timeschedule.Count() > 0)
            {
                for (int i = 0; i < Timeschedule.Count(); i++)
                {
                    if (Timeschedule[i].status == "NOT VALID")
                    {

                        for (int k = 0; k < employeedata.Count; k++)
                        {

                            if (employeedata[k].workerid == Timeschedule[i].workerid)
                            {
                                WorkerList.Items.Remove(employeedata[k].workername);

                            }

                        }

                    }
                }
            }
        }
            
             

        private void btnsave_Click(object sender, EventArgs e)
        {
            var appointment = new AppointmentDAO();//get info from dao
            List<getEmployee> employeedata = new List<getEmployee>(); // get all task 
            employeedata = appointment.getEmployee(); // assign to variable
            
            tb_r_appointment data = new tb_r_appointment();
            data.ORDER_H_ID = TBOrderID.Text;
            data.APP_TITLE_APPOIINT = "Order Sticker:"+ TBOrderID.Text;
            data.APP_DATETIME_START = Convert.ToDateTime(appstartdate.Text);
            data.APP_DATETIME_FINISH = Convert.ToDateTime(appenddate.Text);
            data.DURATION_HH = Convert.ToDouble(Manhours.Text);
            data.PLAN_DATETIME_START = Convert.ToDateTime(planstartdate.Text);
            data.PLAN_DATETIME_END = Convert.ToDateTime(planenddate.Text);
            TimeSpan ts_check = (Convert.ToDateTime(planenddate.Text) - Convert.ToDateTime(planstartdate.Text));
            data.PLAN_DURATION_HOUR = ts_check.TotalHours;
            data.STATUS_APP_POINTMENT = "WAITING";
            
            AppointmentDAO dao = new AppointmentDAO();
            bool ret = dao.saveAppointment(data);
            bool ret2 = false;

           // Console.WriteLine(data.APP_ID);

            for (int i = 0; i < WorkerList.Items.Count; i++ )
            {
               // Console.WriteLine(WorkerList.Items[i]);
                if (WorkerList.GetItemCheckState(i) == CheckState.Checked) {
                    tb_r_tasks_schedule data2 = new tb_r_tasks_schedule();
                    data2.ORDER_H_ID = TBOrderID.Text;
                    data2.APP_ID = data.APP_ID;
                    data2.PLAN_START_DATETIME = Convert.ToDateTime(planstartdate.Text);
                    data2.PLAN_FINISH_DATETIME = Convert.ToDateTime(planenddate.Text);
                    data2.PLAN_DURATION_HH = Convert.ToDecimal(ts_check.TotalHours);
                    data2.EMP_ID = employeedata[i].workerid;
                    data2.WORK_STATUS = 0;
                    
                     ret2 = dao.saveSchedule(data2);
                     if (ret2 == false) {
                         break;
                     }
                }
            }
            if (ret && ret2)
            {
                MessageBox.Show("บันทึกข้อมูลสำเร็จ");

            }
            else
            {
                MessageBox.Show("บันทึกข้อมูลไม่สำเร็จ กรุณาตรวจสอบใหม่");
            }
            
          
            }
    }
}
