﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WrapCar_Connection;
using WrapCar_DataModel;
using WrapCar_DataModel.Sticker;
using WrapCar_DataModel.Order;

namespace WrapCar._00Utilities
{
    class CalEffort
    {
        StickerMHDAO dao = new StickerMHDAO();

        private decimal calEff(decimal area, int stickerid)
        {
            StickerItemData data = dao.getMH_RATE(stickerid);
            decimal result;
            if (area <= 20) { result = 1; }
            else if (area <= 100) { result = 2; }
            else if (area <= 400) { result = 4; }
            else if (area <= 800) { result = 6; }
            else { result = 8; }
            
            result = Convert.ToDecimal(result) * Convert.ToDecimal(data.MH_RATE);
            return Math.Round( result, 0);
                
        }

        public decimal calEff_List(List<OrderDData> data)
        {
            decimal result = 0;
            for (int i = 0; i < data.Count; i++)
            {
                result += calEff(Convert.ToDecimal(data[i].AREA_USED_INCH), data[i].STK_ITEM_ID);
            }

            return result;
                
        }



    }

}
