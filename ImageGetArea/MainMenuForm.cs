﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WrapCar._10Customer;
using WrapCar._20Desing;
using WrapCar._30Appointment2;
using WrapCar._40Schedule;
using WrapCar._60Master;
using WrapCar._80CarInOut;
namespace WrapCar
{
    public partial class MainMenuForm : Form
    {
        public MainMenuForm()
        {
            InitializeComponent();
        }

        private void  btnDesignForm_Click(object sender, EventArgs e)
        {
            //DesignForm form = new DesignForm();
            CustomerOrderForm form = new CustomerOrderForm();
            form.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            CustomerForm form = new CustomerForm();
            form.Show();
        }

        private void MainMenuForm_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            MainMaster form = new MainMaster();
            form.Show();
        }

        private void apppointmentBtn_Click(object sender, EventArgs e)
        {
            DataAppointment form = new DataAppointment();
            form.Show();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            ScheduleForm form = new ScheduleForm();
            form.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CarInOut form = new CarInOut();
            form.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DataAppointment form = new DataAppointment();
            form.Show();
        }
    }
}
