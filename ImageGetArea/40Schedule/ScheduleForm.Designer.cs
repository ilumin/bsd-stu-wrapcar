﻿namespace WrapCar._40Schedule
{
    partial class ScheduleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.selectLate = new System.Windows.Forms.RadioButton();
            this.selectFinished = new System.Windows.Forms.RadioButton();
            this.selectWorking = new System.Windows.Forms.RadioButton();
            this.selectPending = new System.Windows.Forms.RadioButton();
            this.selectAll = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(0, 61);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(775, 498);
            this.dataGridView1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.selectLate);
            this.groupBox1.Controls.Add(this.selectFinished);
            this.groupBox1.Controls.Add(this.selectWorking);
            this.groupBox1.Controls.Add(this.selectPending);
            this.groupBox1.Controls.Add(this.selectAll);
            this.groupBox1.Location = new System.Drawing.Point(12, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(751, 39);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filter task";
            // 
            // selectLate
            // 
            this.selectLate.AutoSize = true;
            this.selectLate.Location = new System.Drawing.Point(254, 14);
            this.selectLate.Name = "selectLate";
            this.selectLate.Size = new System.Drawing.Size(42, 17);
            this.selectLate.TabIndex = 6;
            this.selectLate.TabStop = true;
            this.selectLate.Text = "late";
            this.selectLate.UseVisualStyleBackColor = true;
            // 
            // selectFinished
            // 
            this.selectFinished.AutoSize = true;
            this.selectFinished.Location = new System.Drawing.Point(187, 15);
            this.selectFinished.Name = "selectFinished";
            this.selectFinished.Size = new System.Drawing.Size(61, 17);
            this.selectFinished.TabIndex = 6;
            this.selectFinished.TabStop = true;
            this.selectFinished.Text = "finished";
            this.selectFinished.UseVisualStyleBackColor = true;
            // 
            // selectWorking
            // 
            this.selectWorking.AutoSize = true;
            this.selectWorking.Location = new System.Drawing.Point(119, 14);
            this.selectWorking.Name = "selectWorking";
            this.selectWorking.Size = new System.Drawing.Size(62, 17);
            this.selectWorking.TabIndex = 5;
            this.selectWorking.TabStop = true;
            this.selectWorking.Text = "working";
            this.selectWorking.UseVisualStyleBackColor = true;
            // 
            // selectPending
            // 
            this.selectPending.AutoSize = true;
            this.selectPending.Location = new System.Drawing.Point(50, 14);
            this.selectPending.Name = "selectPending";
            this.selectPending.Size = new System.Drawing.Size(63, 17);
            this.selectPending.TabIndex = 5;
            this.selectPending.TabStop = true;
            this.selectPending.Text = "pending";
            this.selectPending.UseVisualStyleBackColor = true;
            // 
            // selectAll
            // 
            this.selectAll.AutoSize = true;
            this.selectAll.Checked = true;
            this.selectAll.Location = new System.Drawing.Point(9, 14);
            this.selectAll.Name = "selectAll";
            this.selectAll.Size = new System.Drawing.Size(35, 17);
            this.selectAll.TabIndex = 4;
            this.selectAll.TabStop = true;
            this.selectAll.Text = "all";
            this.selectAll.UseVisualStyleBackColor = true;
            // 
            // ScheduleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(775, 559);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "ScheduleForm";
            this.Text = "Schedule";
            this.Load += new System.EventHandler(this.ScheduleForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton selectLate;
        private System.Windows.Forms.RadioButton selectFinished;
        private System.Windows.Forms.RadioButton selectWorking;
        private System.Windows.Forms.RadioButton selectPending;
        private System.Windows.Forms.RadioButton selectAll;
    }
}