﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WrapCar_Connection;
using WrapCar_DataModel;

namespace WrapCar._40Schedule
{
    public partial class ScheduleForm : Form
    {
        private Dictionary<int, string> status_text = new Dictionary<int, string>()
        { 
            { 0, "Pending" },
            { 1, "Working" },
            { 2, "Finished" }
        };

        private string filterObject = "all";
        private DateTime currentDate = new System.DateTime();
        private ScheduleDAO dao = new ScheduleDAO();
        private List<ScheduleData> scheduleDataList = new List<ScheduleData>();

        public ScheduleForm()
        {
            InitializeComponent();

            // MessageBox.Show(String.Format("{0}", this.currentDate));

            var checkButton = groupBox1.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked);

            this.datagrid_init();
            this.datagrid_load();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0 || e.ColumnIndex != dataGridView1.Columns["ButtonEdit"].Index)
                return;

            // use task id to get task information
            int taskId = (int) dataGridView1.Rows[e.RowIndex].Cells[1].Value;

            ScheduleUpdateForm childform = new ScheduleUpdateForm(taskId);
            childform.Show();

            this.Close();
        }

        /**
         * Create datagrid column
         */
        private void datagrid_init()
        {
            dataGridView1.ColumnCount = 6;
            dataGridView1.Columns[0].Name = "Order ID";
            dataGridView1.Columns[1].Name = "Task ID";
            dataGridView1.Columns[2].Name = "Employee";
            dataGridView1.Columns[3].Name = "Status";
            dataGridView1.Columns[4].Name = "Start date";
            dataGridView1.Columns[5].Name = "Due date";

            // tools column[6]
            DataGridViewButtonColumn col_edit = new DataGridViewButtonColumn();
            col_edit.Name = "ButtonEdit";
            col_edit.HeaderText = "Update";
            dataGridView1.Columns.Add(col_edit);

            dataGridView1.CellClick += new DataGridViewCellEventHandler(dataGridView1_CellClick);

            this.datagrid_clear();
        }

        private void datagrid_clear()
        {
            dataGridView1.Rows.Clear();
        }

        private void datagrid_load()
        {
            // clear data before load
            this.datagrid_clear();

            // MessageBox.Show(String.Format("Clear {0}", this.filterObject));

            // load data 
            List<ScheduleData> scheduleDataList = dao.getScheduleList(this.filterObject);

            int row = 0;
            object[] temp = new object[7];
            List<DataGridViewRow> rows_item = new List<DataGridViewRow>();
            foreach (ScheduleData item in scheduleDataList)
            {
                row = row + 1;

                temp[0] = item.ORDER_H_ID;
                temp[1] = item.TASK_ID;
                temp[2] = item.EMP_NAME;
                temp[3] = this.status_text[(int)item.WORK_STATUS];
                temp[4] = ((DateTime) item.PLAN_START_DATETIME).ToString("d/M/yyyy");
                temp[5] = ((DateTime) item.PLAN_FINISH_DATETIME).ToString("d/M/yyyy");
                temp[6] = "UPDATE";

                // dataGridView1.Rows.Add(dataRow);
                rows_item.Add(new DataGridViewRow());
                rows_item[rows_item.Count - 1].CreateCells(dataGridView1, temp);

                // MessageBox.Show(String.Format("Row {0}", row));
            }

            dataGridView1.Rows.AddRange(rows_item.ToArray());
        }

        private void ScheduleForm_Load(object sender, EventArgs e)
        {
            selectAll.CheckedChanged += new EventHandler(radioButtons_CheckedChanged);
            selectPending.CheckedChanged += new EventHandler(radioButtons_CheckedChanged);
            selectWorking.CheckedChanged += new EventHandler(radioButtons_CheckedChanged);
            selectFinished.CheckedChanged += new EventHandler(radioButtons_CheckedChanged);
            selectLate.CheckedChanged += new EventHandler(radioButtons_CheckedChanged);

            var checkButton = groupBox1.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked);
            this.filterObject = checkButton.Text;
        }

        private void radioButtons_CheckedChanged(object s, EventArgs e)
        {
            RadioButton radioButton = s as RadioButton;

            if (radioButton != null)
            {
                if (radioButton.Checked)
                {
                    this.filterObject = radioButton.Text;

                    // load data
                    this.datagrid_load();
                }
            }
        }
    }
}
