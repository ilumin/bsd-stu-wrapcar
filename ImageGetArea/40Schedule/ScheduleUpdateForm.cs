﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WrapCar_Connection;
using WrapCar_DataModel;

namespace WrapCar._40Schedule
{
    public partial class ScheduleUpdateForm : Form
    {
        private int taskId;
        private ScheduleData schedule;
        private ScheduleDAO dao = new ScheduleDAO();

        public ScheduleUpdateForm(int taskId)
        {
            InitializeComponent();

            this.taskId = taskId;

            this.schedule = this.dao.getSchedule(this.taskId);

            // Display
            // - Start Date
            // - End Date
            // - Status (as dropdown: finished, start)

            tableLayoutPanel1.ColumnCount = 2;
            tableLayoutPanel1.RowCount = 7;

            this.addTextCell("Start Date", 0, 0);
            this.addTextCell(schedule.PLAN_START_DATETIME.ToString(), 0, 1);

            this.addTextCell("Due Date", 1, 0);
            this.addTextCell(schedule.PLAN_FINISH_DATETIME.ToString(), 1, 1);

            Dictionary<int, string> statusValue = new Dictionary<int, string>();
            statusValue.Add(0, "Pending");
            statusValue.Add(1, "Working");
            statusValue.Add(2, "Finished");

            // List<ComboboxItem> list = new List<ComboboxItem>(); 

            ComboBox scheduleStatus = new ComboBox()
            {
                Name = "scheduleStatus",
                // DataSource = statusValue, //new BindingSource(statusValue, null),
                // DisplayMember = "Value",
                // ValueMember = "Key",
                // SelectedItem = 1
            };

            scheduleStatus.Items.AddRange(new object[] {
                "Pending",
                "Working",
                "Finished"
            });

            scheduleStatus.SelectedIndex = schedule.WORK_STATUS;

            // set default value
            // scheduleStatus.Text = statusValue[schedule.WORK_STATUS];
            // scheduleStatus.SelectedValue = schedule.WORK_STATUS;
            // scheduleStatus.SelectedValue = statusValue[schedule.WORK_STATUS];
            // scheduleStatus.SelectedItem = schedule.WORK_STATUS;

            this.addTextCell("Status", 2, 0);
            tableLayoutPanel1.Controls.Add(scheduleStatus, 1, 2);
        }

        private void addTextCell(string data, int row, int column)
        {
            tableLayoutPanel1.Controls.Add(new Label()
            {
                Text = data
            }, column, row);
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Control c = tableLayoutPanel1.GetControlFromPosition(1, 2);
            var status_text = c.Text;
            var status_code = 0;
            // MessageBox.Show(String.Format("Yeeeha {0} {1}", this.taskId, c.Text));

            switch (status_text)
            {
                case "Finished":
                    status_code = 2;
                    break;
                case "Working":
                    status_code = 1;
                    break;
                case "Pending":
                default:
                    status_code = 0;
                    break;
            }

            this.dao.updateScheduleStatus(this.taskId, status_code);

            MessageBox.Show("Schedule Update!");

            var scheduleForm = new ScheduleForm();
            scheduleForm.Show();

            this.Close();
        }
    }
}
