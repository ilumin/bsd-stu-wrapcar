﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WrapCar_Connection;
using WrapCar_Connection.Model;
using WrapCar_DataModel.Model;

namespace WrapCar._60Master
{
    public partial class ModelAdd : Form
    {
        public ModelAdd()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void ModelHAdd_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;

            this.btnLeftSide.Enabled = false;
            this.hiddModelDID.Text = "" + this.LEFT_VIEW;
            this.clearImageList();
        }

        private void clearImageList()
        {
            this.imageList = null;
            this.imageList = new Dictionary<int, Image>();
            this.imageList.Add(0, null);
            this.imageList.Add(1, null);
            this.imageList.Add(2, null);
            this.imageList.Add(3, null);
            this.imageList.Add(4, null);
            this.pictureBox1.Image = null;
        }

        Dictionary<int, Image> imageList;
        private void button1_Click_1(object sender, EventArgs e)
        {
            string image_file = "";
            openFileDialog1.Title = "เลือกไฟล์รูปภาพ";
            openFileDialog1.FileName = "";
            openFileDialog1.Filter = "Image files (*.png) | *.png";

            if (openFileDialog1.ShowDialog() != DialogResult.Cancel)
            {
                image_file = openFileDialog1.FileName;
                txtImgPath.Text = image_file;
                this.pictureBox1.Image = Image.FromFile(image_file);
                this.pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;

                if (hiddModelDID.Text != "")
                    this.imageList[Convert.ToInt32(hiddModelDID.Text)] = this.pictureBox1.Image;
            }
        }

        int LEFT_VIEW = 0;
        int TOP_VIEW = 1;
        int FORNT_VIEW = 2;
        int RIGHT_VIEW = 3;
        int BACK_VIEW = 4;
        string[] SIDE_VIEW = { "มุมมองด้านซ้าย", "มุมมองด้านบน", "มุมมองด้านหน้า", "มุมมองด้านขวา", "มุมมองด้านหลัง" };

        private void btnLeftSide_Click(object sender, EventArgs e)
        {
            hiddModelDID.Text = "" + this.LEFT_VIEW;
            label7.Text = "มุมมองด้านซ้าย";

            this.resetCarSideToEnable();
            this.btnLeftSide.Enabled = false;

            //Switch Show LEFT_VIEW
            Image imgView = this.imageList[Convert.ToInt32(LEFT_VIEW)];
            this.pictureBox1.Image = imgView;

        }
        private void btnTopSide_Click(object sender, EventArgs e)
        {
            hiddModelDID.Text = "" + this.TOP_VIEW;
            label7.Text = "มุมมองด้านบน";

            this.resetCarSideToEnable();
            this.btnTopSide.Enabled = false;

            //Switch Show TOP_VIEW
            Image imgView = this.imageList[Convert.ToInt32(TOP_VIEW)];
            this.pictureBox1.Image = imgView;
        }
        private void btnFrontSide_Click(object sender, EventArgs e)
        {
            hiddModelDID.Text = "" + this.FORNT_VIEW;
            label7.Text = "มุมมองด้านหน้า";

            this.resetCarSideToEnable();
            this.btnFrontSide.Enabled = false;

            //Switch Show TOP_VIEW
            Image imgView = this.imageList[Convert.ToInt32(FORNT_VIEW)];
            this.pictureBox1.Image = imgView;
        }
        private void btnRightSide_Click(object sender, EventArgs e)
        {
            hiddModelDID.Text = "" + this.RIGHT_VIEW;
            label7.Text = "มุมมองด้านขวา";

            this.resetCarSideToEnable();
            this.btnRightSide.Enabled = false;

            //Switch Show RIGHT_VIEW
            Image imgView = this.imageList[Convert.ToInt32(RIGHT_VIEW)];
            this.pictureBox1.Image = imgView;
        }
        private void btnBackSide_Click(object sender, EventArgs e)
        {
            hiddModelDID.Text = "" + this.BACK_VIEW;
            label7.Text = "มุมมองด้านหลัง";

            this.resetCarSideToEnable();
            this.btnBackSide.Enabled = false;

            //Switch Show BACK_VIEW
            Image imgView = this.imageList[Convert.ToInt32(BACK_VIEW)];
            this.pictureBox1.Image = imgView;
        }
        private void resetCarSideToEnable()
        {
            this.btnLeftSide.Enabled = true;
            this.btnRightSide.Enabled = true;
            this.btnTopSide.Enabled = true;
            this.btnFrontSide.Enabled = true;
            this.btnBackSide.Enabled = true;
        }

        private void txtCarWidth_TextChanged(object sender, EventArgs e)
        {
            if (txtCarWidth.Text != "" && txtCarHeight.Text != null)
            {
                decimal widthInch = Convert.ToDecimal(txtCarWidth.Text);
                decimal heightInch = Convert.ToDecimal(txtCarHeight.Text);

                decimal widthPixel = Constants.DEFAULT_DPI * widthInch;
                decimal heightPixel = Constants.DEFAULT_DPI * heightInch;

                decimal ratio = (Convert.ToDecimal(pictureBox1.Width) * 100) / widthPixel;

                txtRatio.Text = "" + ratio;
            }

        }

        private void txtCarHeight_TextChanged(object sender, EventArgs e)
        {
            if (txtCarWidth.Text != "" && txtCarHeight.Text != null)
            {
                decimal widthInch = Convert.ToDecimal(txtCarWidth.Text);
                decimal heightInch = Convert.ToDecimal(txtCarHeight.Text);

                decimal widthPixel = Constants.DEFAULT_DPI * widthInch;
                decimal heightPixel = Constants.DEFAULT_DPI * heightInch;

                decimal ratio = (Convert.ToDecimal(pictureBox1.Width) * 100) / widthPixel;

                txtRatio.Text = "" + ratio;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            
           bool isValid = this.validateBeforeSave();
           if (isValid)
           {
               bool isSuccess = true;

               ModelDAO dao = new ModelDAO();
               List<ModelHData> dataHOldList = dao.getModelAll();
               //ModelHData dataHOld = dataHOldList.Find(t => (t.MODEL_BRAND == txtBrand.Text) &&
               //                                             (t.MODEL_SERIES == txtModel.Text) &&
               //                                             (t.MODEL_YEAR == txtYear.Text));
               //List<ModelDData> dataDOld = dataHOld.ModelDList.ToList();

               //For Insert New
               tb_m_model_h modelH = new tb_m_model_h();
               modelH.MODEL_BRAND = txtBrand.Text;
               modelH.MODEL_SERIES = txtModel.Text;
               modelH.MODEL_YEAR = txtYear.Text;
               modelH.MODEL_NAME = "โครงรถ " + txtBrand.Text + " รุ่น " + txtModel.Text + " โฉมปี " + txtYear.Text;
               isSuccess = dao.saveModelH(modelH);

               int i = 0;
               foreach(int key in this.imageList.Keys)
               {
                   Image img = ImageUtilities.ResizeImage(this.imageList[key], pictureBox1.Width, pictureBox1.Height);

                    tb_m_model_d modelD = new tb_m_model_d();
                    modelD.MODEL_D_ID = key;
                    modelD.MODEL_H_ID = modelH.MODEL_H_ID;
                    modelD.MODEL_HEIGHT = Convert.ToDecimal(txtCarHeight.Text);
                    modelD.MODEL_WIDTH = Convert.ToDecimal(txtCarWidth.Text);
                    modelD.MODEL_SIDE = SIDE_VIEW[i];
                    modelD.MODELIMAGE = ImageUtilities.ImageToByteArray(img);
                    modelD.MODEL_RATIO = txtRatio.Text;

                    isSuccess = dao.saveModelD(modelD);
                    
                    i++;
               }
               
               if(isSuccess)
                MessageBox.Show("บันทึกข้อมูลสำเร็จ");
           }
        }


        private bool validateBeforeSave()
        {

            bool isContainEmpty = (txtBrand.Text == "") || (txtModel.Text == "") || (txtYear.Text == ""
                                  || (txtCarWidth.Text == "") || (txtCarHeight.Text == "") || (txtRatio.Text == ""));
            if (isContainEmpty)
            {
                MessageBox.Show("กรุณาเลือกรายการรถ รุ่น และโฉมปีที่ต้องการ");
                return false;
            }

            isContainEmpty = this.imageList.ContainsValue(null);
            if (isContainEmpty)
            {
                MessageBox.Show("กรุณากำหนด มุมมองของรถรุ่น " + txtBrand.Text + " " + txtModel.Text + " ปี " + txtYear.Text + " ให้ครบทั้ง 5 มุมมอง");
                return false;
            }

            return true;
        }

    }
}
