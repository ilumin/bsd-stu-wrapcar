﻿namespace WrapCar._60Master
{
    partial class MainMaster
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.stickerCat = new System.Windows.Forms.Button();
            this.stickerItem = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // stickerCat
            // 
            this.stickerCat.Location = new System.Drawing.Point(29, 22);
            this.stickerCat.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.stickerCat.Name = "stickerCat";
            this.stickerCat.Size = new System.Drawing.Size(95, 40);
            this.stickerCat.TabIndex = 0;
            this.stickerCat.Text = "ประเภทลายสติ๊กเกอร์";
            this.stickerCat.UseVisualStyleBackColor = true;
            this.stickerCat.Click += new System.EventHandler(this.stickerCat_Click);
            // 
            // stickerItem
            // 
            this.stickerItem.Location = new System.Drawing.Point(29, 81);
            this.stickerItem.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.stickerItem.Name = "stickerItem";
            this.stickerItem.Size = new System.Drawing.Size(95, 40);
            this.stickerItem.TabIndex = 1;
            this.stickerItem.Text = "จัดการลายสติ๊กเกอร์";
            this.stickerItem.UseVisualStyleBackColor = true;
            this.stickerItem.Click += new System.EventHandler(this.button1_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(348, 58);
            this.button1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(95, 40);
            this.button1.TabIndex = 2;
            this.button1.Text = "จัดการโครงรถ";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // MainMaster
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(588, 282);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.stickerItem);
            this.Controls.Add(this.stickerCat);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "MainMaster";
            this.Text = "MainMaster";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button stickerCat;
        private System.Windows.Forms.Button stickerItem;
        private System.Windows.Forms.Button button1;
    }
}