﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WrapCar_Connection;
using WrapCar_DataModel;

namespace WrapCar._60Master
{
    public partial class StickerCatAdd : Form
    {
        public StickerCatAdd()
        {
            InitializeComponent();
        }

        private void StickerCatAdd_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            tb_m_sticker_category data = new tb_m_sticker_category();
            data.STK_CATEGORY_NAME = textBox1.Text;

            StickerCategoryDAO dao = new StickerCategoryDAO();
            bool ret = dao.saveStickerCategory(data);
            if (ret)
            {
                MessageBox.Show("บันทึกข้อมูลสำเร็จ");
            }
            else
            {
                MessageBox.Show("บันทึกข้อมูลไม่สำเร็จ กรุณาตรวจสอบใหม่");
            }

           
        }
    }
}
