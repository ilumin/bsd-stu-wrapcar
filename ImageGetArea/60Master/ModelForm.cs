﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WrapCar_Connection;
using WrapCar_Connection.Model;
using WrapCar_DataModel.Model;


namespace WrapCar._60Master
{
    public partial class ModelForm : Form
    {
        Dictionary<int, Image> imageList;
        public ModelForm()
        {
            InitializeComponent();
        }


        private void ModelHForm_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            this.BilningCarBrandCombo();

            this.btnLeftSide.Enabled = false;
            this.hiddModelDID.Text = ""+this.LEFT_VIEW;
            this.clearImageList();
        }

        private void clearImageList()
        {
            this.imageList = null;
            this.imageList = new Dictionary<int, Image>();
            this.imageList.Add(0, null);
            this.imageList.Add(1, null);
            this.imageList.Add(2, null);
            this.imageList.Add(3, null);
            this.imageList.Add(4, null);
            this.pictureBox1.Image = null;
        }

        private void BilningCarBrandCombo()
        {
          List<string> brandList =  Utilities.Utilities.GET_STR_CAR_BRAND_MASTER_ALL();
          this.cmbCarBrand.DataSource = brandList.ToArray();
        }

        private void cmbCarBrand_SelectedIndexChanged(object sender, EventArgs e)
        {
           string selcetedBrand = this.cmbCarBrand.SelectedItem.ToString();
           this.BilningCarBrandCombo(selcetedBrand);

        }

        List<ModelHData> modelList;
        private void BilningCarBrandCombo(string selcetedBrand)
        {
            modelList = Utilities.Utilities.GET_OBJ_CAR_MODEL_BY_BRAND(selcetedBrand);
            tvModelSeries.Nodes.Clear();
            this.BildingOrderTreeView(modelList, -1);
        }

        private void tvModelSeries_AfterExpand(object sender, TreeViewEventArgs e)
        {
            tvModelSeries.Nodes[e.Node.Index].Nodes.Clear();
            this.BildingOrderTreeView(modelList, e.Node.Index);
        }

        private void BildingOrderTreeView(List<ModelHData> modelList, int nodeIdx)
        {

            foreach (ModelHData element in modelList)
            {
                int driveImage = 6;
                TreeNode node = new TreeNode(element.MODEL_SERIES, driveImage, driveImage);
                node.Tag = element.MODEL_SERIES;

                if (nodeIdx == -1)
                {
                    node.Nodes.Add("");
                    tvModelSeries.Nodes.Add(node);
                }
                else
                {
                    tvModelSeries.Nodes[nodeIdx].Nodes.Add(element.MODEL_YEAR);
                }
            }
        }

        private void tvModelSeries_AfterSelect(object sender, TreeViewEventArgs e)
        {
           //the last level
           if (e.Node.Parent == null) return;

           TreeNode nodeSelected = tvModelSeries.SelectedNode;

           string[] spliteNode = nodeSelected.FullPath.Split(@"\\".ToArray());
           string brandName = cmbCarBrand.SelectedItem.ToString();
           string modelName = spliteNode[0];
           string modelYear = spliteNode[1];

           txtBrand.Text = brandName;
           txtModel.Text = modelName;
           txtYear.Text = modelYear;

           ModelHData modelYearData = Utilities.Utilities.GET_OBJ_CAR_MODEL_BY_BRAND(brandName, modelName, modelYear);

           if (modelYearData != null)
           {
               foreach (ModelDData data in modelYearData.ModelDList)
               {
                   if (data.MODELIMAGE != null)
                   {
                       imageList[data.MODEL_D_ID] = ImageUtilities.ByteArrayToImage(data.MODELIMAGE);
                   }
               }
               //Default Show LEFT_VIEW
               this.pictureBox1.Image = imageList[this.LEFT_VIEW];
           }
           else
           {
               this.clearImageList();
           }
        }

        int LEFT_VIEW = 0;
        int TOP_VIEW = 1; 
        int FORNT_VIEW = 2;
        int RIGHT_VIEW = 3; 
        int BACK_VIEW = 4;
        string[] SIDE_VIEW = { "มุมมองด้านซ้าย", "มุมมองด้านบน", "มุมมองด้านหน้า", "มุมมองด้านขวา", "มุมมองด้านหลัง" };

        private void btnLeftSide_Click(object sender, EventArgs e)
        {
            hiddModelDID.Text = ""+this.LEFT_VIEW;
            label7.Text = "มุมมองด้านซ้าย";

            this.resetCarSideToEnable();
            this.btnLeftSide.Enabled = false;

            //Switch Show LEFT_VIEW
            Image imgView = this.imageList[Convert.ToInt32(LEFT_VIEW)];
            this.pictureBox1.Image = imgView;
            
        }
        private void btnTopSide_Click(object sender, EventArgs e)
        {
            hiddModelDID.Text = "" + this.TOP_VIEW;
            label7.Text = "มุมมองด้านบน";

            this.resetCarSideToEnable();
            this.btnTopSide.Enabled = false;

            //Switch Show TOP_VIEW
            Image imgView = this.imageList[Convert.ToInt32(TOP_VIEW)];
            this.pictureBox1.Image = imgView;
        }
        private void btnFrontSide_Click(object sender, EventArgs e)
        {
            hiddModelDID.Text = "" + this.FORNT_VIEW;
            label7.Text = "มุมมองด้านหน้า";

            this.resetCarSideToEnable();
            this.btnFrontSide.Enabled = false;

            //Switch Show TOP_VIEW
            Image imgView = this.imageList[Convert.ToInt32(FORNT_VIEW)];
            this.pictureBox1.Image = imgView;
        }
        private void btnRightSide_Click(object sender, EventArgs e)
        {
            hiddModelDID.Text = "" + this.RIGHT_VIEW;
            label7.Text = "มุมมองด้านขวา";

            this.resetCarSideToEnable();
            this.btnRightSide.Enabled = false;

            //Switch Show RIGHT_VIEW
            Image imgView = this.imageList[Convert.ToInt32(RIGHT_VIEW)];
            this.pictureBox1.Image = imgView;
        }
        private void btnBackSide_Click(object sender, EventArgs e)
        {
            hiddModelDID.Text = "" + this.BACK_VIEW;
            label7.Text = "มุมมองด้านหลัง";

            this.resetCarSideToEnable();
            this.btnBackSide.Enabled = false;

            //Switch Show BACK_VIEW
            Image imgView = this.imageList[Convert.ToInt32(BACK_VIEW)];
            this.pictureBox1.Image = imgView;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string image_file = "";
            openFileDialog1.Title = "เลือกไฟล์รูปภาพ";
            openFileDialog1.FileName = "";
            openFileDialog1.Filter = "Image files (*.png) | *.png";

            if (openFileDialog1.ShowDialog() != DialogResult.Cancel)
            {
                image_file = openFileDialog1.FileName;
                txtImgPath.Text = image_file;
                this.pictureBox1.Image = Image.FromFile(image_file);
                this.pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;

                if(hiddModelDID.Text != "")
                    this.imageList[Convert.ToInt32(hiddModelDID.Text)] = this.pictureBox1.Image;
            }
        }

        private void resetCarSideToEnable()
        {
            this.btnLeftSide.Enabled = true;
            this.btnRightSide.Enabled = true;
            this.btnTopSide.Enabled = true;
            this.btnFrontSide.Enabled = true;
            this.btnBackSide.Enabled = true;
        }

        private void btnCreateNew_Click(object sender, EventArgs e)
        {
           bool isValid = this.validateBeforeSave();
           if (isValid)
           {
               bool isSuccess = true;

               ModelDAO dao = new ModelDAO();
               List<ModelHData> dataHOldList  = dao.getModelAll();
               ModelHData dataHOld = dataHOldList.Find(t => (t.MODEL_BRAND == txtBrand.Text) && 
                                                            (t.MODEL_SERIES == txtModel.Text) && 
                                                            (t.MODEL_YEAR== txtYear.Text));
               List<ModelDData> dataDOld = dataHOld.ModelDList.ToList();


               //For Insert New
               tb_m_model_h modelH = new tb_m_model_h();
               modelH.MODEL_H_ID = dataHOld.MODEL_H_ID;
               modelH.MODEL_BRAND = txtBrand.Text;
               modelH.MODEL_SERIES = txtModel.Text;
               modelH.MODEL_YEAR = txtYear.Text;
               modelH.MODEL_NAME = dataHOld.MODEL_NAME;

               int i = 0;
               foreach(int key in this.imageList.Keys)
               {
                   Image img = ImageUtilities.ResizeImage(this.imageList[key], pictureBox1.Width, pictureBox1.Height);

                    tb_m_model_d modelD = new tb_m_model_d();
                    modelD.MODEL_D_ID = key;
                    modelD.MODEL_H_ID = modelH.MODEL_H_ID;
                    modelD.MODEL_HEIGHT = Convert.ToDecimal(txtCarHeight.Text);
                    modelD.MODEL_WIDTH = Convert.ToDecimal(txtCarWidth.Text);
                    modelD.MODEL_SIDE = SIDE_VIEW[i];
                    modelD.MODELIMAGE = ImageUtilities.ImageToByteArray(img);
                    modelD.MODEL_RATIO = txtRatio.Text;

                   bool isHasDetail = true;
                   if (dataDOld != null)
                    {
                        ModelDData temp = dataDOld.Find(x => (x.MODEL_D_ID == i && x.MODEL_H_ID == modelH.MODEL_H_ID));
                        if (temp != null)
                        {
                            //For Delete Old
                            ModelDData delTmp = dataDOld.Find(x => x.MODEL_H_ID == modelH.MODEL_H_ID && x.MODEL_D_ID == key);
                            tb_m_model_d del = new tb_m_model_d();
                            del.MODEL_H_ID = delTmp.MODEL_H_ID;
                            del.MODEL_D_ID = delTmp.MODEL_D_ID;

                            isSuccess = dao.updateModelD(del, modelD);
                        }
                        else
                        {
                            isHasDetail = false;
                        }
                    }
                   else
                   {
                       isHasDetail = false;
                   }

                    if(!isHasDetail)
                    {
                        isSuccess = dao.saveModelD(modelD);
                    }
                    i++;
               }
               //"โครงรถ " + txtBrand.Text + " รุ่น " + txtModel.Text + " โฉมปี " + txtYear.Text;
               if(isSuccess)
                MessageBox.Show("บันทึกข้อมูลสำเร็จ");
           }

        }

        private bool validateBeforeSave()
        {

            bool isContainEmpty = (txtBrand.Text == "") || (txtModel.Text == "") || (txtYear.Text == "" 
                                  || (txtCarWidth.Text == "") || (txtCarHeight.Text == "") || (txtRatio.Text == ""));
            if (isContainEmpty)
            {
                MessageBox.Show("กรุณาเลือกรายการรถ รุ่น และโฉมปีที่ต้องการ");
                return false;
            }

            isContainEmpty = this.imageList.ContainsValue(null);
            if (isContainEmpty)
            {
                MessageBox.Show("กรุณากำหนด มุมมองของรถรุ่น " + txtBrand.Text + " " + txtModel.Text + " ปี " + txtYear.Text + " ให้ครบทั้ง 5 มุมมอง");
                return false;
            }

            return true;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            ModelAdd form = new ModelAdd();
            form.Show();
        }

        private void txtCarWidth_TextChanged(object sender, EventArgs e)
        {
            if (txtCarWidth.Text != "" && txtCarHeight.Text != null)
            {
                decimal widthInch = Convert.ToDecimal(txtCarWidth.Text);
                decimal heightInch = Convert.ToDecimal(txtCarHeight.Text);

                decimal widthPixel = Constants.DEFAULT_DPI * widthInch;
                decimal heightPixel = Constants.DEFAULT_DPI * heightInch;

                decimal ratio = (Convert.ToDecimal(pictureBox1.Width) * 100) / widthPixel;

                txtRatio.Text = "" + ratio;
            }

        }

        private void txtCarHeight_TextChanged(object sender, EventArgs e)
        {
            if (txtCarWidth.Text != "" && txtCarHeight.Text != null)
            {
                decimal widthInch = Convert.ToDecimal(txtCarWidth.Text);
                decimal heightInch = Convert.ToDecimal(txtCarHeight.Text);

                decimal widthPixel = Constants.DEFAULT_DPI * widthInch;
                decimal heightPixel = Constants.DEFAULT_DPI * heightInch;

                decimal ratio = (Convert.ToDecimal(pictureBox1.Width) * 100) / widthPixel;

                txtRatio.Text = "" + ratio;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.BilningCarBrandCombo();

            this.btnLeftSide.Enabled = false;
            this.hiddModelDID.Text = "" + this.LEFT_VIEW;
            this.clearImageList();
        }

    }
}
