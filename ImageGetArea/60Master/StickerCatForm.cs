﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WrapCar_Connection;
using WrapCar_DataModel;

namespace WrapCar._60Master
{
    public partial class StickerCatForm : Form
    {
        public StickerCatForm()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void StickerCatForm_Load(object sender, EventArgs e)
        {

        }

        private void search_Click(object sender, EventArgs e)
        {
            StickerCategoryDAO dao = new StickerCategoryDAO();
            List<StickerCategoryData> stickerCatDataList = new List<StickerCategoryData>();

            stickerCatDataList = dao.getStickerCategoryList();
            int row = 0;

            dataGridView1.ColumnCount = 3;
            dataGridView1.Columns[0].Name = "No.";
            dataGridView1.Columns[1].Name = "Category ID";
            dataGridView1.Columns[2].Name = "Category Name";

            if (CatName.Text != "")
                stickerCatDataList = stickerCatDataList.Where(cat => cat.STK_CATEGORY_NAME.Contains(CatName.Text)).ToList();

            dataGridView1.Rows.Clear();
            foreach (StickerCategoryData catData in stickerCatDataList)
            {
                string[] dataRow = new string[] { "" + (++row)
                                                , catData.STK_CATEGORY_ID.ToString()
                                                , catData.STK_CATEGORY_NAME };
                dataGridView1.Rows.Add(dataRow);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void add_Click(object sender, EventArgs e)
        {
            StickerCatAdd form = new StickerCatAdd();
            form.Show();
        }
    }
}
