﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WrapCar_Connection;
using WrapCar_DataModel;
using WrapCar_DataModel.Sticker;

namespace WrapCar._60Master
{
    public partial class StickerAdd : Form
    {
        public StickerAdd()
        {
            InitializeComponent();


        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFD = new OpenFileDialog();
            openFD.Filter = "Image Only. |*.jpg;*.jpeg; *.png";
            DialogResult dr = openFD.ShowDialog();
            pictureBox1.Image = Image.FromFile(openFD.FileName);
            label4.Text = openFD.FileName.ToString();
          // label4.Text = Path.GetFileNameWithoutExtension(openFD.FileName.ToString());

            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            double srcImageWidth = (double)pictureBox1.Image.Width;
            double srcImageHeight = (double)pictureBox1.Image.Height;
            WIDTHSTICK.Text = Convert.ToString(srcImageWidth);
            HEIGHT.Text = Convert.ToString(srcImageHeight);
        }
       

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if ((string.Empty == textBox1.Text)
               || (string.Empty == label4.Text) ||
                (string.Empty == comboBox1.Text))
            {
                MessageBox.Show("กรุณาระบุ ข้อมูลให้ครบถ้วน");
            }
            else
            {
                Name = Path.GetFileNameWithoutExtension(label4.Text);
                byte[] imgData = null;
                FileStream fstream = new FileStream(this.label4.Text, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fstream);
                imgData = br.ReadBytes((int)fstream.Length);
                tb_m_sticker_item data = new tb_m_sticker_item();
                 data.STK_NAME = textBox1.Text;
                 data.STK_PATH = Name;
                 data.STK_CATEGORY_ID = Convert.ToInt32(comboBox1.SelectedValue);
                 data.WIDTH = Convert.ToDecimal(WIDTHSTICK.Text);
                data.HEIGHT = Convert.ToDecimal(HEIGHT.Text);
                data.IMAGE = imgData;
                


                StickerItemDAO dao = new StickerItemDAO();
                bool ret = dao.saveStickerItem(data);
                

                if (ret)
                {
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ");
                }
                else
                {
                    MessageBox.Show("บันทึกข้อมูลไม่สำเร็จ กรุณาตรวจสอบใหม่");
                }
            }
        }

    

        private void StickerAdd_Load(object sender, EventArgs e)
        {

            StickerCategoryDAO dao = new StickerCategoryDAO();
            List<StickerCategoryData> stickerList = new List<StickerCategoryData>();
            stickerList = dao.getStickerCategoryList();

            comboBox1.DataSource = stickerList;
            comboBox1.DisplayMember = "STK_CATEGORY_NAME";
            comboBox1.ValueMember = "STK_CATEGORY_ID";
        }

        private void label3_Click_1(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}
