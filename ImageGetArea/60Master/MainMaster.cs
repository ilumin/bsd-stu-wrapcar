﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WrapCar._60Master
{
    public partial class MainMaster : Form
    {
        public MainMaster()
        {
            InitializeComponent();
        }

        private void stickerCat_Click(object sender, EventArgs e)
        {
            StickerCatForm form = new StickerCatForm();
            form.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            StickerItemForm form = new StickerItemForm();
            form.Show();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            ModelForm form = new ModelForm();
            form.Show();
        }
    }
}
