﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WrapCar_Connection;
using WrapCar_DataModel.Sticker;


namespace WrapCar._60Master
{
    public partial class StickerItemForm : Form
    {
        public StickerItemForm()
        {
            InitializeComponent();
        }

        private void StickerItemForm_Load(object sender, EventArgs e)
        {

        }

        private void search_Click(object sender, EventArgs e)
        {
            StickerItemDAO dao = new StickerItemDAO();
            List<StickerItemData> stickerList = new List<StickerItemData>();

            stickerList = dao.getStickerItemList();
            int row = 0;

            dataGridView1.ColumnCount = 5;
            dataGridView1.Columns[0].Name = "No.";
            dataGridView1.Columns[1].Name = "Item ID";
            dataGridView1.Columns[2].Name = "Sticker Name";
            dataGridView1.Columns[3].Name = "Width";
            dataGridView1.Columns[2].Name = "Height";

            if (txtname.Text != "")
                stickerList = stickerList.Where(cus => cus.STK_NAME.Contains(txtname.Text)).ToList();

            dataGridView1.Rows.Clear();
            foreach (StickerItemData SitemData in stickerList)
            {
                string[] dataRow = new string[] { "" + (++row)
                                                , SitemData.STK_ITEM_ID.ToString()
                                                , SitemData.STK_NAME 
                                                , SitemData.WIDTH.ToString()
                                                , SitemData.HEIGHT.ToString()
                };
                dataGridView1.Rows.Add(dataRow);
            }
        }

        private void add_Click(object sender, EventArgs e)
        {
            StickerAdd form = new StickerAdd();
            form.Show();
        }
    }
}
