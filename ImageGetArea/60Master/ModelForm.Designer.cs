﻿namespace WrapCar._60Master
{
    partial class ModelForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModelForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmbCarBrand = new System.Windows.Forms.ComboBox();
            this.tvModelSeries = new System.Windows.Forms.TreeView();
            this.groupButtonCarSide = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnBackSide = new System.Windows.Forms.Button();
            this.btnRightSide = new System.Windows.Forms.Button();
            this.btnLeftSide = new System.Windows.Forms.Button();
            this.btnFrontSide = new System.Windows.Forms.Button();
            this.btnTopSide = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.hiddModelDID = new System.Windows.Forms.Label();
            this.txtYear = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtImgPath = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.txtBrand = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtModel = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtCarHeight = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtCarWidth = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtRatio = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnSearch = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupButtonCarSide.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnSearch);
            this.groupBox1.Controls.Add(this.cmbCarBrand);
            this.groupBox1.Controls.Add(this.tvModelSeries);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 30);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(260, 688);
            this.groupBox1.TabIndex = 62;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "เลือกรายการรถ :";
            // 
            // cmbCarBrand
            // 
            this.cmbCarBrand.DropDownHeight = 200;
            this.cmbCarBrand.FormattingEnabled = true;
            this.cmbCarBrand.IntegralHeight = false;
            this.cmbCarBrand.ItemHeight = 13;
            this.cmbCarBrand.Location = new System.Drawing.Point(15, 76);
            this.cmbCarBrand.Name = "cmbCarBrand";
            this.cmbCarBrand.Size = new System.Drawing.Size(143, 21);
            this.cmbCarBrand.TabIndex = 63;
            this.cmbCarBrand.SelectedIndexChanged += new System.EventHandler(this.cmbCarBrand_SelectedIndexChanged);
            // 
            // tvModelSeries
            // 
            this.tvModelSeries.Location = new System.Drawing.Point(15, 107);
            this.tvModelSeries.Name = "tvModelSeries";
            this.tvModelSeries.Size = new System.Drawing.Size(227, 568);
            this.tvModelSeries.TabIndex = 62;
            this.tvModelSeries.AfterExpand += new System.Windows.Forms.TreeViewEventHandler(this.tvModelSeries_AfterExpand);
            this.tvModelSeries.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvModelSeries_AfterSelect);
            // 
            // groupButtonCarSide
            // 
            this.groupButtonCarSide.Controls.Add(this.label5);
            this.groupButtonCarSide.Controls.Add(this.label4);
            this.groupButtonCarSide.Controls.Add(this.label3);
            this.groupButtonCarSide.Controls.Add(this.label2);
            this.groupButtonCarSide.Controls.Add(this.label1);
            this.groupButtonCarSide.Controls.Add(this.btnBackSide);
            this.groupButtonCarSide.Controls.Add(this.btnRightSide);
            this.groupButtonCarSide.Controls.Add(this.btnLeftSide);
            this.groupButtonCarSide.Controls.Add(this.btnFrontSide);
            this.groupButtonCarSide.Controls.Add(this.btnTopSide);
            this.groupButtonCarSide.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupButtonCarSide.Location = new System.Drawing.Point(280, 30);
            this.groupButtonCarSide.Name = "groupButtonCarSide";
            this.groupButtonCarSide.Size = new System.Drawing.Size(142, 688);
            this.groupButtonCarSide.TabIndex = 64;
            this.groupButtonCarSide.TabStop = false;
            this.groupButtonCarSide.Text = "เลือกมุมมอง :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(15, 258);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 25);
            this.label5.TabIndex = 50;
            this.label5.Text = "5.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(15, 202);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 25);
            this.label4.TabIndex = 49;
            this.label4.Text = "4.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(15, 143);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 25);
            this.label3.TabIndex = 48;
            this.label3.Text = "3.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(15, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 25);
            this.label2.TabIndex = 47;
            this.label2.Text = "2.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 25);
            this.label1.TabIndex = 46;
            this.label1.Text = "1.";
            // 
            // btnBackSide
            // 
            this.btnBackSide.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnBackSide.BackgroundImage")));
            this.btnBackSide.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBackSide.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBackSide.Location = new System.Drawing.Point(53, 252);
            this.btnBackSide.Name = "btnBackSide";
            this.btnBackSide.Size = new System.Drawing.Size(70, 36);
            this.btnBackSide.TabIndex = 45;
            this.btnBackSide.UseVisualStyleBackColor = true;
            this.btnBackSide.Click += new System.EventHandler(this.btnBackSide_Click);
            // 
            // btnRightSide
            // 
            this.btnRightSide.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRightSide.BackgroundImage")));
            this.btnRightSide.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRightSide.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRightSide.Location = new System.Drawing.Point(53, 196);
            this.btnRightSide.Name = "btnRightSide";
            this.btnRightSide.Size = new System.Drawing.Size(70, 36);
            this.btnRightSide.TabIndex = 44;
            this.btnRightSide.UseVisualStyleBackColor = true;
            this.btnRightSide.Click += new System.EventHandler(this.btnRightSide_Click);
            // 
            // btnLeftSide
            // 
            this.btnLeftSide.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLeftSide.BackgroundImage")));
            this.btnLeftSide.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnLeftSide.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLeftSide.Location = new System.Drawing.Point(53, 30);
            this.btnLeftSide.Name = "btnLeftSide";
            this.btnLeftSide.Size = new System.Drawing.Size(70, 36);
            this.btnLeftSide.TabIndex = 43;
            this.btnLeftSide.UseVisualStyleBackColor = true;
            this.btnLeftSide.Click += new System.EventHandler(this.btnLeftSide_Click);
            // 
            // btnFrontSide
            // 
            this.btnFrontSide.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFrontSide.BackgroundImage")));
            this.btnFrontSide.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFrontSide.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFrontSide.Location = new System.Drawing.Point(53, 137);
            this.btnFrontSide.Name = "btnFrontSide";
            this.btnFrontSide.Size = new System.Drawing.Size(70, 36);
            this.btnFrontSide.TabIndex = 42;
            this.btnFrontSide.UseVisualStyleBackColor = true;
            this.btnFrontSide.Click += new System.EventHandler(this.btnFrontSide_Click);
            // 
            // btnTopSide
            // 
            this.btnTopSide.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnTopSide.BackgroundImage")));
            this.btnTopSide.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTopSide.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTopSide.Location = new System.Drawing.Point(53, 81);
            this.btnTopSide.Name = "btnTopSide";
            this.btnTopSide.Size = new System.Drawing.Size(70, 36);
            this.btnTopSide.TabIndex = 41;
            this.btnTopSide.UseVisualStyleBackColor = true;
            this.btnTopSide.Click += new System.EventHandler(this.btnTopSide_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.btnNew);
            this.groupBox2.Controls.Add(this.btnSave);
            this.groupBox2.Controls.Add(this.pictureBox1);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(432, 30);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(820, 688);
            this.groupBox2.TabIndex = 94;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "รายละเอียดโครงสร้างของรถ";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.hiddModelDID);
            this.groupBox4.Controls.Add(this.txtYear);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.txtImgPath);
            this.groupBox4.Controls.Add(this.button1);
            this.groupBox4.Controls.Add(this.txtBrand);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.txtModel);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Location = new System.Drawing.Point(53, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(554, 113);
            this.groupBox4.TabIndex = 122;
            this.groupBox4.TabStop = false;
            // 
            // hiddModelDID
            // 
            this.hiddModelDID.Location = new System.Drawing.Point(297, 17);
            this.hiddModelDID.Name = "hiddModelDID";
            this.hiddModelDID.Size = new System.Drawing.Size(73, 23);
            this.hiddModelDID.TabIndex = 120;
            this.hiddModelDID.Text = "hiddModelDID";
            this.hiddModelDID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.hiddModelDID.Visible = false;
            // 
            // txtYear
            // 
            this.txtYear.BackColor = System.Drawing.Color.White;
            this.txtYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtYear.Location = new System.Drawing.Point(300, 44);
            this.txtYear.Name = "txtYear";
            this.txtYear.ReadOnly = true;
            this.txtYear.Size = new System.Drawing.Size(61, 22);
            this.txtYear.TabIndex = 119;
            this.txtYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(217, 42);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(78, 23);
            this.label15.TabIndex = 118;
            this.label15.Text = "โฉมปีที่ผลิต :";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(75, 95);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(276, 15);
            this.label9.TabIndex = 117;
            this.label9.Text = "(รองรับไฟล์ .png ขนาด 750 x 450 เท่านั้น) ";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(1, 70);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 23);
            this.label8.TabIndex = 116;
            this.label8.Text = "รูปภาพ :";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtImgPath
            // 
            this.txtImgPath.BackColor = System.Drawing.Color.White;
            this.txtImgPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtImgPath.Location = new System.Drawing.Point(76, 71);
            this.txtImgPath.Name = "txtImgPath";
            this.txtImgPath.ReadOnly = true;
            this.txtImgPath.Size = new System.Drawing.Size(354, 22);
            this.txtImgPath.TabIndex = 115;
            this.txtImgPath.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // button1
            // 
            this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(436, 64);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(44, 32);
            this.button1.TabIndex = 114;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtBrand
            // 
            this.txtBrand.BackColor = System.Drawing.Color.White;
            this.txtBrand.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBrand.Location = new System.Drawing.Point(76, 18);
            this.txtBrand.Name = "txtBrand";
            this.txtBrand.ReadOnly = true;
            this.txtBrand.Size = new System.Drawing.Size(128, 22);
            this.txtBrand.TabIndex = 113;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(4, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(66, 23);
            this.label11.TabIndex = 112;
            this.label11.Text = "ยี่ห้อรถ :";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtModel
            // 
            this.txtModel.BackColor = System.Drawing.Color.White;
            this.txtModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtModel.Location = new System.Drawing.Point(76, 45);
            this.txtModel.Name = "txtModel";
            this.txtModel.ReadOnly = true;
            this.txtModel.Size = new System.Drawing.Size(128, 22);
            this.txtModel.TabIndex = 111;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(7, 42);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 23);
            this.label6.TabIndex = 110;
            this.label6.Text = "รุ่นรถ :";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.txtCarHeight);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.txtCarWidth);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.txtRatio);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Location = new System.Drawing.Point(53, 124);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(748, 95);
            this.groupBox3.TabIndex = 121;
            this.groupBox3.TabStop = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(390, 20);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(31, 13);
            this.label13.TabIndex = 130;
            this.label13.Text = "(นิ้ว)";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(390, 47);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(31, 13);
            this.label14.TabIndex = 129;
            this.label14.Text = "(นิ้ว)";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(256, 47);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(59, 13);
            this.label16.TabIndex = 128;
            this.label16.Text = "ด้านยาว :";
            // 
            // txtCarHeight
            // 
            this.txtCarHeight.Location = new System.Drawing.Point(320, 43);
            this.txtCarHeight.Name = "txtCarHeight";
            this.txtCarHeight.Size = new System.Drawing.Size(64, 20);
            this.txtCarHeight.TabIndex = 127;
            this.txtCarHeight.Text = "57.9";
            this.txtCarHeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCarHeight.TextChanged += new System.EventHandler(this.txtCarHeight_TextChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(250, 20);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(65, 13);
            this.label17.TabIndex = 126;
            this.label17.Text = "ด้านกว้าง :";
            // 
            // txtCarWidth
            // 
            this.txtCarWidth.Location = new System.Drawing.Point(320, 17);
            this.txtCarWidth.Name = "txtCarWidth";
            this.txtCarWidth.Size = new System.Drawing.Size(64, 20);
            this.txtCarWidth.TabIndex = 125;
            this.txtCarWidth.Text = "189.2";
            this.txtCarWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCarWidth.TextChanged += new System.EventHandler(this.txtCarWidth_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(389, 72);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 13);
            this.label10.TabIndex = 124;
            this.label10.Text = "(นิ้ว / Pixel)";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(210, 70);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(107, 13);
            this.label12.TabIndex = 123;
            this.label12.Text = "อัตราส่วนเทียบเคียง :";
            // 
            // txtRatio
            // 
            this.txtRatio.Enabled = false;
            this.txtRatio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRatio.Location = new System.Drawing.Point(320, 69);
            this.txtRatio.Name = "txtRatio";
            this.txtRatio.ReadOnly = true;
            this.txtRatio.Size = new System.Drawing.Size(64, 20);
            this.txtRatio.TabIndex = 122;
            this.txtRatio.Text = "0.2522";
            this.txtRatio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Gainsboro;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(10, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(191, 67);
            this.label7.TabIndex = 121;
            this.label7.Text = "มุมมองด้านซ้าย";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnNew
            // 
            this.btnNew.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNew.BackgroundImage")));
            this.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNew.Location = new System.Drawing.Point(712, 26);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(93, 95);
            this.btnNew.TabIndex = 111;
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSave.BackgroundImage")));
            this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(613, 27);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(94, 94);
            this.btnSave.TabIndex = 106;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnCreateNew_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(53, 225);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(750, 450);
            this.pictureBox1.TabIndex = 94;
            this.pictureBox1.TabStop = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btnSearch
            // 
            this.btnSearch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSearch.BackgroundImage")));
            this.btnSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Location = new System.Drawing.Point(164, 19);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(78, 79);
            this.btnSearch.TabIndex = 64;
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // ModelForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 730);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupButtonCarSide);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "ModelForm";
            this.Text = "ModelHForm";
            this.Load += new System.EventHandler(this.ModelHForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupButtonCarSide.ResumeLayout(false);
            this.groupButtonCarSide.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cmbCarBrand;
        private System.Windows.Forms.TreeView tvModelSeries;
        private System.Windows.Forms.GroupBox groupButtonCarSide;
        private System.Windows.Forms.Button btnBackSide;
        private System.Windows.Forms.Button btnRightSide;
        private System.Windows.Forms.Button btnLeftSide;
        private System.Windows.Forms.Button btnFrontSide;
        private System.Windows.Forms.Button btnTopSide;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtCarHeight;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtCarWidth;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtRatio;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label hiddModelDID;
        private System.Windows.Forms.TextBox txtYear;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtImgPath;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtBrand;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtModel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnSearch;
    }
}