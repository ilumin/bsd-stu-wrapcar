﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrapCar
{
    public class Constants
    {
        public static readonly string BASE_PATH = AppDomain.CurrentDomain.BaseDirectory;
        public static string orderID = "";

        public static readonly string IMG_CAR_DIR = "\\01 Car";
        public static readonly string IMG_STICKER_DIR = "\\02 Sticker\\images";
        public static readonly string LINE_STICKER_DIR = "\\02 Sticker\\lines";
        public static readonly string DRAFT_DIR = "\\03 Make_To_Order";
        public static readonly string CAR_CUTOFF_PREFIX = "CAR_CutOff_";
        public static readonly string STK_PRINT_CUTOFF_PREFIX = "STK_PRINT_";
        public static readonly string STK_DICUT_CUTOFF_PREFIX = "STK_DICUT_";

        public static readonly int MIN_PIECE_WIDTH = 20;    // Minimum width of jigsaw piece in pixels.
        public static readonly int MIN_PIECE_HEIGHT = 20;   // Minimum height of jigsaw piece in pixels.

        public static readonly int NUM_COLUMNS = 2;
        public static readonly int NUM_ROWS = 1;
        public static readonly int DEFAULT_DPI = 72;

        public static readonly int STK_IMAGE_TYPE = 0; // Image Type
        public static readonly int STK_LINE_TYPE = 1;  // Line Type

        //public static readonly int SNAP_TOLERANCE = 15;
        //public static readonly byte GHOST_PICTURE_ALPHA = 127;

        public static readonly int PIECE_OUTLINE_WIDTH = 4;
        public static readonly bool DRAW_PIECE_OUTLINE = true;

        public static readonly int DROP_SHADOW_DEPTH = 3;
        public static readonly Color DROP_SHADOW_COLOR = Color.FromArgb(50, 50, 50);

        public static readonly int LAYOUT_MODEL_WIDTH = 750;
        public static readonly int LAYOUT_MODEL_HEIGHT = 450;

    }
}
