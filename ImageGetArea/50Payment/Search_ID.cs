﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WrapCar_Connection;
using WrapCar_DataModel;
using PrintControl;

namespace WrapCar._50Payment
{
    public partial class Search_ID : Form
    {
        PaymentDao paymentDAO = new PaymentDao();
        OrderData orderdata = new OrderData();
        PaymentData paymentdata;

        public Search_ID()
        {
            InitializeComponent();
        }

        public Search_ID(string ID)
        {
            InitializeComponent();
            lbl_ord_search.Visible = false;
            tb_ord_search.Text = ID;
            tb_ord_search.Visible = false;
            button1.Visible = false;
            searchOrder();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            searchOrder();
        }

        private void searchOrder ()
        {
             //var paymentDAO = new PaymentDao();
            //var orderdata = new OrderData();
            paymentdata = new PaymentData();
            orderdata = paymentDAO.getOrderData(tb_ord_search.Text);

            if (orderdata != null)
            {
                mainpanel.Visible = true;
                label12.Text = orderdata.ORDER_H_ID.ToString();
                label13.Text = orderdata.TITLE.ToString() + " " + orderdata.CUS_FNAME.ToString() + " " + orderdata.CUS_LNAME.ToString();
                label14.Text = orderdata.CAR_BRAND.ToString() + " " + orderdata.CAR_MODEL.ToString() + " ปี " + orderdata.CAR_YEAR.ToString() + " สี " + orderdata.CAR_COLOR.ToString() + " ทะเบียน " + orderdata.CAR_LICENSE_NO.ToString();
                label15.Text = orderdata.ORDER_DATE.ToString();
                label16.Text = orderdata.TOTAL_PRICE.ToString();
                label23.Text = (orderdata.TOTAL_PRICE / 2).ToString();
                label24.Text = (orderdata.TOTAL_PRICE / 2).ToString();

                if (orderdata.PART_PAYMENT != null)
                {
                    
                    label27.Text = "ชำระเรียบร้อย";
                    btPay1.Visible = false;
                    btPay2.Enabled = true;
                    btPrint1.Visible = true;
                }
                else
                {
                   
                    label27.Text = "ค้างชำระ";
                    btPay1.Visible = true;
                    btPay2.Enabled = false;
                    btPrint1.Visible = false;
                }

                if (orderdata.UNPAID_PAYMENT != null)
                {
                    
                    label28.Text = "ชำระเรียบร้อย";
                    btPay2.Visible = false;
                    btPrint2.Visible = true;
                }
                else
                {                    
                    label28.Text = "ค้างชำระ";
                    btPay2.Visible = true;
                    btPrint2.Visible = false;
                }
            }
            else
            {
                MessageBox.Show("ไม่พบข้อมูลรายการสั่งซื้อ", "Not Found", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

        }

        private void btPay1_Click(object sender, EventArgs e)
        {
            tb_r_payment data = new tb_r_payment();                      
            data.ORDER_H_ID = orderdata.ORDER_H_ID;
            data.PAY_TYPE = "Part";
            data.PAY_AMOUNT = Convert.ToDecimal(orderdata.TOTAL_PRICE/2);
            data.PAY_DATE = DateTime.Now;
            data.EMP_ID = 1;

            paymentdata = paymentDAO.savepaymentTran(data, "Part");
            if (paymentdata != null)
            {
                MessageBox.Show("บันทึกข้อมูลเรียบร้อย");
                tb_ord_search.Text = paymentdata.ORDER_H_ID;
                searchOrder();
                
            }
            else
            {
                MessageBox.Show("บันทึกข้อมูลผิดพลาด กรุณาติดต่อผู้ดูแลระบบ");
            }

            //bool ret = paymentDAO.savePayment(data);
            //if (ret)
            //{
            //    paymentdata = paymentDAO.getPaymentData(orderdata.ORDER_H_ID, "Part");
            //    if (paymentdata != null)
            //    {

            //    }
            //    MessageBox.Show("บันทึกข้อมูลสำเร็จ");
            //}
            
            //{
            //    MessageBox.Show("บันทึกข้อมูลไม่สำเร็จ กรุณาตรวจสอบใหม่");
            //}



        }

        private void btPay2_Click(object sender, EventArgs e)
        {
            tb_r_payment data = new tb_r_payment();
            data.ORDER_H_ID = orderdata.ORDER_H_ID;
            data.PAY_TYPE = "Unpaid";
            data.PAY_AMOUNT = Convert.ToDecimal(orderdata.TOTAL_PRICE / 2);
            data.PAY_DATE = DateTime.Now;
            data.EMP_ID = 1;

            paymentdata = paymentDAO.savepaymentTran(data, "Unpaid");
            if (paymentdata != null)
            {
                MessageBox.Show("บันทึกข้อมูลเรียบร้อย");
                tb_ord_search.Text = paymentdata.ORDER_H_ID;
                searchOrder();

            }
            else
            {
                MessageBox.Show("บันทึกข้อมูลผิดพลาด กรุณาติดต่อผู้ดูแลระบบ");
            }
        }

        //private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
        //    {
        //        e.Handled = true;
        //    }
        //}

        public void showPrintPreview()
        {
            try
            {
                pnlOutput.Visible = true;
                    Cursor = Cursors.WaitCursor;
                    PrintDocumentForm form = new PrintDocumentForm(pnlOutput);
                    Cursor = Cursors.Default;
                    form.Show();
                    form.Activate();
                    pnlOutput.Visible = false;

            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return;
            }

        }

        private void Recipt_addData(PaymentData pay)
        {
            label37.Text = pay.PAYMENT_ID.ToString();
            label38.Text = pay.PAY_DATE.ToString();

            label39.Text = orderdata.ORDER_H_ID;
            label40.Text = orderdata.TITLE + " " + orderdata.CUS_FNAME + " " + orderdata.CUS_LNAME;
            label41.Text = orderdata.CAR_BRAND + " " + orderdata.CAR_MODEL + " ปี " + orderdata.CAR_YEAR + " สี " + orderdata.CAR_COLOR + " ทะเบียน " + orderdata.CAR_LICENSE_NO;

            if (pay.PAY_TYPE == "Part")
            {
                label42.Text = "เงินมัดจำ";
            }
            else
            {
                label42.Text = "เงินส่วนที่เหลือ";
            }
            label43.Text = pay.PAY_AMOUNT.ToString();

            label44.Text = pay.EMP_TITLE + " " + pay.EMP_FNAME + " " + pay.EMP_LNAME;
            
        }

        private void btPrint1_Click(object sender, EventArgs e)
        {
            Recipt_addData(paymentDAO.getPaymentData(orderdata.ORDER_H_ID,"Part"));            
            showPrintPreview();            
        }

        private void btPrint2_Click(object sender, EventArgs e)
        {
            Recipt_addData(paymentDAO.getPaymentData(orderdata.ORDER_H_ID, "Unpaid"));
            showPrintPreview();
        }

       

        
    }


}
