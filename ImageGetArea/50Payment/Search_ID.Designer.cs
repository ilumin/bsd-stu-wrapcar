﻿namespace WrapCar._50Payment
{
    partial class Search_ID
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_ord_search = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.tb_ord_search = new System.Windows.Forms.TextBox();
            this.mainpanel = new System.Windows.Forms.Panel();
            this.btPrint2 = new System.Windows.Forms.Button();
            this.btPrint1 = new System.Windows.Forms.Button();
            this.btPay2 = new System.Windows.Forms.Button();
            this.btPay1 = new System.Windows.Forms.Button();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlOutput = new System.Windows.Forms.Panel();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.mainpanel.SuspendLayout();
            this.pnlOutput.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_ord_search
            // 
            this.lbl_ord_search.AutoSize = true;
            this.lbl_ord_search.Location = new System.Drawing.Point(279, 114);
            this.lbl_ord_search.Name = "lbl_ord_search";
            this.lbl_ord_search.Size = new System.Drawing.Size(86, 13);
            this.lbl_ord_search.TabIndex = 0;
            this.lbl_ord_search.Text = "รหัสรายการสั่งซื้อ";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(546, 109);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "ค้นหา";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tb_ord_search
            // 
            this.tb_ord_search.Location = new System.Drawing.Point(371, 111);
            this.tb_ord_search.Name = "tb_ord_search";
            this.tb_ord_search.Size = new System.Drawing.Size(157, 20);
            this.tb_ord_search.TabIndex = 2;
            // 
            // mainpanel
            // 
            this.mainpanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mainpanel.Controls.Add(this.btPrint2);
            this.mainpanel.Controls.Add(this.btPrint1);
            this.mainpanel.Controls.Add(this.btPay2);
            this.mainpanel.Controls.Add(this.btPay1);
            this.mainpanel.Controls.Add(this.label28);
            this.mainpanel.Controls.Add(this.label27);
            this.mainpanel.Controls.Add(this.label26);
            this.mainpanel.Controls.Add(this.label25);
            this.mainpanel.Controls.Add(this.label24);
            this.mainpanel.Controls.Add(this.label23);
            this.mainpanel.Controls.Add(this.label22);
            this.mainpanel.Controls.Add(this.label21);
            this.mainpanel.Controls.Add(this.label20);
            this.mainpanel.Controls.Add(this.label19);
            this.mainpanel.Controls.Add(this.label18);
            this.mainpanel.Controls.Add(this.label17);
            this.mainpanel.Controls.Add(this.label16);
            this.mainpanel.Controls.Add(this.label15);
            this.mainpanel.Controls.Add(this.label14);
            this.mainpanel.Controls.Add(this.label13);
            this.mainpanel.Controls.Add(this.label12);
            this.mainpanel.Controls.Add(this.label11);
            this.mainpanel.Controls.Add(this.label10);
            this.mainpanel.Controls.Add(this.label9);
            this.mainpanel.Controls.Add(this.label8);
            this.mainpanel.Controls.Add(this.label7);
            this.mainpanel.Controls.Add(this.label6);
            this.mainpanel.Controls.Add(this.label5);
            this.mainpanel.Controls.Add(this.label4);
            this.mainpanel.Controls.Add(this.label3);
            this.mainpanel.Controls.Add(this.label2);
            this.mainpanel.Location = new System.Drawing.Point(173, 180);
            this.mainpanel.Name = "mainpanel";
            this.mainpanel.Size = new System.Drawing.Size(572, 329);
            this.mainpanel.TabIndex = 3;
            this.mainpanel.Visible = false;
            // 
            // btPrint2
            // 
            this.btPrint2.Location = new System.Drawing.Point(460, 278);
            this.btPrint2.Name = "btPrint2";
            this.btPrint2.Size = new System.Drawing.Size(75, 23);
            this.btPrint2.TabIndex = 30;
            this.btPrint2.Text = "พิมพ์ใชเสร็จ";
            this.btPrint2.UseVisualStyleBackColor = true;
            this.btPrint2.Click += new System.EventHandler(this.btPrint2_Click);
            // 
            // btPrint1
            // 
            this.btPrint1.Location = new System.Drawing.Point(460, 244);
            this.btPrint1.Name = "btPrint1";
            this.btPrint1.Size = new System.Drawing.Size(75, 23);
            this.btPrint1.TabIndex = 29;
            this.btPrint1.Text = "พิมพ์ใชเสร็จ";
            this.btPrint1.UseVisualStyleBackColor = true;
            this.btPrint1.Click += new System.EventHandler(this.btPrint1_Click);
            // 
            // btPay2
            // 
            this.btPay2.Location = new System.Drawing.Point(361, 278);
            this.btPay2.Name = "btPay2";
            this.btPay2.Size = new System.Drawing.Size(75, 23);
            this.btPay2.TabIndex = 28;
            this.btPay2.Text = "ชำระเงิน";
            this.btPay2.UseVisualStyleBackColor = true;
            this.btPay2.Click += new System.EventHandler(this.btPay2_Click);
            // 
            // btPay1
            // 
            this.btPay1.Location = new System.Drawing.Point(361, 244);
            this.btPay1.Name = "btPay1";
            this.btPay1.Size = new System.Drawing.Size(75, 23);
            this.btPay1.TabIndex = 27;
            this.btPay1.Text = "ชำระเงิน";
            this.btPay1.UseVisualStyleBackColor = true;
            this.btPay1.Click += new System.EventHandler(this.btPay1_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(285, 283);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(41, 13);
            this.label28.TabIndex = 26;
            this.label28.Text = "label28";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(285, 248);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(41, 13);
            this.label27.TabIndex = 25;
            this.label27.Text = "label27";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(236, 283);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(26, 13);
            this.label26.TabIndex = 24;
            this.label26.Text = "บาท";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(236, 249);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(26, 13);
            this.label25.TabIndex = 23;
            this.label25.Text = "บาท";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(185, 283);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(41, 13);
            this.label24.TabIndex = 22;
            this.label24.Text = "label24";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(185, 249);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(41, 13);
            this.label23.TabIndex = 21;
            this.label23.Text = "label23";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(236, 175);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(26, 13);
            this.label22.TabIndex = 20;
            this.label22.Text = "บาท";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(152, 283);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(10, 13);
            this.label21.TabIndex = 19;
            this.label21.Text = ":";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(152, 249);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(10, 13);
            this.label20.TabIndex = 18;
            this.label20.Text = ":";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(48, 283);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(76, 13);
            this.label19.TabIndex = 17;
            this.label19.Text = "เงินส่วนที่เหลือ";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(46, 249);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(50, 13);
            this.label18.TabIndex = 16;
            this.label18.Text = "เงินมัดจำ";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(29, 214);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(97, 13);
            this.label17.TabIndex = 15;
            this.label17.Text = "สถานะการชำระเงิน";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(185, 175);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(41, 13);
            this.label16.TabIndex = 14;
            this.label16.Text = "label16";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(185, 135);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 13);
            this.label15.TabIndex = 13;
            this.label15.Text = "label15";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(185, 98);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 13);
            this.label14.TabIndex = 12;
            this.label14.Text = "label14";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(185, 58);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 13);
            this.label13.TabIndex = 11;
            this.label13.Text = "label13";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(185, 22);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 13);
            this.label12.TabIndex = 10;
            this.label12.Text = "label12";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(152, 175);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(10, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = ":";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(152, 135);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(10, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = ":";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(152, 98);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(10, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = ":";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(152, 58);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(10, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = ":";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(152, 23);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(10, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = ":";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(29, 175);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "ราคารวม";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(26, 135);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "วันที่สั้งซื้อ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 98);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "รถยนต์";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "ชื่อ - นามสกุล";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "รหัวรายการสั่งซื้อ";
            // 
            // pnlOutput
            // 
            this.pnlOutput.BackColor = System.Drawing.Color.White;
            this.pnlOutput.Controls.Add(this.label44);
            this.pnlOutput.Controls.Add(this.label43);
            this.pnlOutput.Controls.Add(this.label42);
            this.pnlOutput.Controls.Add(this.label41);
            this.pnlOutput.Controls.Add(this.label40);
            this.pnlOutput.Controls.Add(this.label39);
            this.pnlOutput.Controls.Add(this.label38);
            this.pnlOutput.Controls.Add(this.label37);
            this.pnlOutput.Controls.Add(this.label36);
            this.pnlOutput.Controls.Add(this.label35);
            this.pnlOutput.Controls.Add(this.label34);
            this.pnlOutput.Controls.Add(this.label33);
            this.pnlOutput.Controls.Add(this.label32);
            this.pnlOutput.Controls.Add(this.label31);
            this.pnlOutput.Controls.Add(this.label30);
            this.pnlOutput.Controls.Add(this.label29);
            this.pnlOutput.Controls.Add(this.label1);
            this.pnlOutput.Location = new System.Drawing.Point(715, 149);
            this.pnlOutput.Name = "pnlOutput";
            this.pnlOutput.Size = new System.Drawing.Size(382, 376);
            this.pnlOutput.TabIndex = 29;
            this.pnlOutput.Visible = false;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(156, 312);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(54, 18);
            this.label44.TabIndex = 16;
            this.label44.Text = "label44";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(156, 256);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(54, 18);
            this.label43.TabIndex = 15;
            this.label43.Text = "label43";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(156, 225);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(54, 18);
            this.label42.TabIndex = 14;
            this.label42.Text = "label42";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(159, 175);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(54, 18);
            this.label41.TabIndex = 13;
            this.label41.Text = "label41";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(159, 146);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(54, 18);
            this.label40.TabIndex = 12;
            this.label40.Text = "label40";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(159, 115);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(54, 18);
            this.label39.TabIndex = 11;
            this.label39.Text = "label39";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(281, 66);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(54, 18);
            this.label38.TabIndex = 10;
            this.label38.Text = "label38";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(282, 43);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(54, 18);
            this.label37.TabIndex = 9;
            this.label37.Text = "label37";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(82, 312);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(59, 18);
            this.label36.TabIndex = 8;
            this.label36.Text = "ผู้รับเงิน :";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(77, 256);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(73, 18);
            this.label35.TabIndex = 7;
            this.label35.Text = "จำนวนเงิน :";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(40, 225);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(110, 18);
            this.label34.TabIndex = 6;
            this.label34.Text = "ประเภทการชำระ :";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(97, 175);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(56, 18);
            this.label33.TabIndex = 5;
            this.label33.Text = "รถยนต์ :";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(61, 146);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(92, 18);
            this.label32.TabIndex = 4;
            this.label32.Text = "ชื่อ - นามสกุล :";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(40, 115);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(113, 18);
            this.label31.TabIndex = 3;
            this.label31.Text = "รหัสรายการสั่งซื้อ :";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(234, 66);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(41, 18);
            this.label30.TabIndex = 2;
            this.label30.Text = "วันที่ :";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(231, 43);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(44, 18);
            this.label29.TabIndex = 1;
            this.label29.Text = "เลขที่ :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(39, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(156, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "ใบเสร็จรับเงิน";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(37, 26);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(274, 46);
            this.label45.TabIndex = 30;
            this.label45.Text = "ระบบรับชำระเงิน";
            // 
            // Search_ID
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(907, 577);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.mainpanel);
            this.Controls.Add(this.pnlOutput);
            this.Controls.Add(this.tb_ord_search);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lbl_ord_search);
            this.Name = "Search_ID";
            this.Text = "Serach_ID";
            this.mainpanel.ResumeLayout(false);
            this.mainpanel.PerformLayout();
            this.pnlOutput.ResumeLayout(false);
            this.pnlOutput.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_ord_search;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox tb_ord_search;
        private System.Windows.Forms.Panel mainpanel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btPay2;
        private System.Windows.Forms.Button btPay1;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Panel pnlOutput;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Button btPrint2;
        private System.Windows.Forms.Button btPrint1;
        private System.Windows.Forms.Label label45;
    }
}