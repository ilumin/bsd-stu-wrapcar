﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrapCar._20Desing
{
    public class Block
    {
        public Root Fit { get; set; }

        public double x { get; set; }

        public double y { get; set; }

        public double w { get; set; }

        public double h { get; set; }
    }


    public class Root
    {
        public Root()
        {

        }

        public Root(double x, double y, double w, double h)
        {
            this.x = x;
            this.y = y;
            this.w = w;
            this.h = h;
        }

        public Bitmap sticker { get; set; }

        public Root down { get; set; }

        public Root right { get; set; }

        public bool used { get; set; }

        public double x { get; set; }

        public double y { get; set; }

        public double w { get; set; }

        public double h { get; set; }
    }

    public class Packer
    {
        public Root root { get; set; }

        public Packer(double w, double h)
        {
           this.init(w, h);
        }

        public void init(double w, double h)
        {
             this.root = new Root(0, 0, w, h);
        }

        public void Fit(List<Block> blocks)
        {
            // This is the  area which needs change
            Block block;
            Root node = this.root;

            for (int n = 0; n < blocks.Count; n++)
            {
                block = blocks[n];
                if (node == this.findNode(this.root, block.w, block.h))
                    block.Fit = this.SplitNode(node, block.w, block.h);
                else
                    block.Fit = this.SplitNode(node, block.w, block.h);
            }
        }

        public Root findNode(Root root, double w, double h)
        {
            if (root.used)
            {
                if (this.findNode(root.right, w, h) != null)
                {
                    return this.findNode(root.right, w, h);
                }
                else if (this.findNode(root.down, w, h) != null)
                {
                    return this.findNode(root.down, w, h);
                }
                else
                {
                    return null;
                }
            }
            else if ((w <= root.w) && (h <= root.h))
                return root;
            else
                return null;

        }

        public Root SplitNode(Root node, double w, double h)
        {
            node.used = true;

            node.down = new Root { x = node.x, y = node.y + h, w = node.w, h = node.h - h };

            node.right = new Root { x = node.x + w, y = node.y, w = node.w - w, h = h };

            return node;
        }
    }
           

}


