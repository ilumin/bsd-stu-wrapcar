﻿using System;
using System.Drawing;
using System.Globalization;
using System.Windows;
using System.Collections.Generic;

namespace WrapCar._20Desing
{
    public class BezierCurve
    {
        public static void GetPinkingEffect()
        {
            //here depth will decide curve depth
            int depth = 5;
            Bitmap bmp = new Bitmap(@"C:\Users\Prapapan\Desktop\StudiO Sample\WrapCar\ImageGetArea\bin\Debug\03 Make_To_Order\ORD_201432\STK_DICUT_0.png");

            var CurvePoints = new List<System.Drawing.Point>();

            using (var g = Graphics.FromImage(bmp))
            {
                //Only Upper edge considered for Pinking effect..
                CurvePoints.Add(new System.Drawing.Point(0, 0));
                for (var i = 0; i < Math.Ceiling((Decimal)bmp.Width / (depth * 2)); i++)
                {
                    CurvePoints.Add(new System.Drawing.Point(5 + i * 10, depth));
                    CurvePoints.Add(new System.Drawing.Point((i + 1) * 10, 0));
                }
                g.FillPolygon(new SolidBrush(Color.White), CurvePoints.ToArray());
            }
            bmp.Save(@"C:\Users\Prapapan\Desktop\StudiO Sample\WrapCar\ImageGetArea\bin\Debug\03 Make_To_Order\ORD_201432\Pinking_Hydrangeas.png");
        }
    }
}
