﻿namespace WrapCar._20Desing
{
    partial class DesignForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DesignForm));
            this.openImg = new System.Windows.Forms.OpenFileDialog();
            this.layout = new System.Windows.Forms.PictureBox();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtTotalAll = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtItemPrice = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtUnitPrice = new System.Windows.Forms.TextBox();
            this.cbxStickerType = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtStickerName = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtStickerArea = new System.Windows.Forms.TextBox();
            this.txtHeightInc = new System.Windows.Forms.TextBox();
            this.txtWidthInc = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtHeightPx = new System.Windows.Forms.TextBox();
            this.txtWidthPx = new System.Windows.Forms.TextBox();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblHiddOldColor = new System.Windows.Forms.Label();
            this.lblColorCode = new System.Windows.Forms.Label();
            this.btnChangeColor = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtRatio = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtCarHeight = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtCarWidth = new System.Windows.Forms.TextBox();
            this.groupButtonCarSide = new System.Windows.Forms.GroupBox();
            this.btnBackSide = new System.Windows.Forms.Button();
            this.btnRightSide = new System.Windows.Forms.Button();
            this.btnLeftSide = new System.Windows.Forms.Button();
            this.btnFrontSide = new System.Windows.Forms.Button();
            this.btnTopSide = new System.Windows.Forms.Button();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.btnConfirmOrder = new System.Windows.Forms.Button();
            this.btnBluePrint = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnApply = new System.Windows.Forms.Button();
            this.btGetImg = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnStkText = new System.Windows.Forms.Button();
            this.btnStkLine = new System.Windows.Forms.Button();
            this.btnStkImage = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.boxImgRander = new System.Windows.Forms.PictureBox();
            this.label24 = new System.Windows.Forms.Label();
            this.boxStkRander = new System.Windows.Forms.PictureBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.lblCustName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.layout)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupButtonCarSide.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.boxImgRander)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.boxStkRander)).BeginInit();
            this.SuspendLayout();
            // 
            // openImg
            // 
            this.openImg.FileName = "openFileDialog1";
            // 
            // layout
            // 
            this.layout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.layout.Image = ((System.Drawing.Image)(resources.GetObject("layout.Image")));
            this.layout.Location = new System.Drawing.Point(12, 9);
            this.layout.Margin = new System.Windows.Forms.Padding(0);
            this.layout.Name = "layout";
            this.layout.Size = new System.Drawing.Size(750, 450);
            this.layout.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.layout.TabIndex = 3;
            this.layout.TabStop = false;
            this.layout.Paint += new System.Windows.Forms.PaintEventHandler(this.layout_Paint);
            this.layout.MouseMove += new System.Windows.Forms.MouseEventHandler(this.layout_MouseMove);
            this.layout.MouseUp += new System.Windows.Forms.MouseEventHandler(this.layout_MouseUp);
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "ลายสติ๊กเกอร์ที่เลือก :";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.txtTotalAll);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtItemPrice);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtUnitPrice);
            this.groupBox1.Controls.Add(this.cbxStickerType);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.txtStickerName);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.txtStickerArea);
            this.groupBox1.Controls.Add(this.txtHeightInc);
            this.groupBox1.Controls.Add(this.txtWidthInc);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtHeightPx);
            this.groupBox1.Controls.Add(this.txtWidthPx);
            this.groupBox1.Controls.Add(this.shapeContainer1);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.groupBox1.Location = new System.Drawing.Point(805, 461);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(414, 294);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ข้อมูลสติ๊กเกอร์ที่ใช้ :";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(296, 247);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(38, 20);
            this.label27.TabIndex = 49;
            this.label27.Text = "บาท";
            // 
            // txtTotalAll
            // 
            this.txtTotalAll.Enabled = false;
            this.txtTotalAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalAll.ForeColor = System.Drawing.Color.Blue;
            this.txtTotalAll.Location = new System.Drawing.Point(120, 236);
            this.txtTotalAll.Multiline = true;
            this.txtTotalAll.Name = "txtTotalAll";
            this.txtTotalAll.Size = new System.Drawing.Size(175, 44);
            this.txtTotalAll.TabIndex = 48;
            this.txtTotalAll.Text = "0.0";
            this.txtTotalAll.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(20, 248);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(98, 20);
            this.label26.TabIndex = 47;
            this.label26.Text = "รวมเป็นเงิน :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(295, 210);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 46;
            this.label2.Text = "(บาท)";
            // 
            // txtItemPrice
            // 
            this.txtItemPrice.Enabled = false;
            this.txtItemPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtItemPrice.Location = new System.Drawing.Point(232, 205);
            this.txtItemPrice.Name = "txtItemPrice";
            this.txtItemPrice.Size = new System.Drawing.Size(62, 22);
            this.txtItemPrice.TabIndex = 45;
            this.txtItemPrice.Text = "0.0";
            this.txtItemPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtItemPrice.TextChanged += new System.EventHandler(this.txtItemPrice_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(186, 210);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 44;
            this.label1.Text = "ราคา :";
            // 
            // txtUnitPrice
            // 
            this.txtUnitPrice.Enabled = false;
            this.txtUnitPrice.Location = new System.Drawing.Point(234, 83);
            this.txtUnitPrice.Name = "txtUnitPrice";
            this.txtUnitPrice.Size = new System.Drawing.Size(62, 20);
            this.txtUnitPrice.TabIndex = 43;
            this.txtUnitPrice.Text = "12.00";
            this.txtUnitPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtUnitPrice.TextChanged += new System.EventHandler(this.txtUnitPrice_TextChanged);
            // 
            // cbxStickerType
            // 
            this.cbxStickerType.FormattingEnabled = true;
            this.cbxStickerType.Location = new System.Drawing.Point(123, 52);
            this.cbxStickerType.Name = "cbxStickerType";
            this.cbxStickerType.Size = new System.Drawing.Size(173, 21);
            this.cbxStickerType.TabIndex = 42;
            this.cbxStickerType.SelectedIndexChanged += new System.EventHandler(this.cbxStickerType_SelectedIndexChanged);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(2, 58);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(125, 13);
            this.label25.TabIndex = 41;
            this.label25.Text = "ประเภทสติ๊กเกอร์ที่เลือก :";
            // 
            // txtStickerName
            // 
            this.txtStickerName.Enabled = false;
            this.txtStickerName.Location = new System.Drawing.Point(123, 26);
            this.txtStickerName.Name = "txtStickerName";
            this.txtStickerName.Size = new System.Drawing.Size(174, 20);
            this.txtStickerName.TabIndex = 40;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(143, 86);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(83, 13);
            this.label17.TabIndex = 35;
            this.label17.Text = "ราคา(ต่อหน่วย) :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(196, 153);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 13);
            this.label7.TabIndex = 33;
            this.label7.Text = "ยาว :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(191, 127);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(36, 13);
            this.label16.TabIndex = 32;
            this.label16.Text = "กว้าง :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(295, 154);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 13);
            this.label6.TabIndex = 31;
            this.label6.Text = "(นิ้ว)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(295, 128);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 13);
            this.label5.TabIndex = 30;
            this.label5.Text = "(นิ้ว)";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(295, 183);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(43, 13);
            this.label14.TabIndex = 29;
            this.label14.Text = "(ตร.นิ้ว)";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(117, 183);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(110, 13);
            this.label11.TabIndex = 28;
            this.label11.Text = "พท.สติ๊กเกอร์ที่ใช้ :";
            // 
            // txtStickerArea
            // 
            this.txtStickerArea.Enabled = false;
            this.txtStickerArea.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStickerArea.Location = new System.Drawing.Point(232, 179);
            this.txtStickerArea.Name = "txtStickerArea";
            this.txtStickerArea.Size = new System.Drawing.Size(62, 20);
            this.txtStickerArea.TabIndex = 28;
            this.txtStickerArea.Text = "0.0";
            this.txtStickerArea.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtStickerArea.TextChanged += new System.EventHandler(this.txtStickerArea_TextChanged);
            // 
            // txtHeightInc
            // 
            this.txtHeightInc.Enabled = false;
            this.txtHeightInc.Location = new System.Drawing.Point(232, 150);
            this.txtHeightInc.Name = "txtHeightInc";
            this.txtHeightInc.Size = new System.Drawing.Size(62, 20);
            this.txtHeightInc.TabIndex = 23;
            this.txtHeightInc.Text = "0.0";
            this.txtHeightInc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtWidthInc
            // 
            this.txtWidthInc.Enabled = false;
            this.txtWidthInc.Location = new System.Drawing.Point(232, 124);
            this.txtWidthInc.Name = "txtWidthInc";
            this.txtWidthInc.Size = new System.Drawing.Size(62, 20);
            this.txtWidthInc.TabIndex = 22;
            this.txtWidthInc.Text = "0.0";
            this.txtWidthInc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(295, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "(บาท/ตร.นิ้ว)";
            // 
            // txtHeightPx
            // 
            this.txtHeightPx.Enabled = false;
            this.txtHeightPx.Location = new System.Drawing.Point(147, 153);
            this.txtHeightPx.Name = "txtHeightPx";
            this.txtHeightPx.Size = new System.Drawing.Size(34, 20);
            this.txtHeightPx.TabIndex = 17;
            this.txtHeightPx.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtHeightPx.Visible = false;
            // 
            // txtWidthPx
            // 
            this.txtWidthPx.Enabled = false;
            this.txtWidthPx.Location = new System.Drawing.Point(146, 127);
            this.txtWidthPx.Name = "txtWidthPx";
            this.txtWidthPx.Size = new System.Drawing.Size(34, 20);
            this.txtWidthPx.TabIndex = 15;
            this.txtWidthPx.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtWidthPx.Visible = false;
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(3, 16);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(408, 275);
            this.shapeContainer1.TabIndex = 37;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape1
            // 
            this.lineShape1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 44;
            this.lineShape1.X2 = 362;
            this.lineShape1.Y1 = 96;
            this.lineShape1.Y2 = 96;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.lblHiddOldColor);
            this.groupBox2.Controls.Add(this.lblColorCode);
            this.groupBox2.Controls.Add(this.btnChangeColor);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.txtRatio);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.txtCarHeight);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.txtCarWidth);
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(804, 268);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(415, 187);
            this.groupBox2.TabIndex = 26;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "ข้อมูลพื้นฐานของรถ :";
            // 
            // lblHiddOldColor
            // 
            this.lblHiddOldColor.AutoSize = true;
            this.lblHiddOldColor.Location = new System.Drawing.Point(1, 23);
            this.lblHiddOldColor.Name = "lblHiddOldColor";
            this.lblHiddOldColor.Size = new System.Drawing.Size(86, 13);
            this.lblHiddOldColor.TabIndex = 37;
            this.lblHiddOldColor.Text = "Hidden Old color";
            this.lblHiddOldColor.Visible = false;
            // 
            // lblColorCode
            // 
            this.lblColorCode.AutoSize = true;
            this.lblColorCode.Location = new System.Drawing.Point(295, 68);
            this.lblColorCode.Name = "lblColorCode";
            this.lblColorCode.Size = new System.Drawing.Size(19, 13);
            this.lblColorCode.TabIndex = 36;
            this.lblColorCode.Text = "    ";
            // 
            // btnChangeColor
            // 
            this.btnChangeColor.BackColor = System.Drawing.Color.White;
            this.btnChangeColor.Location = new System.Drawing.Point(229, 63);
            this.btnChangeColor.Name = "btnChangeColor";
            this.btnChangeColor.Size = new System.Drawing.Size(65, 23);
            this.btnChangeColor.TabIndex = 35;
            this.btnChangeColor.UseVisualStyleBackColor = false;
            this.btnChangeColor.BackColorChanged += new System.EventHandler(this.btnChangeColor_BackColorChanged);
            this.btnChangeColor.Click += new System.EventHandler(this.btnChangeColor_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(162, 67);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(59, 13);
            this.label22.TabIndex = 34;
            this.label22.Text = "สีภายนอก :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(227, 23);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(39, 13);
            this.label21.TabIndex = 32;
            this.label21.Text = "Honda";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(181, 45);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(40, 13);
            this.label20.TabIndex = 31;
            this.label20.Text = "รุ่นรถ :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(175, 23);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(46, 13);
            this.label19.TabIndex = 30;
            this.label19.Text = "ยี่ห้อรถ :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(227, 45);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(103, 13);
            this.label18.TabIndex = 29;
            this.label18.Text = "Honda Accord 2012";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(295, 96);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(27, 13);
            this.label15.TabIndex = 28;
            this.label15.Text = "(นิ้ว)";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(296, 156);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 13);
            this.label10.TabIndex = 27;
            this.label10.Text = "(นิ้ว / Pixel)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(100, 156);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(124, 13);
            this.label9.TabIndex = 26;
            this.label9.Text = "อัตราส่วนเทียบเคียง :";
            // 
            // txtRatio
            // 
            this.txtRatio.Enabled = false;
            this.txtRatio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRatio.Location = new System.Drawing.Point(232, 152);
            this.txtRatio.Name = "txtRatio";
            this.txtRatio.Size = new System.Drawing.Size(64, 20);
            this.txtRatio.TabIndex = 25;
            this.txtRatio.Text = "0.2522";
            this.txtRatio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(295, 123);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(27, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "(นิ้ว)";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(193, 123);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(31, 13);
            this.label12.TabIndex = 18;
            this.label12.Text = "ยาว :";
            // 
            // txtCarHeight
            // 
            this.txtCarHeight.Location = new System.Drawing.Point(231, 119);
            this.txtCarHeight.Name = "txtCarHeight";
            this.txtCarHeight.Size = new System.Drawing.Size(64, 20);
            this.txtCarHeight.TabIndex = 17;
            this.txtCarHeight.Text = "57.9";
            this.txtCarHeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(188, 96);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(36, 13);
            this.label13.TabIndex = 16;
            this.label13.Text = "กว้าง :";
            // 
            // txtCarWidth
            // 
            this.txtCarWidth.Location = new System.Drawing.Point(231, 93);
            this.txtCarWidth.Name = "txtCarWidth";
            this.txtCarWidth.Size = new System.Drawing.Size(64, 20);
            this.txtCarWidth.TabIndex = 15;
            this.txtCarWidth.Text = "189.2";
            this.txtCarWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupButtonCarSide
            // 
            this.groupButtonCarSide.Controls.Add(this.btnBackSide);
            this.groupButtonCarSide.Controls.Add(this.btnRightSide);
            this.groupButtonCarSide.Controls.Add(this.btnLeftSide);
            this.groupButtonCarSide.Controls.Add(this.btnFrontSide);
            this.groupButtonCarSide.Controls.Add(this.btnTopSide);
            this.groupButtonCarSide.Location = new System.Drawing.Point(178, 460);
            this.groupButtonCarSide.Name = "groupButtonCarSide";
            this.groupButtonCarSide.Size = new System.Drawing.Size(445, 62);
            this.groupButtonCarSide.TabIndex = 41;
            this.groupButtonCarSide.TabStop = false;
            // 
            // btnBackSide
            // 
            this.btnBackSide.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnBackSide.BackgroundImage")));
            this.btnBackSide.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBackSide.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBackSide.Location = new System.Drawing.Point(346, 16);
            this.btnBackSide.Name = "btnBackSide";
            this.btnBackSide.Size = new System.Drawing.Size(70, 36);
            this.btnBackSide.TabIndex = 45;
            this.btnBackSide.UseVisualStyleBackColor = true;
            this.btnBackSide.Click += new System.EventHandler(this.btnBackSide_Click);
            // 
            // btnRightSide
            // 
            this.btnRightSide.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRightSide.BackgroundImage")));
            this.btnRightSide.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRightSide.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRightSide.Location = new System.Drawing.Point(266, 16);
            this.btnRightSide.Name = "btnRightSide";
            this.btnRightSide.Size = new System.Drawing.Size(70, 36);
            this.btnRightSide.TabIndex = 44;
            this.btnRightSide.UseVisualStyleBackColor = true;
            this.btnRightSide.Click += new System.EventHandler(this.btnRightSide_Click);
            // 
            // btnLeftSide
            // 
            this.btnLeftSide.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLeftSide.BackgroundImage")));
            this.btnLeftSide.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnLeftSide.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLeftSide.Location = new System.Drawing.Point(103, 16);
            this.btnLeftSide.Name = "btnLeftSide";
            this.btnLeftSide.Size = new System.Drawing.Size(70, 36);
            this.btnLeftSide.TabIndex = 43;
            this.btnLeftSide.UseVisualStyleBackColor = true;
            this.btnLeftSide.Click += new System.EventHandler(this.btnLeftSide_Click);
            // 
            // btnFrontSide
            // 
            this.btnFrontSide.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFrontSide.BackgroundImage")));
            this.btnFrontSide.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFrontSide.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFrontSide.Location = new System.Drawing.Point(185, 15);
            this.btnFrontSide.Name = "btnFrontSide";
            this.btnFrontSide.Size = new System.Drawing.Size(70, 36);
            this.btnFrontSide.TabIndex = 42;
            this.btnFrontSide.UseVisualStyleBackColor = true;
            this.btnFrontSide.Click += new System.EventHandler(this.btnFrontSide_Click);
            // 
            // btnTopSide
            // 
            this.btnTopSide.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnTopSide.BackgroundImage")));
            this.btnTopSide.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTopSide.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTopSide.Location = new System.Drawing.Point(20, 16);
            this.btnTopSide.Name = "btnTopSide";
            this.btnTopSide.Size = new System.Drawing.Size(70, 36);
            this.btnTopSide.TabIndex = 41;
            this.btnTopSide.UseVisualStyleBackColor = true;
            this.btnTopSide.Click += new System.EventHandler(this.btnTopSide_Click);
            // 
            // fontDialog1
            // 
            this.fontDialog1.Color = System.Drawing.SystemColors.ControlText;
            // 
            // btnConfirmOrder
            // 
            this.btnConfirmOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfirmOrder.Location = new System.Drawing.Point(405, 685);
            this.btnConfirmOrder.Name = "btnConfirmOrder";
            this.btnConfirmOrder.Size = new System.Drawing.Size(158, 62);
            this.btnConfirmOrder.TabIndex = 42;
            this.btnConfirmOrder.Text = "ยืนยันการสั่งทำ";
            this.btnConfirmOrder.UseVisualStyleBackColor = true;
            this.btnConfirmOrder.Click += new System.EventHandler(this.btnConfirmOrder_Click);
            // 
            // btnBluePrint
            // 
            this.btnBluePrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBluePrint.Location = new System.Drawing.Point(241, 685);
            this.btnBluePrint.Name = "btnBluePrint";
            this.btnBluePrint.Size = new System.Drawing.Size(158, 62);
            this.btnBluePrint.TabIndex = 43;
            this.btnBluePrint.Text = "แสดงแบบพิมพ์เขียว";
            this.btnBluePrint.UseVisualStyleBackColor = true;
            this.btnBluePrint.Click += new System.EventHandler(this.btnBluePrint_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnApply);
            this.panel1.Controls.Add(this.groupButtonCarSide);
            this.panel1.Controls.Add(this.btGetImg);
            this.panel1.Controls.Add(this.layout);
            this.panel1.Location = new System.Drawing.Point(11, 145);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(776, 527);
            this.panel1.TabIndex = 44;
            // 
            // btnApply
            // 
            this.btnApply.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApply.Location = new System.Drawing.Point(654, 470);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(108, 48);
            this.btnApply.TabIndex = 45;
            this.btnApply.Text = "ปรับใช้";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // btGetImg
            // 
            this.btGetImg.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btGetImg.BackgroundImage")));
            this.btGetImg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btGetImg.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btGetImg.Location = new System.Drawing.Point(0, 494);
            this.btGetImg.Margin = new System.Windows.Forms.Padding(0);
            this.btGetImg.Name = "btGetImg";
            this.btGetImg.Size = new System.Drawing.Size(49, 32);
            this.btGetImg.TabIndex = 2;
            this.btGetImg.UseVisualStyleBackColor = true;
            this.btGetImg.Visible = false;
            this.btGetImg.Click += new System.EventHandler(this.btGetImg_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.lblCustName);
            this.groupBox3.Controls.Add(this.label28);
            this.groupBox3.Controls.Add(this.btnStkText);
            this.groupBox3.Controls.Add(this.btnStkLine);
            this.groupBox3.Controls.Add(this.btnStkImage);
            this.groupBox3.ForeColor = System.Drawing.SystemColors.Window;
            this.groupBox3.Location = new System.Drawing.Point(11, 70);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(776, 73);
            this.groupBox3.TabIndex = 47;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "เมนูลายสติ๊กเกอร์";
            // 
            // btnStkText
            // 
            this.btnStkText.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnStkText.BackgroundImage")));
            this.btnStkText.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnStkText.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStkText.Location = new System.Drawing.Point(612, 10);
            this.btnStkText.Name = "btnStkText";
            this.btnStkText.Size = new System.Drawing.Size(149, 59);
            this.btnStkText.TabIndex = 37;
            this.btnStkText.UseVisualStyleBackColor = true;
            this.btnStkText.Click += new System.EventHandler(this.btnStkText_Click);
            // 
            // btnStkLine
            // 
            this.btnStkLine.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnStkLine.BackgroundImage")));
            this.btnStkLine.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnStkLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStkLine.Location = new System.Drawing.Point(464, 10);
            this.btnStkLine.Name = "btnStkLine";
            this.btnStkLine.Size = new System.Drawing.Size(149, 59);
            this.btnStkLine.TabIndex = 36;
            this.btnStkLine.UseVisualStyleBackColor = true;
            this.btnStkLine.Click += new System.EventHandler(this.btnStkLine_Click);
            // 
            // btnStkImage
            // 
            this.btnStkImage.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnStkImage.BackgroundImage")));
            this.btnStkImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnStkImage.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStkImage.Location = new System.Drawing.Point(316, 10);
            this.btnStkImage.Name = "btnStkImage";
            this.btnStkImage.Size = new System.Drawing.Size(149, 59);
            this.btnStkImage.TabIndex = 35;
            this.btnStkImage.UseVisualStyleBackColor = true;
            this.btnStkImage.Click += new System.EventHandler(this.btnStkImage_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Transparent;
            this.groupBox4.Controls.Add(this.boxImgRander);
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Controls.Add(this.boxStkRander);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.ForeColor = System.Drawing.Color.White;
            this.groupBox4.Location = new System.Drawing.Point(804, 70);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(414, 197);
            this.groupBox4.TabIndex = 48;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "แสดงภาพตัวอย่าง";
            // 
            // boxImgRander
            // 
            this.boxImgRander.BackColor = System.Drawing.SystemColors.Control;
            this.boxImgRander.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.boxImgRander.Location = new System.Drawing.Point(6, 32);
            this.boxImgRander.Name = "boxImgRander";
            this.boxImgRander.Size = new System.Drawing.Size(199, 154);
            this.boxImgRander.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.boxImgRander.TabIndex = 49;
            this.boxImgRander.TabStop = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(209, 16);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(82, 13);
            this.label24.TabIndex = 52;
            this.label24.Text = "สติ๊กเกอร์ที่เลือก";
            // 
            // boxStkRander
            // 
            this.boxStkRander.BackColor = System.Drawing.SystemColors.Control;
            this.boxStkRander.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.boxStkRander.Location = new System.Drawing.Point(208, 32);
            this.boxStkRander.Name = "boxStkRander";
            this.boxStkRander.Size = new System.Drawing.Size(199, 154);
            this.boxStkRander.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.boxStkRander.TabIndex = 50;
            this.boxStkRander.TabStop = false;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(9, 16);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(61, 13);
            this.label23.TabIndex = 51;
            this.label23.Text = "พื้นที่ที่เลือก";
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold);
            this.label28.Location = new System.Drawing.Point(6, 26);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(57, 26);
            this.label28.TabIndex = 49;
            this.label28.Text = "ลูกค้า :";
            // 
            // lblCustName
            // 
            this.lblCustName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold);
            this.lblCustName.Location = new System.Drawing.Point(69, 26);
            this.lblCustName.Name = "lblCustName";
            this.lblCustName.Size = new System.Drawing.Size(241, 26);
            this.lblCustName.TabIndex = 50;
            this.lblCustName.Text = "ลูกค้า :";
            // 
            // DesignForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1276, 768);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.btnConfirmOrder);
            this.Controls.Add(this.btnBluePrint);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.Name = "DesignForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Design Form";
            this.Load += new System.EventHandler(this.DesignForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layout)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupButtonCarSide.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.boxImgRander)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.boxStkRander)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openImg;
        private System.Windows.Forms.PictureBox layout;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtHeightPx;
        private System.Windows.Forms.TextBox txtWidthPx;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtHeightInc;
        private System.Windows.Forms.TextBox txtWidthInc;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtCarHeight;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtCarWidth;
        private System.Windows.Forms.TextBox txtRatio;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtStickerArea;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.GroupBox groupButtonCarSide;
        private System.Windows.Forms.Button btnBackSide;
        private System.Windows.Forms.Button btnRightSide;
        private System.Windows.Forms.Button btnLeftSide;
        private System.Windows.Forms.Button btnFrontSide;
        private System.Windows.Forms.Button btnTopSide;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Button btnChangeColor;
        private System.Windows.Forms.Label lblColorCode;
        private System.Windows.Forms.Label lblHiddOldColor;
        private System.Windows.Forms.FontDialog fontDialog1;
        private System.Windows.Forms.Button btnConfirmOrder;
        private System.Windows.Forms.Button btnBluePrint;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtStickerName;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox cbxStickerType;
        private System.Windows.Forms.TextBox txtUnitPrice;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtItemPrice;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtTotalAll;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnStkText;
        private System.Windows.Forms.Button btnStkLine;
        private System.Windows.Forms.Button btnStkImage;
        private System.Windows.Forms.Button btGetImg;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.PictureBox boxStkRander;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.PictureBox boxImgRander;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label lblCustName;
    }
}

