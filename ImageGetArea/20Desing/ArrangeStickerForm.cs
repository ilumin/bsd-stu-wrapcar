﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using PrintControl;
using WrapCar_Connection;

namespace WrapCar._20Desing
{
    public partial class ArrangeStickerForm : Form
    {
        PrintDocumentForm printForm = null;
        int stkRollWidth;
        int stkRollHeight;

        public ArrangeStickerForm()
        {
            InitializeComponent();
            this.monthCalendar1.SelectionRange = new SelectionRange(
                                    ImageUtilities.StartOfWeek(DateTime.Now, DayOfWeek.Sunday)
                                    , ImageUtilities.EndOfWeek(DateTime.Now, DayOfWeek.Saturday));

            this.LoadOrderTreeView(this.monthCalendar1.SelectionRange.Start, this.monthCalendar1.SelectionRange.End);
        }

        private void ArrangeStickerForm_Load(object sender, EventArgs e)
        {
          this.WindowState = FormWindowState.Maximized;
        }

        private void btnLoadSticker_Click(object sender, EventArgs e)
        {
            this.arrangeStickerMain();
        }

        private void btnPrintPreview_Click(object sender, EventArgs e)
        {
            this.showPrintPreview();
        }

        public void showPrintPreview()
        {
            try
            {
                bool isEmty = this.arrangeStickerMain();
                if (!isEmty)
                {
                    Cursor = Cursors.WaitCursor;

                    if (printForm == null)
                        printForm = new PrintDocumentForm(pnlOutput);

                    Cursor = Cursors.Default;
                    printForm.Show();
                    printForm.Activate();
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return;
            }

        }

        public bool arrangeStickerMain()
        {
            this.stkRollWidth = this.pnlOutput.Width;
            this.stkRollHeight = this.pnlOutput.Height;

            //Step 0: Load sticker image from order.
            Bitmap[] bmps = this.getStickerFilesFromFolder();

            bool isEmty = (bmps == null || bmps.Length == 0);
            if (!isEmty)
            {
                List<Bitmap> bmpsRaw = bmps.ToList();

                //Step 1: Sorted Sticker By Size DESC
                bmpsRaw = this.sortingBySizeStickerFiles(bmpsRaw);

                //Step 2: Arrange sticker items  on paper roll.
                List<List<Piece>> stkTable = this.arrangeStickerPiecePuzzle(bmpsRaw);
                //this.packingStickerToBins(bmpsRaw);

                //Step 3: Display sticker items on paper roll.
                this.displayArrangeStickerPiece(stkTable);
            }
            else
            {
                DialogResult result = MessageBox.Show("ไม่พบรายการสั่งทำสติ๊กเกอร์วันนี้ กรุณาตรวจสอบใหม่.");
                this.Close();
            }
            return isEmty;
        }

        private void packingStickerToBins(List<Bitmap> bmpsRaw)
        {
            List<Block> blocks = new List<Block>();
            foreach(Bitmap bmp in bmpsRaw){
                Block block = new Block();
                block.w = bmp.Width;
                block.h = bmp.Height;
                blocks.Add(block);
            }

            // New Packer
            Packer packer = new Packer(this.pnlOutput.Width, this.pnlOutput.Height);
            packer.Fit(blocks);

            this.displayStickerPiece(blocks);
        }


        private void displayStickerPiece(List<Block> blocks)
        {
            float[] dashValues = { 2, 2, 2, 2 };
            Pen blackPen = new Pen(Color.Gray, 1);
            blackPen.DashPattern = dashValues;

            Bitmap bb = new Bitmap(pnlOutput.Width, pnlOutput.Height);
            Graphics e = Graphics.FromImage(bb);

            for (int n = 0; n < blocks.Count; n++)
            {
                Block block = blocks[n];
                //Draw Image Border
                e.DrawRectangle(blackPen, Convert.ToInt16(block.Fit.x), Convert.ToInt16(block.Fit.y), Convert.ToInt16(block.w), Convert.ToInt16(block.h));
                
            }
            
        }

        private Bitmap[] getStickerFilesFromFolder()
        {
            string path = Constants.BASE_PATH + Constants.DRAFT_DIR + "/" + Constants.orderID;
            Bitmap[] bmps = null;

            try
            {
                string[] filePaths = Directory.GetFiles(path, Constants.STK_PRINT_CUTOFF_PREFIX + "*.png", SearchOption.AllDirectories);
                bmps = new Bitmap[filePaths.Length];

                int i = 0;
                foreach (string imgPath in filePaths)
                {
                    bmps[i] = (Bitmap)Image.FromFile(imgPath);
                    i++;
                }

            } catch(Exception ex){
                Console.WriteLine( ex.StackTrace);
            }

            return bmps;
        }

        private Bitmap[] getStickerFilesFromDB()
        {
            OrderDAO dao = new OrderDAO();
            Bitmap[] bmps = null;
            try
            {
                bmps = dao.getStickerItemListForPrint(DateTime.Now).ToArray() ;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }

            return bmps;
        }

        private List<Bitmap> sortingBySizeStickerFiles(List<Bitmap> bmpsRaw)
        {
            //Order by size (width*height) DESC
            bmpsRaw = bmpsRaw.OrderByDescending(t => (t.Width*t.Height)).ToList();
            return bmpsRaw;
        }

        private List<List<Piece>> arrangeStickerPiecePuzzle(List<Bitmap> bmpsRaw)
        {
            List<List<Piece>> hashRowtable = new List<List<Piece>>();
           
            //Iniitial sticker set in the first row
            List<Piece> stkBmpCols = new List<Piece>();
            hashRowtable.Add(stkBmpCols);

            int accWidth = 0;
            int accHeight = 0;
            int rowAt = 0;
            int pieceID = 0;
            int stX = 0;
            int stY = 0;

            //Recursive Arrange
            hashRowtable = this.arrangeWithRecursive(bmpsRaw
                                                   , hashRowtable
                                                   , stkBmpCols
                                                   , accWidth
                                                   , accHeight
                                                   , rowAt
                                                   , pieceID
                                                   , stX
                                                   , stY
                                                   );

            return hashRowtable;
        }
        private List<List<Piece>> arrangeWithRecursive(   List<Bitmap> bmpsRaw
                                                        , List<List<Piece>> hashRowtable
                                                        , List<Piece> stkBmpCols
                                                        , int accWidth
                                                        , int accHeight
                                                        , int rowAt
                                                        , int pieceID
                                                        , int stX
                                                        , int stY) {
        
            foreach (Bitmap bmp in bmpsRaw)
            {
                //bool overWidth = (accWidth >= this.stkRollWidth);
                int usedWidth = stkBmpCols.Sum(t => t.Width);
                int avaliableWidth = this.stkRollWidth - usedWidth;

                Bitmap current = bmpsRaw.Find(t => (t.Width <= avaliableWidth));

                //Add sticker item in the row
                if (current != null)
                {
                    accWidth += current.Width;
                    accHeight += current.Height;

                    Piece piece = new Piece
                    {
                        ID = pieceID,
                        Width = current.Width,
                        Height = current.Height,
                        startX = stX,
                        startY = stY,
                        Picture = current
                    };
                    stkBmpCols.Add(piece);
                    bmpsRaw.Remove(current);
                    pieceID++;

                    hashRowtable[rowAt] = stkBmpCols;
                    stX = accWidth;
                }
                else
                {
                   
                    ////Add sticker item in the row
                    //if (current != null)
                    //{


                    //    Piece piece = new Piece
                    //    {
                    //        ID = pieceID,
                    //        Width = current.Width,
                    //        Height = current.Height,
                    //        startX = stX,
                    //        startY = stY,
                    //        Picture = current
                    //    };

                    //    stkBmpCols.Add(piece);
                    //    bmpsRaw.Remove(current);
                    //    pieceID++;

                    //    hashRowtable[rowAt] = stkBmpCols;
                    //    stX = accWidth;

                    //}
                    ////Create sticker item in the new row
                    //else
                    //{
                        rowAt++;

                        accWidth = bmp.Width;
                        accHeight = 0;
                        stX = 0;
                        stY += stkBmpCols.Max(r => r.Height); //Max height at row-1

                        stkBmpCols = new List<Piece>();
                        Piece piece = new Piece
                        {
                            ID = pieceID,
                            Width = bmp.Width,
                            Height = bmp.Height,
                            startX = stX,
                            startY = stY,
                            Picture = bmp
                        };

                        stkBmpCols.Add(piece);
                        bmpsRaw.Remove(bmp);

                        pieceID++;

                        hashRowtable.Add(stkBmpCols);
                        stX = accWidth;
                    //}
                }

                // Finding Next item
                this.arrangeWithRecursive(bmpsRaw
                                      , hashRowtable
                                      , stkBmpCols
                                      , accWidth
                                      , accHeight
                                      , rowAt
                                      , pieceID
                                      , stX
                                      , stY
                                      );

                if (bmpsRaw.Count == 0) break;
            }

            return hashRowtable;
       }

        private void displayArrangeStickerPiece(List<List<Piece>> hashtable)
        {
            Bitmap bb = new Bitmap(pnlOutput.Width, pnlOutput.Height);
            pnlOutput.Image = bb;
            Graphics e = Graphics.FromImage(bb);

            float[] dashValues = { 2, 2, 2, 2 };
            Pen blackPen = new Pen(Color.Gray, 1);
            blackPen.DashPattern = dashValues;

            #region print data
            int i = 0;
            foreach (List<Piece> bmpList in hashtable)
            {            
                foreach (Piece piece in bmpList)
                {
                    //Draw Image
                    e.DrawImage(piece.Picture, piece.startX, piece.startY);

                    //Draw Image Border
                    e.DrawRectangle(blackPen, piece.startX, piece.startY, piece.Width, piece.Height);
                 }
                i++;
            }
            #endregion
        }

        private void pnlOutput_MouseDown(object sender, MouseEventArgs e)
        {
           
        }

        private void pnlOutput_MouseMove(object sender, MouseEventArgs e)
        {
           
        }

        private void pnlOutput_MouseUp(object sender, MouseEventArgs e)
        {
           
        
        }

        private void monthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
        {
            DateTime selDate = this.monthCalendar1.SelectionStart;
            //this.monthCalendar1.SelectionRange.Start = ImageUtilities.StartOfWeek(this.monthCalendar1.SelectionStart, DayOfWeek.Sunday);
            //this.monthCalendar1.SelectionRange.End = ImageUtilities.StartOfWeek(this.monthCalendar1.SelectionStart, DayOfWeek.Saturday);
            this.monthCalendar1.SelectionRange = new SelectionRange(
                                      ImageUtilities.StartOfWeek(selDate, DayOfWeek.Sunday)
                                    , ImageUtilities.EndOfWeek(selDate, DayOfWeek.Saturday));
        }

        private void LoadOrderTreeView(DateTime startDt, DateTime endDt)
        {
            string path = Constants.BASE_PATH + Constants.DRAFT_DIR;
            string[] drives = Directory.GetDirectories(path);

            this.BildingOrderTreeView(drives, -1);
        }

        private void treeView1_AfterExpand(object sender, TreeViewEventArgs e)
        {
            try
            {
                TreeNode node = e.Node;
                string drivePath = node.Tag.ToString();

                string[] filePaths = Directory.GetFiles(drivePath, "*.png", SearchOption.AllDirectories);
                treeView1.Nodes[e.Node.Index].Nodes.Clear();
                this.BildingOrderTreeView(filePaths, e.Node.Index);

            } catch(Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }

        private void BildingOrderTreeView(string[] drives, int nodeIdx)
        {
           
            foreach (string drive in drives)
            {
                DriveInfo di = new DriveInfo(drive);
                int driveImage;

                switch (di.DriveType)    //set the drive's icon
                {
                    case DriveType.CDRom:
                        driveImage = 3;
                        break;
                    case DriveType.Network:
                        driveImage = 6;
                        break;
                    case DriveType.NoRootDirectory:
                        driveImage = 8;
                        break;
                    case DriveType.Unknown:
                        driveImage = 8;
                        break;
                    default:
                        driveImage = 2;
                        break;
                }

                TreeNode node = new TreeNode(Path.GetFileName(drive), driveImage, driveImage);
                node.Tag = drive;

                if (nodeIdx == -1)
                {
                    if (di.IsReady)
                    {
                        node.Nodes.Add("");
                    }

                    treeView1.Nodes.Add(node);                   
                }
                else
                {          
                   treeView1.Nodes[nodeIdx].Nodes.Add(node);              
                    
                }
            }
        }

    }
}
