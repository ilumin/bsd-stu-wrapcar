﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WrapCar_Connection;
using WrapCar_DataModel;
using WrapCar_DataModel.Order;

namespace WrapCar._20Desing
{
    public partial class CustomerOrderForm : Form
    {
        ArrangeStickerForm arrgForm;

        public CustomerOrderForm()
        {
            InitializeComponent();
        }

        private void ArrangeStickerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (arrgForm == null)
                arrgForm = new ArrangeStickerForm();

            arrgForm.showPrintPreview();
        }

        private void CustomerOrderForm_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            this.pnlBackground.Width = this.Width;
            this.pnlBackground.Height = this.Height;

            this.gvStickerList.ColumnCount = 5;
            this.gvStickerList.RowCount = 3;
            btnCreateNew.Enabled = false;
            btnMyGallory.Enabled = false;
            btnCopy.Enabled = false;

            this.clearOrderDisplay();
            this.initCustomerList();
        }
        List<CustomerData> customerList = null;
        private void initCustomerList()
        {
            CustomerDAO dao = new CustomerDAO();
            customerList = dao.getCustomerList();

            DataTable table = new DataTable();
            table.Columns.Add("รหัสลูกค้า", typeof(int)); // Cells[0]
            table.Columns.Add("คำนำหน้า", typeof(string));// Cells[1]
            table.Columns.Add("ชื่อลูกค้า", typeof(string));// Cells[2]
            table.Columns.Add("เบอร์โทรศัพท์", typeof(string));// Cells[3]
            this.gvCustomerList.DataSource = table;

            this.BlidingCustomerGridView(customerList);

            DataTable table2 = new DataTable();
            table2.Columns.Add("รหัสรถ", typeof(int)); // Cells[0]
            table2.Columns.Add("เลขทะเบียน", typeof(string));// Cells[1]
            table2.Columns.Add("ยี่ห้อรถ", typeof(string));// Cells[2]
            table2.Columns.Add("รุ่น", typeof(string));// Cells[3]
            table2.Columns.Add("โฉมปี", typeof(string));// Cells[4]
            this.gvCarList.DataSource = table2;
        }

        private void BlidingCustomerGridView(List<CustomerData> customerList)
        {
            DataTable table = (DataTable)this.gvCustomerList.DataSource;
            table.Rows.Clear();
            foreach (CustomerData data in customerList)
            {
                table.Rows.Add(data.CUS_ID
                                , data.TITLE
                                , data.CUS_FNAME + " " + data.CUS_LNAME
                                , data.MOBILE_NO
                                );
            }
            this.gvCustomerList.DataSource = table;

            this.gvCustomerList.ColumnHeadersDefaultCellStyle.Font = new Font("Tahoma", 8F, FontStyle.Bold);
            this.gvCustomerList.Columns[0].Width = 30;
            this.gvCustomerList.Columns[1].Width = 60;
            this.gvCustomerList.Columns[2].Width = 200;
        }


        private void BlidingCustomerCarGridView(List<CustomerCarData> customerCarList)
        {
            DataTable table = (DataTable)this.gvCarList.DataSource;
            table.Rows.Clear();

            if (customerCarList.Count > 0)
            {
                foreach (CustomerCarData data in customerCarList)
                {
                    table.Rows.Add(data.CUS_CAR_ID
                                    , data.CAR_LICENSE_NO
                                    , data.CAR_BRAND
                                    , data.CAR_MODEL
                                    , data.CAR_YEAR
                                    );
                }
                this.gvCarList.DataSource = table;

                this.gvCarList.ColumnHeadersDefaultCellStyle.Font = new Font("Tahoma", 8F, FontStyle.Bold);
                this.btnCreateNew.Enabled = true;
                this.btnCopy.Enabled = true;
            }
            else
            {
                this.clearOrderDisplay();
                this.btnCreateNew.Enabled = false;
                this.btnCopy.Enabled = false;
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            List<CustomerData> filterList = customerList.Where(t => t.CUS_FNAME.Contains(txtSearhCustName.Text)).ToList() ;
            this.BlidingCustomerGridView(filterList);

        }


        private void btnCreateNew_Click(object sender, EventArgs e)
        {
            DesignForm form = new DesignForm(Convert.ToInt32(txtCusId.Text), Convert.ToInt32(hddCustCarID.Text), null);
            form.Show();
        }

        private List<OrderHData> orderHList = null;
        private StickerListForm stkListForm = null;
        private void gvCustomerList_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            try {
                   int selectedCustId = Convert.ToInt32(this.gvCustomerList.Rows[e.RowIndex].Cells[0].Value);
                   OrderDAO dao = new OrderDAO();
                   orderHList = dao.getOrderList(selectedCustId);
                   
                   CustomerData customer = customerList.Find(t => t.CUS_ID == selectedCustId);
                   txtCusId.Text = customer.CUS_ID.ToString();
                   txtCustName.Text = customer.TITLE + " " + customer.CUS_FNAME + " " + customer.CUS_LNAME;
                   txtEmail.Text = customer.EMAIL;
                   txtTelNo.Text = customer.MOBILE_NO;
                   txtTotalOrder.Text = "0";
                   btnCreateNew.Enabled = false;
                   btnMyGallory.Enabled = false;
                   btnCopy.Enabled = false;

                   this.clearOrderDisplay();

                //################ Prepare Car data
                   CustomerDAO dao1 = new CustomerDAO();
                   List<CustomerCarData> customerCarList = dao1.getCustomerCarList();
                   customerCarList = customerCarList.Where(t => t.CUS_ID == customer.CUS_ID).ToList();
                   this.BlidingCustomerCarGridView(customerCarList);


            } catch (Exception ex)
            {
              Console.WriteLine( ex.StackTrace);
            }
        }

        private void gvCarList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                int selectedCarId = Convert.ToInt32(this.gvCarList.Rows[e.RowIndex].Cells[0].Value);
                this.clearOrderDisplay();

                this.imageList1.ImageSize = new Size(180, 100);
                if (orderHList != null && orderHList.Count > 0)
                {
                    txtTotalOrder.Text = orderHList.Count().ToString();
                    hddCustCarID.Text = selectedCarId.ToString();
                    btnMyGallory.Enabled = false;
                    btnCreateNew.Enabled = true;
                    btnCopy.Enabled = true;

                    foreach (OrderHData orH in orderHList)
                    {
                        this.imageList1.Images.Add(ImageUtilities.ByteArrayToImage(orH.IMG_PREVIEW));
                    }

                    //Creation of the Image List 
                    stkListForm = new StickerListForm(Constants.STK_IMAGE_TYPE);
                    stkListForm.Init(this.imageList1, 0, 0, 4, 2);
                    stkListForm.ItemClick += new StickerListLoadingEventHandler(OnItemClicked);
                    stkListForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;

                    stkListForm.MdiParent = this;
                    this.panel1.Controls.Add(stkListForm);
                    stkListForm.Dock = DockStyle.Fill;
                    stkListForm.Show();

                }
                // New customer, no any order
                else
                {
                    btnMyGallory.Enabled = false;
                    btnCreateNew.Enabled = !btnMyGallory.Enabled;
                    DialogResult result = MessageBox.Show("ไม่พบแบบจำลองของฉัน ต้องการสร้างแบบจำลองใหม่หรือไม่ ?", "ข้อความยืนยัน", MessageBoxButtons.YesNo);
                    if (DialogResult.Yes == result)
                    {
                        DesignForm form = new DesignForm(Convert.ToInt32(txtCusId.Text), Convert.ToInt32( hddCustCarID.Text), null);
                        form.Show();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }



        private void btnCopy_Click(object sender, EventArgs e)
        {
            DesignForm form = new DesignForm(Convert.ToInt32(txtCusId.Text), Convert.ToInt32(hddCustCarID.Text), dataH);
            form.Show();
        }

        private void clearOrderDisplay()
        {
            txtOrderDate.Text = "";
            txtOrderID.Text = "";
            txtStkUseTotal.Text = "";
            txtTotalAll.Text = "";
            txtUnitPrice.Text = "";
            hddCustCarID.Text = "";

            this.pictureBox1.Image = null;
            this.gvStickerList.Rows.Clear();
            this.imageList1.Images.Clear();

            if(this.stkListForm != null){
                this.panel1.Controls.Remove(stkListForm);
                this.stkListForm.Close();
                this.stkListForm = null;
            }

            this.gvStickerList.Columns[0].Name = "ลำดับ";
            this.gvStickerList.Columns[1].Name = "รายการสติกเกอร์";
            this.gvStickerList.Columns[2].Name = "กว้าง x ยาว (นิ้ว)";
            this.gvStickerList.Columns[3].Name = "พท.ที่ใช้ (ตร.นิ้ว)";
            this.gvStickerList.Columns[4].Name = "ราคา (บาท)";
            this.gvStickerList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.gvStickerList.ColumnHeadersDefaultCellStyle.Font = new Font("Tahoma", 8F, FontStyle.Bold);
            this.gvStickerList.Columns[0].Width = 40;
        }

        private OrderHData dataH;
        private void OnItemClicked(object sender, StickerListLoadingEventArgs e)
        {
           //Display Preview
           Bitmap selectedItem = (Bitmap)this.imageList1.Images[e.SelectedItem];
           if (orderHList != null)
           {
               if (e.SelectedItem < orderHList.Count())
               {
                   dataH = orderHList[e.SelectedItem];
                   txtOrderDate.Text = dataH.ORDER_DATE.ToString("MMMM dd, yyyy hh:mm");
                   txtOrderID.Text = dataH.ORDER_H_ID;
                   txtStkUseTotal.Text = dataH.OrderDList.Count().ToString("#,##0.00");
                   txtTotalAll.Text = Convert.ToDecimal(dataH.TOTAL_PRICE).ToString("#,##0.00");
                   txtUnitPrice.Text = "1.50";

                   this.blindingStickerGridViwe(dataH);
               }
           }
        }

        private void blindingStickerGridViwe(OrderHData dataH)
        {
            this.pictureBox1.Image = ImageUtilities.ResizeImage((Bitmap)ImageUtilities.ByteArrayToImage(dataH.IMG_PREVIEW), this.pictureBox1.Width, this.pictureBox1.Height);
            List<OrderDData> dataDList = dataH.OrderDList.ToList();

            int i = 0;
            this.gvStickerList.Rows.Clear();
            foreach (OrderDData dataD in dataDList)
            {

                DataGridViewRow row = new DataGridViewRow();
                //No.
                DataGridViewTextBoxCell NoCell = new DataGridViewTextBoxCell();
                NoCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                NoCell.Value = (++i).ToString();
                row.Cells.Add(NoCell);

                //Sticker.
                Image img = ImageUtilities.ResizeImage(ImageUtilities.ByteArrayToImage(dataD.ORIGINAL_IMG), 40, 24);
                DataGridViewImageCell ImageCell = new DataGridViewImageCell();
                ImageCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                ImageCell.Value = img;
                row.Cells.Add(ImageCell);

                //Size.
                DataGridViewTextBoxCell SizeCell = new DataGridViewTextBoxCell();
                SizeCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                SizeCell.Value = dataD.WIDTH_INCH + " x " + dataD.HEIGHT_INCH;
                row.Cells.Add(SizeCell);

                //Area.
                DataGridViewTextBoxCell AreaCell = new DataGridViewTextBoxCell();
                AreaCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                AreaCell.Value = Convert.ToDecimal(dataD.AREA_USED_INCH).ToString("#,##0.00");
                row.Cells.Add(AreaCell);

                //Unit Price.
                DataGridViewTextBoxCell UnitPriceCell = new DataGridViewTextBoxCell();
                UnitPriceCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                UnitPriceCell.Value = Convert.ToDecimal(dataD.UNIT_PRICE).ToString("#,##0.00");
                row.Cells.Add(UnitPriceCell);
                this.gvStickerList.Rows.Add(row);

            }
        }



    }
}
