﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WrapCar_DataModel;

namespace WrapCar_Connection
{
    public class CustomerDAO
    {
        #region Customer

        public CustomerData getCustomer(int CUST_ID)
        {
            try
            {
                Entities context = new Entities();

                var result = (from x in context.tb_m_customer
                              where x.CUS_ID == CUST_ID
                              select new CustomerData()
                              {
                                  CUS_FNAME = x.CUS_FNAME,
                                  CUS_ID = x.CUS_ID,
                                  CUS_LNAME = x.CUS_LNAME,
                                  DRIVING_LICENCE_EXP_DATE = x.DRIVING_LICENCE_EXP_DATE,
                                  DRIVING_LICENCE_ID = x.DRIVING_LICENCE_ID,
                                  DRIVING_LICENCE_ISSUE_DATE = x.DRIVING_LICENCE_ISSUE_DATE,
                                  EMAIL = x.EMAIL,
                                  ID_CARD = x.ID_CARD,
                                  ID_CARD_EXP_DATE = x.ID_CARD_EXP_DATE,
                                  ID_CARD_ISSUE_DATE = x.ID_CARD_ISSUE_DATE,
                                  MOBILE_NO = x.MOBILE_NO,
                                  REGISTER_DATE = x.REGISTER_DATE,
                                  TITLE = x.TITLE
                              }).FirstOrDefault();

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CustomerData> getCustomerList()
        {
            try
            {
                Entities context = new Entities();

                var result = (from x in context.tb_m_customer
                              select new CustomerData()
                              {
                                  CUS_FNAME = x.CUS_FNAME,
                                  CUS_ID = x.CUS_ID,
                                  CUS_LNAME = x.CUS_LNAME,
                                  DRIVING_LICENCE_EXP_DATE = x.DRIVING_LICENCE_EXP_DATE,
                                  DRIVING_LICENCE_ID = x.DRIVING_LICENCE_ID,
                                  DRIVING_LICENCE_ISSUE_DATE = x.DRIVING_LICENCE_ISSUE_DATE,
                                  EMAIL = x.EMAIL,
                                  ID_CARD = x.ID_CARD,
                                  ID_CARD_EXP_DATE = x.ID_CARD_EXP_DATE,
                                  ID_CARD_ISSUE_DATE = x.ID_CARD_ISSUE_DATE,
                                  MOBILE_NO = x.MOBILE_NO,
                                  REGISTER_DATE = x.REGISTER_DATE,
                                  TITLE = x.TITLE
                              }).ToList();

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public bool saveCustomer(tb_m_customer data)
        {
            bool ret = false;
            try
            {
                Entities context = new Entities();
                context.tb_m_customer.Add(data);
                context.SaveChanges();

                ret = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ret;
        }

        public bool UpdateCustomer(tb_m_customer data)
        {
            bool ret = false;
            try
            {
                Entities context = new Entities();

                tb_m_customer change = context.tb_m_customer.FirstOrDefault(t => t.CUS_ID == data.CUS_ID);
                change.TITLE = data.TITLE;
                change.CUS_FNAME = data.CUS_FNAME;
                change.CUS_LNAME = data.CUS_LNAME;
                change.EMAIL = data.EMAIL;
                change.MOBILE_NO = data.MOBILE_NO;
                change.ID_CARD = data.ID_CARD;
                change.ID_CARD_ISSUE_DATE = data.ID_CARD_ISSUE_DATE;
                change.ID_CARD_EXP_DATE = data.ID_CARD_EXP_DATE;
                change.DRIVING_LICENCE_ID = data.DRIVING_LICENCE_ID;
                change.DRIVING_LICENCE_ISSUE_DATE = data.DRIVING_LICENCE_ISSUE_DATE;
                change.DRIVING_LICENCE_EXP_DATE = data.DRIVING_LICENCE_EXP_DATE;
                context.SaveChanges();

                ret = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ret;
        }



        public bool saveCustomerCar(tb_m_customer_car data1)
        {
            bool ret1 = false;
            try
            {
                Entities context = new Entities();
                context.tb_m_customer_car.Add(data1);
                context.SaveChanges();

                ret1 = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ret1;
        }


        public bool saveCustomerCar(tb_m_customer data1)
        {
            throw new NotImplementedException();
        }


        public CustomerCarData getCustomerCar(int CAR_ID)
        {
            try
            {
                Entities context = new Entities();
                var result1 = (from x in context.tb_m_customer_car
                               where x.CUS_CAR_ID == CAR_ID
                               select new CustomerCarData()
                               {
                                   CAR_BRAND = x.CAR_BRAND,
                                   CAR_COLOR = x.CAR_COLOR,
                                   CAR_COLOR_HEXCODE = x.CAR_COLOR_HEXCODE,
                                   CAR_LICENSE_NO = x.CAR_LICENSE_NO,
                                   CAR_MODEL = x.CAR_MODEL,
                                   CAR_YEAR = x.CAR_YEAR,
                                   CUS_CAR_ID = x.CUS_CAR_ID,
                                   CUS_ID = x.CUS_ID,
                               }).FirstOrDefault();
                return result1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CustomerCarData> getCustomerCarList()
        {
            try
            {
                Entities context = new Entities();

                var result = (from x in context.tb_m_customer_car
                              //where x.CUS_ID == CUS_ID
                              select new CustomerCarData()
                              {
                                  CAR_BRAND = x.CAR_BRAND,
                                  CAR_COLOR = x.CAR_COLOR,
                                  CAR_COLOR_HEXCODE = x.CAR_COLOR_HEXCODE,
                                  CAR_LICENSE_NO = x.CAR_LICENSE_NO,
                                  CAR_MODEL = x.CAR_MODEL,
                                  CAR_YEAR = x.CAR_YEAR,
                                  CUS_CAR_ID = x.CUS_CAR_ID,
                                  CUS_ID = x.CUS_ID
                              }).ToList();

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int CUS_ID { get; set; }

        public bool UpdateCustomerCar(tb_m_customer_car data)
        {
            bool ret = false;
            try
            {
                Entities context = new Entities();

                tb_m_customer_car change = context.tb_m_customer_car.FirstOrDefault(t => t.CUS_ID == data.CUS_ID && t.CUS_CAR_ID == data.CUS_CAR_ID);
                change.CUS_CAR_ID = data.CUS_CAR_ID;
                change.CUS_ID = data.CUS_ID;
                change.CAR_LICENSE_NO = data.CAR_LICENSE_NO;
                change.CAR_BRAND = data.CAR_BRAND;
                change.CAR_MODEL = data.CAR_MODEL;
                change.CAR_YEAR = data.CAR_YEAR;
                change.CAR_COLOR = data.CAR_COLOR;
                change.CAR_COLOR_HEXCODE = data.CAR_COLOR_HEXCODE;
                context.SaveChanges();


                ret = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ret;
        }

    }
}
