﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WrapCar_DataModel;
using System.Data.Entity;
using System.Data.SqlClient; 

namespace WrapCar_Connection
{
    public class CarInOutDAO
    {
        public CarInOutData getCarInOutData(int ID)
        {
            try
            {
                Entities context = new Entities();

                var result = (from car in context.tb_r_carinout
                              join e1 in context.tb_m_employee on car.CARIN_EMP_ID equals e1.EMP_ID into emp3
                              from emp in emp3.DefaultIfEmpty()
                              join e2 in context.tb_m_employee on car.CAROUT_EMP_ID equals e2.EMP_ID into emp4
                              from emp1 in emp4.DefaultIfEmpty()                          
                              where ((car.APP_ID == ID)
                                                           )
                              select new CarInOutData()
                              {
                                  CARINOUT_ID = car.CARINOUT_ID,
                                  APP_ID = car.APP_ID,
                                  CARIN_DATE = car.CARIN_DATE,
                                  CARIN_EMP_ID = car.CARIN_EMP_ID,
                                  CARIN_EMP_TITLE = emp.TITLE,
                                  CARIN_EMP_FNAME = emp.EMP_FNAME,
                                  CARIN_EMP_LNAME = emp.EMP_LNAME,
                                  CAROUT_DATE = car.CAROUT_DATE,
                                  CAROUTEMP_ID = car.CAROUT_EMP_ID,
                                  CAROUT_EMP_TITLE = emp1.TITLE,
                                  CAROUT_EMP_FNAME = emp1.EMP_FNAME,
                                  CAROUT_EMP_LNAME = emp1.EMP_LNAME
                              }).FirstOrDefault();

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

            public APPCarData getAPPCarData (int ID)
            {
                try
                {
                Entities context = new Entities();

                var result = (from app in context.tb_r_appointment
                              from order_h in context.tb_r_order_h
                              from cus in context.tb_m_customer
                              from cus_car in context.tb_m_customer_car                              
                              where ((app.APP_ID == ID)
                              && (app.ORDER_H_ID == order_h.ORDER_H_ID)
                              && (order_h.CUS_ID == cus.CUS_ID)
                              && (order_h.CUS_ID == cus_car.CUS_ID) && (order_h.CUS_CAR_ID == cus_car.CUS_CAR_ID)
                              )
                              select new APPCarData()
                              {
                                  APP_ID = app.APP_ID,
                                  APP_DATETIME_START = app.APP_DATETIME_START,
                                  APP_DATETIME_FINISH = app.APP_DATETIME_FINISH,
                                  STATUS_APP = app.STATUS_APP_POINTMENT,                     
                                  ORDER_H_ID = order_h.ORDER_H_ID,                                  
                                  CUS_ID = order_h.CUS_ID,
                                  CUS_FNAME = cus.CUS_FNAME,
                                  CUS_LNAME = cus.CUS_LNAME,
                                  TITLE = cus.TITLE,
                                  CUS_CAR_ID = order_h.CUS_CAR_ID,
                                  CAR_LICENSE_NO = cus_car.CAR_LICENSE_NO,
                                  CAR_BRAND = cus_car.CAR_BRAND,
                                  CAR_MODEL = cus_car.CAR_MODEL,
                                  CAR_YEAR = cus_car.CAR_YEAR,
                                  CAR_COLOR = cus_car.CAR_COLOR,                                  
                              }).FirstOrDefault();

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
            }

            public bool saveCarintout(tb_r_carinout data)
            {
                bool ret = false;
                try
                {
                    Entities context = new Entities();
                    context.tb_r_carinout.Add(data);

                    var change = context.tb_r_appointment.FirstOrDefault(a => a.APP_ID == data.APP_ID);

                    change.STATUS_APP_POINTMENT = "IN PROGRESS";          
                    context.SaveChanges();

                    ret = true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return ret;
            }        
           

            public bool Updatecarout(int ID,int car_id,int emp_id)
            {
                bool ret = false;

                try
                {
                    Entities context = new Entities();
                    var change = context.tb_r_carinout.FirstOrDefault(a => a.CARINOUT_ID == car_id);

                    change.CAROUT_DATE = DateTime.Now;
                    change.CAROUT_EMP_ID = emp_id;

                    var change1 = context.tb_r_appointment.FirstOrDefault(a => a.APP_ID == ID);

                    change1.STATUS_APP_POINTMENT = "APPROVE";


                    context.SaveChanges();
                    ret = true;

                    return ret;
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }

            public List<TaskData> getListTask(int ID)
            {
                try
                {
                    Entities context = new Entities();

                    var result = (from task in context.tb_r_tasks_schedule
                                  where ((task.APP_ID == ID)
                                                               )
                                  select new TaskData()
                                  {
                                      TASK_ID = task.TASK_ID,
                                      APP_ID = task.APP_ID,
                                      work_status = task.WORK_STATUS
                                      
                                  }).ToList();

                    return result;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

        
    }
}
