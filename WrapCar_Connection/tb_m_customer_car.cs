//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WrapCar_Connection
{
    using System;
    using System.Collections.Generic;
    
    public partial class tb_m_customer_car
    {
        public int CUS_CAR_ID { get; set; }
        public int CUS_ID { get; set; }
        public string CAR_LICENSE_NO { get; set; }
        public string CAR_BRAND { get; set; }
        public string CAR_MODEL { get; set; }
        public string CAR_YEAR { get; set; }
        public string CAR_COLOR { get; set; }
        public string CAR_COLOR_HEXCODE { get; set; }
    
        public virtual tb_m_customer tb_m_customer { get; set; }
    }
}
