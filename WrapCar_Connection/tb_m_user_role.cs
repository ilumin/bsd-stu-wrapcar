//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WrapCar_Connection
{
    using System;
    using System.Collections.Generic;
    
    public partial class tb_m_user_role
    {
        public int ROLE_ID { get; set; }
        public string ROLE_NAME { get; set; }
        public string ROLE_DESIGN { get; set; }
        public string ROLE_MASTER { get; set; }
        public string ROLE_CUSTOMER { get; set; }
        public string ROLE_APPOINTMENT { get; set; }
        public string ROLE_SCHEDULE_WORKER { get; set; }
    }
}
