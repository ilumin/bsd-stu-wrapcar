//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WrapCar_Connection
{
    using System;
    using System.Collections.Generic;
    
    public partial class tb_m_customer
    {
        public tb_m_customer()
        {
            this.tb_m_customer_car = new HashSet<tb_m_customer_car>();
        }
    
        public int CUS_ID { get; set; }
        public string TITLE { get; set; }
        public string CUS_FNAME { get; set; }
        public string CUS_LNAME { get; set; }
        public string MOBILE_NO { get; set; }
        public string EMAIL { get; set; }
        public string ID_CARD { get; set; }
        public Nullable<System.DateTime> ID_CARD_ISSUE_DATE { get; set; }
        public Nullable<System.DateTime> ID_CARD_EXP_DATE { get; set; }
        public string DRIVING_LICENCE_ID { get; set; }
        public Nullable<System.DateTime> DRIVING_LICENCE_ISSUE_DATE { get; set; }
        public Nullable<System.DateTime> DRIVING_LICENCE_EXP_DATE { get; set; }
        public System.DateTime REGISTER_DATE { get; set; }
    
        public virtual ICollection<tb_m_customer_car> tb_m_customer_car { get; set; }
    }
}
