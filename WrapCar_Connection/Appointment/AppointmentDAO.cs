﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WrapCar_DataModel;


namespace WrapCar_Connection
{
    public class AppointmentDAO
    {


        public List<AppointmentData> getAppointmentDataList() {
                Entities context = new Entities();
                var result = (from x in context.tb_r_appointment
                              select new AppointmentData(){
                                    appid = x.APP_ID,
                                    taskid = x.ORDER_H_ID,
                                    taskname = x.APP_TITLE_APPOIINT,
                                    startdate = (DateTime) x.PLAN_DATETIME_START,
                                    enddate = (DateTime)x.PLAN_DATETIME_END,
                                    manhour = x.PLAN_DURATION_HOUR,
                                    status = x.STATUS_APP_POINTMENT
                                }).ToList();

                return result;
        }

        public List<AppointmentOrderData> getAppointmentOrderList(String OrderID) {
            Entities context = new Entities();
            var result = (from x in context.tb_r_order_h
                          join c in context.tb_m_customer on x.CUS_ID equals c.CUS_ID
                          where x.ORDER_H_ID == OrderID
                          select new AppointmentOrderData()
                          {
                              orderid = x.ORDER_H_ID,
                              Customername = c.CUS_FNAME,
                              manhour = x.MAN_DAY_EFF_PLAN,
                              orderdate =x.ORDER_DATE
                          }).ToList();

            return result;
        }

        public List<getUnAvaliableworker> getUnAvaliableTime(DateTime Starttime, DateTime Endtime)
        {
            Entities context = new Entities();
            var result = (from a in context.tb_r_appointment
                         join t in context.tb_r_tasks_schedule on a.APP_ID equals t.APP_ID
                         join e in context.tb_m_employee on t.EMP_ID equals e.EMP_ID
                         where t.PLAN_START_DATETIME >=  Starttime && t.PLAN_FINISH_DATETIME <= Endtime && e.DEPT_ID==2
                          select new getUnAvaliableworker()
                          {
                            workerid =t.EMP_ID ,
                            status ="NOT VALID", 
                            starttime = (DateTime) t.PLAN_START_DATETIME,
                            endtime = (DateTime) t.PLAN_FINISH_DATETIME
                            
                          }).ToList();

            return result;
        }

        public List<OrderList> getOrderNoApp() { 
            Entities context = new Entities();
            var all_appointment = (from o in context.tb_r_order_h select new OrderList() {
                orderid = o.ORDER_H_ID
            }).ToList();
            var all_order = (from o in context.tb_r_order_h join a in context.tb_r_appointment on o.ORDER_H_ID equals a.ORDER_H_ID select o.ORDER_H_ID).ToList();
            var outer_order = all_appointment
                .Where(o => !all_order.Contains(o.orderid))
                .ToList();

            return outer_order;
        }

        public List<getEmployee> getEmployee()
        {
            Entities context = new Entities();
            var result = (from  e in context.tb_m_employee
                          where e.DEPT_ID==2
                          select new getEmployee()
                          {
                              workerid = e.EMP_ID,
                              workername = e.EMP_FNAME+" "+e.EMP_LNAME                               
                          }).ToList();

            return result;
        }

        public bool saveAppointment(tb_r_appointment data)
        {
            bool ret = false;
            try
            {
                Entities context = new Entities();
                context.tb_r_appointment.Add(data);
                context.SaveChanges();

                ret = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ret;
        }

        public bool saveSchedule(tb_r_tasks_schedule data)
        {
            bool ret = false;
            try
            {
                Entities context = new Entities();
                context.tb_r_tasks_schedule.Add(data);
                context.SaveChanges();

                ret = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ret;
        }

        public List<Holiday> HolidayList()
        {
            List<Holiday> result = new List<Holiday>();
            result.Add(new Holiday("ปีใหม่", Convert.ToDateTime("01/01/2014")));
            result.Add(new Holiday("สงกรานต์", Convert.ToDateTime("04/13/2014")));
            result.Add(new Holiday("สงกรานต์", Convert.ToDateTime("04/14/2014")));
            result.Add(new Holiday("สงกรานต์", Convert.ToDateTime("04/15/2014")));
            return result;
        }
        public List<Worker> WorkerList()
        {
            List<Worker> result = new List<Worker>();
            result.Add(new Worker(1, "สมรัก พรรคเพื่อเก้ง"));
            result.Add(new Worker(2, "มานี มีแชร์"));
            result.Add(new Worker(3, "ช่า บันทึกของตุ๊ด"));
            result.Add(new Worker(4, "หวังดี ปีใหม่"));
            return result;
        }
       

        public List<WorkerJob> WorkerJobList() { 
            List<WorkerJob> result = new List<WorkerJob>();
            result.Add(new WorkerJob(1, 1));
            result.Add(new WorkerJob(1, 2));
            result.Add(new WorkerJob(2, 3));
            result.Add(new WorkerJob(3, 4));
            result.Add(new WorkerJob(3, 2));
            result.Add(new WorkerJob(4, 1));
            result.Add(new WorkerJob(4, 3));
            return result;
        }
        
    }
}
