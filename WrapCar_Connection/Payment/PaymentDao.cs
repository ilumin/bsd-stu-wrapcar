﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WrapCar_DataModel;
using System.Transactions;
using System.Data.Entity;
using System.Data.SqlClient; 

namespace WrapCar_Connection
{
    public class PaymentDao
    {
        public OrderData getOrderData(string O_ID)
        {
            try
            {
                Entities context = new Entities();

                var result = (from order_h in context.tb_r_order_h
                              from cus in context.tb_m_customer
                              from cus_car in context.tb_m_customer_car
                              join p in context.tb_r_payment on order_h.PART_PAYMENT equals p.PAYMENT_ID into Payment
                              from pay1 in Payment.DefaultIfEmpty()
                              where ((order_h.ORDER_H_ID == O_ID)
                              && (order_h.CUS_ID == cus.CUS_ID)
                              && (order_h.CUS_ID == cus_car.CUS_ID) && (order_h.CUS_CAR_ID == cus_car.CUS_CAR_ID)
                              )
                              select new OrderData()
                              {
                                  ORDER_H_ID = order_h.ORDER_H_ID,
                                  ORDER_DATE = order_h.ORDER_DATE,
                                  CUS_ID = order_h.CUS_ID,
                                  CUS_FNAME = cus.CUS_FNAME,
                                  CUS_LNAME = cus.CUS_LNAME,
                                  TITLE = cus.TITLE,
                                  CUS_CAR_ID = order_h.CUS_CAR_ID,
                                  CAR_LICENSE_NO = cus_car.CAR_LICENSE_NO,
                                  CAR_BRAND = cus_car.CAR_BRAND,
                                  CAR_MODEL = cus_car.CAR_MODEL,
                                  CAR_YEAR = cus_car.CAR_YEAR,
                                  CAR_COLOR = cus_car.CAR_COLOR,
                                  MAN_DAY_EFF_PLAN = order_h.MAN_DAY_EFF_PLAN,
                                  LABOR_PRICE = order_h.LABOR_PRICE,
                                  ORDER_PRICE = order_h.ORDER_PRICE,
                                  TOTAL_PRICE = order_h.TOTAL_PRICE,
                                  PART_PAYMENT = order_h.PART_PAYMENT,
                                  PAY_AMOUNT = pay1.PAY_AMOUNT,
                                  UNPAID_PAYMENT = order_h.UNPAID_PAYMENT,
                                  EMP_ID = order_h.EMP_ID
                              }).FirstOrDefault();

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool savePayment(tb_r_payment data)
        {
            bool ret = false;
            try
            {
                Entities context = new Entities();
                context.tb_r_payment.Add(data);
                context.SaveChanges();

                ret = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ret;
        }

        public PaymentData savepaymentTran(tb_r_payment data,string P_TYPE)
        {           
            try
            {
                Entities context = new Entities();
                context.tb_r_payment.Add(data);
                context.SaveChanges();

                var result = (from pay in context.tb_r_payment
                              where ((pay.ORDER_H_ID == data.ORDER_H_ID)
                              && (pay.PAY_TYPE == P_TYPE)
                             )
                              select new PaymentData()
                              {
                                  PAYMENT_ID = pay.PAYMENT_ID,
                                  ORDER_H_ID = pay.ORDER_H_ID,
                                  PAY_TYPE = pay.PAY_TYPE,
                                  PAY_AMOUNT = pay.PAY_AMOUNT,
                                  PAY_DATE = pay.PAY_DATE,
                                  EMP_ID = pay.EMP_ID
                              }).FirstOrDefault();

                var change = context.tb_r_order_h.FirstOrDefault(o => o.ORDER_H_ID == data.ORDER_H_ID);
                if (P_TYPE == "Part") 
                {
                    change.PART_PAYMENT = result.PAYMENT_ID;
                }
                else
                {
                    change.UNPAID_PAYMENT = result.PAYMENT_ID;
                }               

                context.SaveChanges();
                return result;
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
           
        } 
    
        public PaymentData getPaymentData(string O_ID, string P_TYPE)
        {
            try
            {
                Entities context = new Entities();

                var result = (from pay in context.tb_r_payment
                              from emp in context.tb_m_employee
                              where ((pay.ORDER_H_ID == O_ID)
                              && (pay.PAY_TYPE == P_TYPE)
                              && (pay.EMP_ID == emp.EMP_ID)
                             )
                              select new PaymentData()
                              {
                                  PAYMENT_ID = pay.PAYMENT_ID,
                                  ORDER_H_ID = pay.ORDER_H_ID,
                                  PAY_TYPE = pay.PAY_TYPE,
                                  PAY_AMOUNT = pay.PAY_AMOUNT,
                                  PAY_DATE = pay.PAY_DATE,
                                  EMP_ID = pay.EMP_ID,
                                  EMP_TITLE = emp.TITLE,
                                  EMP_FNAME = emp.EMP_FNAME,
                                  EMP_LNAME = emp.EMP_LNAME
                              }).FirstOrDefault();

                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
