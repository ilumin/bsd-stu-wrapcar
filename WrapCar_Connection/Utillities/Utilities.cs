﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.IO;
using WrapCar_Connection.Model;
using WrapCar_DataModel.Model;

namespace Utilities
{    

    public static class Utilities
    {
       private static ModelDAO modelDao;
       private static List<ModelHData> dataHList;

       private static List<string> CAR_BRAND_LIST;
       private static List<string> CAR_MODEL_LIST;
       private static List<string> CAR_YEAR_LIST;

       public static void getInstance()
       {
           initCarDBInstance();
       }

       private static void initCarDBInstance()
        {
            if (modelDao == null)
            {
                modelDao = new ModelDAO();
            }

            if (dataHList == null)
            {     
                dataHList = modelDao.getModelAll();
            }

        }


    
        #region Car Brand Data 

       public static List<string> GET_STR_CAR_BRAND_MASTER_ALL()
        {

                    List<string> brandList = dataHList.Select(x => x.MODEL_BRAND).ToList();
                    var result = from element in brandList
                                 orderby element
                                 group element by element;

                    if (result != null)
                        CAR_BRAND_LIST = result.Select(t => t.Key).ToList();


            return CAR_BRAND_LIST;
        }
        #endregion

        #region Car Model(Series) Data
        public static List<string> GET_STR_CAR_MODEL_ALL(string selcetedBrand)
        {
            if (dataHList != null || dataHList.Count > 0)
            {
                List<string> modelList = dataHList.Where(t => t.MODEL_BRAND == selcetedBrand).Select(x => x.MODEL_SERIES).ToList();
                var result = from element in modelList
                             orderby element
                             group element by element;

                if (result != null)
                    CAR_MODEL_LIST = result.Select(t => t.Key).ToList();
            }

            return CAR_MODEL_LIST;
        }

        public static List<string> GET_STR_CAR_YEAR_ALL(string selcetedBrand, string selcetedModel)
        {
            if (dataHList != null || dataHList.Count > 0)
            {
                List<string> modelList = dataHList.Where(t => t.MODEL_BRAND == selcetedBrand && t.MODEL_SERIES == selcetedModel).Select(x => x.MODEL_YEAR).ToList();
                var result = from element in modelList
                             orderby element
                             group element by element;

                if (result != null)
                    CAR_MODEL_LIST = result.Select(t => t.Key).ToList();
            }

            return CAR_MODEL_LIST;
        }
        public static List<ModelHData> GET_OBJ_CAR_MODEL_BY_BRAND(string selcetedBrand)
        {
            return dataHList.Where(t => t.MODEL_BRAND == selcetedBrand).ToList();
        }

        public static ModelHData GET_OBJ_CAR_MODEL_BY_BRAND(string selcetedBrand, string selcetedSeries, string year)
        {
            dataHList = modelDao.getModelAll();
            return dataHList.Find(t => t.MODEL_BRAND.Contains(selcetedBrand) && t.MODEL_SERIES.Contains(selcetedSeries) && t.MODEL_YEAR.Contains(year));
        }
        #endregion
    }   
}