﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.IO;

namespace Utilities
{    

    public static class ImageUtilities
    {

        public static void deleteExistsFile(string fileNamePath)
        {
            if (System.IO.File.Exists(fileNamePath))
                System.IO.File.Delete(fileNamePath);

        }
        public static byte[] ImageToByteArray(Image imageIn)
        {
            if (imageIn == null) return null;

            var ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            return ms.ToArray();
        }

        public static Image ByteArrayToImage(byte[] byteArrayIn)
        {
            if (byteArrayIn == null || byteArrayIn.Length == 0) return null;

            var ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }
    }   
}