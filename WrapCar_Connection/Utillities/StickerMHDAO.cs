﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WrapCar_DataModel.Sticker;

namespace WrapCar_Connection
{
    public class StickerMHDAO
    {
        public StickerItemData getMH_RATE(int ID)
        {
            try
            {
                Entities context = new Entities();

                var result = (from x in context.tb_m_sticker_item
                              where x.STK_ITEM_ID == ID
                              select new StickerItemData()
                              {
                                  STK_ITEM_ID = x.STK_ITEM_ID,
                                  WIDTH = x.WIDTH,
                                  HEIGHT = x.HEIGHT,
                                  MH_RATE = x.MH_RATE
                              }).FirstOrDefault();

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
