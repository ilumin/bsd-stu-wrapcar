﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WrapCar_DataModel;

namespace WrapCar_Connection
{
    public class ScheduleDAO
    {
        private Entities context = new Entities();

        /**
         * Schedule data
         */
        public ScheduleData getSchedule(int TASK_ID)
        {
            try
            {
                var result = (from item in this.context.tb_r_tasks_schedule.Include("tb_m_employee")
                              where item.TASK_ID == TASK_ID
                              select new ScheduleData()
                              {
                                    TASK_ID = item.TASK_ID,
                                    ORDER_H_ID = (string) item.ORDER_H_ID,
                                    APP_ID = (int) item.APP_ID,
                                    EMP_ID = (int) item.EMP_ID,
                                    PLAN_START_DATETIME = item.PLAN_START_DATETIME,
                                    PLAN_FINISH_DATETIME = item.PLAN_FINISH_DATETIME,
                                    PLAN_DURATION_HH = (int) item.PLAN_DURATION_HH,
                                    EMP_NAME = item.tb_m_employee.EMP_FNAME,
                                    WORK_STATUS = (int)item.WORK_STATUS
                              }).FirstOrDefault();

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void updateScheduleStatus(int taskId, int statusCode)
        {
            try
            {
                Entities c = new Entities();
                var result = (from item in c.tb_r_tasks_schedule
                              where item.TASK_ID == taskId
                              select item).FirstOrDefault();

                result.WORK_STATUS = statusCode;

                // if statusCode == 1 and actual date empty then update actual date
                if (statusCode == 1 && result.ACTUAL_START_DATETIME == null)
                {
                    result.ACTUAL_START_DATETIME = DateTime.Now;
                }

                // if statusCode == 2 then update actual date
                if (statusCode == 2)
                {
                    result.ACTUAL_FINISH_DATETIME = DateTime.Now;
                }

                // c.SubmitChanges();
                c.SaveChanges();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /**
         * Get all schedule
         */
        public List<ScheduleData> getScheduleList(string filterObject)
        {
            try
            {
                IQueryable<tb_r_tasks_schedule> matches = this.context.tb_r_tasks_schedule.Include("tb_m_employee");

                switch (filterObject)
                {
                    case "pending":
                        matches = matches.Where(r => r.WORK_STATUS == 0);
                        break;

                    case "working":
                        matches = matches.Where(r => r.WORK_STATUS == 1);
                        break;

                    case "finished":
                        matches = matches.Where(r => r.WORK_STATUS == 2);
                        break;

                    case "late":
                        matches = matches.Where(r => r.WORK_STATUS != 2 && r.PLAN_START_DATETIME < DateTime.Now && r.PLAN_FINISH_DATETIME < DateTime.Now);
                        break;

                    // do nothing
                    case "all":
                    default:
                        break;
                }

                var result = (from item in matches
                orderby item.PLAN_FINISH_DATETIME descending
                select new ScheduleData()
                {
                    TASK_ID = item.TASK_ID,
                    ORDER_H_ID = (string)item.ORDER_H_ID,
                    APP_ID = (int)item.APP_ID,
                    EMP_ID = (int)item.EMP_ID,
                    PLAN_START_DATETIME = (DateTime)item.PLAN_START_DATETIME,
                    PLAN_FINISH_DATETIME = (DateTime)item.PLAN_FINISH_DATETIME,
                    PLAN_DURATION_HH = (int)item.PLAN_DURATION_HH,
                    EMP_NAME = item.tb_m_employee.TITLE + item.tb_m_employee.EMP_FNAME + " " + item.tb_m_employee.EMP_LNAME,
                    WORK_STATUS = (int)item.WORK_STATUS
                }).ToList();

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
