﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WrapCar_DataModel;
using WrapCar_DataModel.Model;

namespace WrapCar_Connection.Model
{
    public class ModelDAO
    {

        public List<ModelHData> getModelAll()
        {
            try
            {
                Entities context = new Entities();
                var result = (from x in context.tb_m_model_h
                              select new ModelHData()
                              {
                                  MODEL_H_ID = x.MODEL_H_ID,
                                  MODEL_NAME = x.MODEL_NAME,
                                  MODEL_YEAR = x.MODEL_YEAR,
                                  MODEL_BRAND = x.MODEL_BRAND,
                                  MODEL_SERIES = x.MODEL_SERIES,
                                  ModelDList = (from y in x.tb_m_model_d
                                                where y.MODEL_H_ID == x.MODEL_H_ID
                                                select new ModelDData()
                                                {
                                                    MODEL_D_ID = y.MODEL_D_ID,
                                                    MODEL_H_ID = y.MODEL_H_ID,
                                                    MODEL_HEIGHT = y.MODEL_HEIGHT,
                                                    MODEL_WIDTH = y.MODEL_WIDTH,
                                                    MODEL_RATIO = y.MODEL_RATIO,
                                                    MODEL_SIDE = y.MODEL_SIDE,
                                                    MODELIMAGE = y.MODELIMAGE
                                                })
                              });

                if (result != null && result.Count() > 0)
                {
                    return result.ToList();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        public bool saveModelH(tb_m_model_h data)
        {
            bool ret = false;
            try
            {
                Entities context = new Entities();
                context.tb_m_model_h.Add(data);
                context.SaveChanges();

                ret = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ret;
        }

        public bool saveModelD(tb_m_model_d data)
        {
            bool ret = false;
            try
            {
                Entities context = new Entities();
                context.tb_m_model_d.Add(data);
                context.SaveChanges();

                ret = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ret;
        }

        public bool updateModelD(tb_m_model_d del, tb_m_model_d data)
        {
            bool ret = false;
            try
            {
                Entities context = new Entities();
                var query = "DELETE FROM tb_m_model_d WHERE MODEL_D_ID=" + del.MODEL_D_ID +
                                                     " AND MODEL_H_ID = " + del.MODEL_H_ID;
                context.Database.ExecuteSqlCommand(query);
                context.SaveChanges();

                context.tb_m_model_d.Add(data);
                context.SaveChanges();

                ret = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ret;
        }
        public bool deleteModelH(tb_m_model_h data)
        {
            bool ret = false;
            try
            {
                Entities context = new Entities();
                context.tb_m_model_h.Remove(data);
                context.SaveChanges();

                ret = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ret;
        }
    }
}
