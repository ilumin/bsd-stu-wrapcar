﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WrapCar_DataModel.Model;

namespace WrapCar_Connection.Model
{
    public class ModelDDAO
    {
        public ModelDData getModelD(int MODEL_D_ID)
        {
            try
            {
                Entities context = new Entities();

                var result = (from x in context.tb_m_model_d
                              where x.MODEL_D_ID == MODEL_D_ID
                              select new ModelDData()
                              {
                                  MODEL_D_ID = x.MODEL_D_ID,
                                  MODEL_H_ID = x.MODEL_H_ID,
                                  MODEL_SIDE = x.MODEL_SIDE,
                                  MODEL_WIDTH = x.MODEL_WIDTH,
                                  MODEL_HEIGHT = x.MODEL_HEIGHT,
                              }).FirstOrDefault();

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<ModelDData> getModelDList()
        {
            try
            {
                Entities context = new Entities();

                var result = (from x in context.tb_m_model_d
                              select new ModelDData()
                              {

                                  MODEL_D_ID = x.MODEL_D_ID,
                                  MODEL_H_ID = x.MODEL_H_ID,
                                  MODEL_SIDE = x.MODEL_SIDE,
                                  MODEL_WIDTH = x.MODEL_WIDTH,
                                  MODEL_HEIGHT = x.MODEL_HEIGHT,
                              }).ToList();

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool saveModelD(tb_m_model_d data)
        {
            bool ret = false;
            try
            {
                Entities context = new Entities();
                context.tb_m_model_d.Add(data);
                context.SaveChanges();

                ret = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ret;
        }
    }
}
