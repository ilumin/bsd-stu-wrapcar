﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WrapCar_DataModel;
using WrapCar_DataModel.Order;
using WrapCar_DataModel.Sticker;
using System.Drawing;
using Utilities;


namespace WrapCar_Connection
{
    public class OrderDAO
    {
        #region OrderHDAO

        public List<OrderHData> getOrderList(int cusId)
        {
            try
            {
                Entities context = new Entities();
                var result = (from x in context.tb_r_order_h
                              where x.CUS_ID == cusId

                              select new OrderHData()
                              {
                                  CUS_CAR_ID = x.CUS_CAR_ID,
                                  CUS_ID = x.CUS_ID,
                                  EMP_ID = x.EMP_ID,
                                  IMG_PREVIEW = x.IMG_PREVIEW,
                                  LABOR_PRICE = x.LABOR_PRICE,
                                  MAN_DAY_EFF_PLAN = x.MAN_DAY_EFF_PLAN,
                                  ORDER_DATE = x.ORDER_DATE,
                                  ORDER_H_ID = x.ORDER_H_ID,
                                  ORDER_PRICE = x.ORDER_PRICE,
                                  PART_PAYMENT = x.PART_PAYMENT,
                                  TOTAL_PRICE = x.TOTAL_PRICE,
                                  UNPAID_PAYMENT = x.UNPAID_PAYMENT,

                                  OrderDList = (from y in x.tb_r_order_d
                                                where y.ORDER_H_ID == x.ORDER_H_ID
                                                select new OrderDData()
                                                {
                                                    ORDER_D_ID = y.ORDER_D_ID,
                                                    ORDER_H_ID = y.ORDER_H_ID,
                                                    STK_ITEM_ID = y.STK_ITEM_ID,
                                                    STK_ITEM_NAME = y.STK_ITEM_NAME,
                                                    STK_SURFACE_ID = y.STK_SURFACE_ID,
                                                    START_X = y.START_X,
                                                    START_Y = y.START_Y,
                                                    WIDTH = y.WIDTH,
                                                    HEIGHT = y.HEIGHT,
                                                    AREA_USED = y.AREA_USED,
                                                    WIDTH_INCH = y.WIDTH_INCH,
                                                    HEIGHT_INCH = y.HEIGHT_INCH,
                                                    AREA_USED_INCH = y.AREA_USED_INCH,
                                                    MH_RATE = y.MH_RATE,
                                                    UNIT_PRICE = y.UNIT_PRICE,
                                                    CUTTING_IMG = y.CUTTING_IMG,
                                                    ORIGINAL_IMG = y.ORIGINAL_IMG
                                                })

                              });

                if(result != null && result.Count() > 0)
                {
                    return result.ToList();
                
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Bitmap> getStickerItemListForPrint(DateTime today)
        {
            List<Bitmap> stkBitmap = new List<Bitmap>() ;
            try
            {
                Entities context = new Entities();
                var result = (from x in context.tb_r_order_h
                              where x.ORDER_DATE.Year == today.Year
                                &&  x.ORDER_DATE.Month == today.Month
                                && x.ORDER_DATE.Day == today.Day
                              select new OrderHData()
                              {
                                  ORDER_DATE = x.ORDER_DATE,
                                  ORDER_H_ID = x.ORDER_H_ID,

                                  OrderDList = (from y in x.tb_r_order_d
                                                where y.ORDER_H_ID == x.ORDER_H_ID
                                                select new OrderDData()
                                                {
                                                    CUTTING_IMG = y.CUTTING_IMG
                                                })
                              });

                if (result != null && result.Count() > 0)
                {
                    List<OrderDData> OrderDList = (List<OrderDData>)result.Select(t => t.OrderDList.ToList());
                    foreach (OrderDData stk in OrderDList)
                    {
                       stkBitmap.Add((Bitmap) ImageUtilities.ByteArrayToImage(stk.CUTTING_IMG));
                    }

                }
                return stkBitmap;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        public bool saveOrderHeader(tb_r_order_h data)
        {
            bool ret = false;
            try
            {
                Entities context = new Entities();
                context.tb_r_order_h.Add(data);
                context.SaveChanges();

                ret = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ret;
        }
    }
}
