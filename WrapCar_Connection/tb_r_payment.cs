//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WrapCar_Connection
{
    using System;
    using System.Collections.Generic;
    
    public partial class tb_r_payment
    {
        public int PAYMENT_ID { get; set; }
        public string ORDER_H_ID { get; set; }
        public string PAY_TYPE { get; set; }
        public decimal PAY_AMOUNT { get; set; }
        public System.DateTime PAY_DATE { get; set; }
        public int EMP_ID { get; set; }
    
        public virtual tb_r_order_h tb_r_order_h { get; set; }
    }
}
