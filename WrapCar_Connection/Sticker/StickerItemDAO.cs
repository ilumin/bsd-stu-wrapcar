﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WrapCar_DataModel;
using WrapCar_DataModel.Sticker;

namespace WrapCar_Connection
{
    public class StickerItemDAO
    {

        public StickerItemData getStickerItem(int STK_ITEM_ID)
        {
            try
            {
                Entities context = new Entities();

                var result = (from x in context.tb_m_sticker_item
                              where x.STK_ITEM_ID == STK_ITEM_ID
                              select new StickerItemData()
                              {

                                  STK_ITEM_ID = x.STK_ITEM_ID,
                                  STK_CATEGORY_ID = x.STK_CATEGORY_ID,
                                  STK_NAME = x.STK_NAME,
                                  STK_PATH = x.STK_PATH,
                                  WIDTH = x.WIDTH,
                                  HEIGHT = x.HEIGHT,
                                  IMAGE =x.IMAGE,


                              }).FirstOrDefault();

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public List<StickerItemData> getStickerItemList()
        {
            try
            {
                Entities context = new Entities();

                var result = (from x in context.tb_m_sticker_item
                              select new StickerItemData()
                              {
                                  STK_ITEM_ID = x.STK_ITEM_ID,
                                  STK_CATEGORY_ID = x.STK_CATEGORY_ID,
                                  STK_NAME = x.STK_NAME,
                                  STK_PATH = x.STK_PATH,
                                  WIDTH = x.WIDTH,
                                  HEIGHT = x.HEIGHT,
                                  IMAGE = x.IMAGE,
                              }).ToList();

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool saveStickerItem(tb_m_sticker_item data)
        {
            bool ret = false;
            try
            {
                Entities context = new Entities();
                context.tb_m_sticker_item.Add(data);
                context.SaveChanges();

                ret = true;
            }
            catch (DbEntityValidationException ex)
            {

                foreach (var eve in ex.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            return ret;
        }
    }
}
