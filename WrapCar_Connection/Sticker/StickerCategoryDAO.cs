﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WrapCar_DataModel;

namespace WrapCar_Connection
{
    public class StickerCategoryDAO
    {
        public StickerCategoryData getStickerCat(int STK_CATEGORY_ID)
        {
            try
            {
                Entities context = new Entities();

                var result = (from x in context.tb_m_sticker_category
                              where x.STK_CATEGORY_ID == STK_CATEGORY_ID
                              select new StickerCategoryData()
                              {

                                  STK_CATEGORY_ID = x.STK_CATEGORY_ID,
                                  STK_CATEGORY_NAME = x.STK_CATEGORY_NAME
                              }).FirstOrDefault();

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public List<StickerCategoryData> getStickerCategoryList()
        {
            try
            {
                Entities context = new Entities();

                var result = (from x in context.tb_m_sticker_category
                              select new StickerCategoryData()
                              {
                                  STK_CATEGORY_ID = x.STK_CATEGORY_ID,
                                  STK_CATEGORY_NAME = x.STK_CATEGORY_NAME
                              }).ToList();

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool saveStickerCategory(tb_m_sticker_category data)
        {
            bool ret = false;
            try
            {
                Entities context = new Entities();
                context.tb_m_sticker_category.Add(data);
                context.SaveChanges();

                ret = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ret;
        }
    }
}
