﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrapCar_DataModel
{
   public class AppointmentData
    {
        public Int32 appid { get; set; }
        public string taskid { get;set; } 
        public string taskname { get; set;}
        public DateTime startdate { get; set; }
        public DateTime enddate { get; set; }
        public double manhour { get; set; }
        public string status { get; set; }

       
    }

   public class AppointmentOrderData
   {
       public String orderid { get; set;}
       public String Customername { get; set; }
       public Decimal? manhour { get; set; }
       public DateTime orderdate { get; set; }
   }

   public class getUnAvaliableworker 
   {
       public int workerid { get; set; }
       public String status { get; set; }
       public DateTime starttime { get; set; }
       public DateTime endtime { get; set; }
       public int manhour { get; set; }
   }

   public class OrderList {
       public string orderid { get; set; }
   }

   public class getEmployee { 
       public int workerid {get; set;}
       public String workername { get; set; }
   }
   public class Holiday
   {
       public string holidayname { get; set; }
       public DateTime holidaydate { get; set; }

       public Holiday(string a, DateTime b) {
           this.holidayname = a;
           this.holidaydate = b;
       }
   }
    public class Worker
    {
        public int workerid { get; set; }
        public string workername{get; set;}
        public Worker(int id, string fullname) {
            this.workerid = id;
            this.workername = fullname;
        }
       
    }
    public class WorkerJob
    {
      

        public int taskid { get; set; }
        public int wordkerid { get; set; }

        public WorkerJob(int p1, int p2)
        {
            // TODO: Complete member initialization
            this.taskid = p1;
            this.wordkerid = p2;
        } 
       
    }
}
