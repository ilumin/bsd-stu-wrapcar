﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrapCar_DataModel
{
    public class CustomerData
    {
        public int CUS_ID { get; set; }
        public string TITLE { get; set; }
        public string CUS_FNAME { get; set; }
        public string CUS_LNAME { get; set; }
        public string MOBILE_NO { get; set; }
        public string EMAIL { get; set; }
        public string ID_CARD { get; set; }
        public Nullable<System.DateTime> ID_CARD_ISSUE_DATE { get; set; }
        public Nullable<System.DateTime> ID_CARD_EXP_DATE { get; set; }
        public string DRIVING_LICENCE_ID { get; set; }
        public Nullable<System.DateTime> DRIVING_LICENCE_ISSUE_DATE { get; set; }
        public Nullable<System.DateTime> DRIVING_LICENCE_EXP_DATE { get; set; }
        public System.DateTime REGISTER_DATE { get; set; }
    }

    public class CustomerCarData
    {
        public int CUS_CAR_ID { get; set; }
        public int CUS_ID { get; set; }
        public string CAR_LICENSE_NO { get; set; }
        public string CAR_BRAND { get; set; }
        public string CAR_MODEL { get; set; }
        public string CAR_YEAR { get; set; }
        public string CAR_COLOR { get; set; }
        public string CAR_COLOR_HEXCODE { get; set; }
    }
}
