﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrapCar_DataModel
{
    public class Employee
    {
        public int EMP_ID { get; set; }
        public int DEPT_ID { get; set; }
        public string TITLE { get; set; }
        public string EMP_FNAME { get; set; }
        public string EMP_LNAME { get; set; }
        public int JOB_LEVEL { get; set; }
    }
}
