﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrapCar_DataModel.Model
{
    public class ModelDData
    {

        public int  MODEL_D_ID { get; set; }
        public int  MODEL_H_ID { get; set; }
        public string MODEL_SIDE  { get; set; }
        public Nullable <decimal> MODEL_WIDTH { get; set; }
        public Nullable <decimal> MODEL_HEIGHT { get; set; } 
        public string  MODEL_RATIO{ get; set; }
        public byte[] MODELIMAGE {get;set;}
    }
}
