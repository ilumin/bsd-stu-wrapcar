﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrapCar_DataModel.Model
{
    public class ModelHData
    {

        public int MODEL_H_ID { get; set; }
        public string MODEL_NAME { get; set; }
        public string  MODEL_YEAR { get; set; }
        public string MODEL_BRAND { get; set; }
        public string  MODEL_SERIES { get; set; }

        public IEnumerable<ModelDData> ModelDList { get; set; }

        public void SubmitChanges()
        {
            throw new NotImplementedException();
        }
    }
}
