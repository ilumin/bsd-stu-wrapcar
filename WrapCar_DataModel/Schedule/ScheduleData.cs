﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrapCar_DataModel
{
    public class ScheduleData
    {
        public int TASK_ID { get; set; }
        public string ORDER_H_ID { get; set; }
        public int APP_ID { get; set; }
        public int EMP_ID { get; set; }
        public Nullable<System.DateTime> PLAN_START_DATETIME { get; set; }
        public Nullable<System.DateTime> PLAN_FINISH_DATETIME { get; set; }
        public int PLAN_DURATION_HH { get; set; }
        public Nullable<System.DateTime> ACTUAL_START_DATETIME { get; set; }
        public Nullable<System.DateTime> ACTUAL_FINISH_DATETIME { get; set; }
        public int ACTUAL_DURATION_HH { get; set; }
        public int OVERTIME_HH { get; set; }
        public int WORK_PROGRESS { get; set; }
        public int WORK_STATUS { get; set; }

        public string EMP_NAME { get; set; }
    }
}
