﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrapCar_DataModel
{
    public class OrderData
    {
        public String ORDER_H_ID { get; set; }
        public Nullable<System.DateTime> ORDER_DATE { get; set; }
        public int CUS_ID { get; set; }
        public string TITLE { get; set; }
        public string CUS_FNAME { get; set; }
        public string CUS_LNAME { get; set; }
        public int CUS_CAR_ID { get; set; }
        public string CAR_LICENSE_NO { get; set; }
        public string CAR_BRAND { get; set; }
        public string CAR_MODEL { get; set; }
        public string CAR_YEAR { get; set; }
        public string CAR_COLOR { get; set; }
        public Nullable<decimal> MAN_DAY_EFF_PLAN { get; set; }
        public Nullable<decimal> LABOR_PRICE { get; set; }
        public Nullable<decimal> ORDER_PRICE { get; set; }
        public Nullable<decimal> TOTAL_PRICE { get; set; }
        public Nullable<int> PART_PAYMENT { get; set; }
        public Nullable<decimal> PAY_AMOUNT { get; set; }
        public Nullable<int> UNPAID_PAYMENT { get; set; }
        public string EMP_ID { get; set; }

    }

    public class PaymentData
    {
        public int PAYMENT_ID { get; set; }
        public string ORDER_H_ID { get; set; }
        public string PAY_TYPE { get; set; }
        public Nullable<decimal> PAY_AMOUNT { get; set; }
        public DateTime PAY_DATE { get; set; }
        public int EMP_ID { get; set; }
        public string EMP_TITLE { get; set; }
        public string EMP_FNAME { get; set; }
        public string EMP_LNAME { get; set; }

     }
}
