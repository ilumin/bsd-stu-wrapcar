﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrapCar_DataModel.Sticker
{
   public class StickerItemData
    {
        public int STK_ITEM_ID { get; set; }
        public int STK_CATEGORY_ID { get; set; }
        public string STK_NAME { get; set; }
        public string STK_PATH { get; set; }
        public Nullable <decimal> WIDTH { get; set; }
        public Nullable<decimal> HEIGHT { get; set; }
        public Nullable<decimal> MH_RATE { get; set; }
        public byte[] IMAGE { get; set; } 
       

        public void SubmitChanges()
        {
            throw new NotImplementedException();
        }
    }
}
