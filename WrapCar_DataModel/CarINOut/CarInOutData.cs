﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrapCar_DataModel
{
    public class CarInOutData
    {
        public int CARINOUT_ID { get; set; }
        public int APP_ID { get; set; }       
        public Nullable<DateTime> CARIN_DATE { get; set; }
        public Nullable<int> CARIN_EMP_ID { get; set; }
        public string CARIN_EMP_TITLE { get; set; }
        public string CARIN_EMP_FNAME { get; set; }
        public string CARIN_EMP_LNAME { get; set; }
        public Nullable<DateTime> CAROUT_DATE { get; set; }
        public Nullable<int> CAROUTEMP_ID { get; set; }
        public string CAROUT_EMP_TITLE { get; set; }
        public string CAROUT_EMP_FNAME { get; set; }
        public string CAROUT_EMP_LNAME { get; set; }
   }

    public class APPCarData
    {
        public int APP_ID { get; set; }
        public Nullable<DateTime> APP_DATETIME_START { get; set; }
        public Nullable<DateTime> APP_DATETIME_FINISH { get; set; }
        public string STATUS_APP { get; set; }
        public String ORDER_H_ID { get; set; }
        public int CUS_ID { get; set; }
        public string TITLE { get; set; }
        public string CUS_FNAME { get; set; }
        public string CUS_LNAME { get; set; }
        public int CUS_CAR_ID { get; set; }
        public string CAR_LICENSE_NO { get; set; }
        public string CAR_BRAND { get; set; }
        public string CAR_MODEL { get; set; }
        public string CAR_YEAR { get; set; }
        public string CAR_COLOR { get; set; }
    }
}
