﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;

namespace WrapCar_DataModel.Order
{
   public class OrderHData
    {
        public string ORDER_H_ID { get; set; }
        public DateTime ORDER_DATE { get; set; }
        public int CUS_ID { get; set; }
        public int CUS_CAR_ID { get; set; }
        public decimal? MAN_DAY_EFF_PLAN { get; set; }
        public decimal? LABOR_PRICE { get; set; }
        public decimal? ORDER_PRICE { get; set; }
        public decimal? TOTAL_PRICE { get; set; }
        public int? PART_PAYMENT { get; set; }
        public int? UNPAID_PAYMENT { get; set; }
        public string EMP_ID { get; set; }
        public byte[]  IMG_PREVIEW { get; set; }

        public IEnumerable<OrderDData> OrderDList { get; set; }

        public void SubmitChanges()
        {
            throw new NotImplementedException();
        }
    }
}
