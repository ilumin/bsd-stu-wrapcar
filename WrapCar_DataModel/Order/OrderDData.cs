﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrapCar_DataModel.Order
{
   public class OrderDData
    {
       public int ORDER_D_ID { get; set; }
        public string ORDER_H_ID { get; set; }
        public int STK_ITEM_ID { get; set; }
        public string STK_ITEM_NAME { get; set; }
        public int STK_SURFACE_ID { get; set; }
        public int? START_X { get; set; }
        public int? START_Y { get; set; }
        public decimal? WIDTH { get; set; }
        public decimal? HEIGHT { get; set; }
        public decimal? AREA_USED { get; set; }
        public decimal? WIDTH_INCH { get; set; }
        public decimal? HEIGHT_INCH { get; set; }
        public decimal? AREA_USED_INCH { get; set; }

        public decimal? MH_RATE { get; set; }
        public decimal? UNIT_PRICE { get; set; }
        public byte[] CUTTING_IMG { get; set; }
        public byte[] ORIGINAL_IMG { get; set; }

        public void SubmitChanges()
        {
            throw new NotImplementedException();
        }
    }
}
